package com.dairycenter.dairycenterapp;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import me.aflak.bluetooth.Bluetooth;

public class Bt extends Activity implements Bluetooth.CommunicationCallback {

    Bluetooth b;
    String name;
    public static String text = "";
    OpenBluetoothActivity select = new OpenBluetoothActivity();
    Activity activity1;
    String reg,bits;
    int str;
    DatabaseHelper db;

    public void obj(Activity activity) {
        b = new Bluetooth(activity);
    }


    public void enable(Activity activity) {
        activity1 = activity;
        b.enableBluetooth();
//        pos = select.posi;

        str = OpenBluetoothActivity.getpos();
        Log.e("img", String.valueOf(str));

        name = b.getPairedDevices().get(str).getName();
        Log.d("name",name);
        b.connectToName(name);
    }


    public void comm(Activity activity) {
        b.setCommunicationCallback(this);
    }

    public void out(String msg) {
        b.send(msg);
    }




    public void connect() {
        b.connectToDevice(b.getPairedDevices().get(str));
    }

    @Override
    public void onConnect(final BluetoothDevice device) {
        Log.d("Connected To : " + device.getName() + " - " + device.getAddress(), " CONNECTION DONE");
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity1.getApplicationContext(), "Device Connected :" + device.getName(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onDisconnect(BluetoothDevice device, String message) {
        Log.d("DICONNECTED :", "TRYING AGAIN : " + message);
        b.connectToDevice(device);
    }


    @Override
    public void onMessage(final String message) {
        db = new DatabaseHelper(this);
//        db.deleteFarmerRegRecord();
        text = message;
        Log.d("INCOMING MESSAGE", "onMESSAGE : " + message);
//        db.insertblue(text);
//        Log.d("nnn",db.insertblue(text));
    }

    @Override
    public void onError(String message) {
        Log.d("SOMETHING EROOR ", "onERROR : " + message);
    }

    @Override
    public void onConnectError(final BluetoothDevice device, String message) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        b.connectToDevice(device);
                        b.connectToName(name);
                    }
                }, 2000);
            }
        });
    }

    public void Display(final String s){
        Toast.makeText(activity1.getApplicationContext(), "Not Connected \nTrying Again", Toast.LENGTH_SHORT).show();
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                text(s + "\n");
//                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        });
    }



    public static String rec(){
        return text;
    }


}






