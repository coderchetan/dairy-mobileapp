package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.CollectionReportListAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CollectionReportListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CollectionReportListAdapter adapter;
    DatabaseHelper db;
    List<CollectionReport> list = new ArrayList<>();
    String shift;
    String currentdate;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private EditText mOutEditText;
    private String mConnectedDeviceName = null;
    private StringBuffer mOutStringBuffer;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService2 mChatService = null;
    Button printbutton,connect;


    public static final String inputFormat = "HH:mm";

    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.UK);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_report_list);

        getSupportActionBar().setTitle("Collection Transaction List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView           = (RecyclerView)findViewById(R.id.recycler_view);
        printbutton            = (Button)findViewById(R.id.print_btn);
        connect                = (Button)findViewById(R.id.connect);

        shift = "";


        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serverIntent = new Intent(CollectionReportListActivity.this, DeviceListActivity2.class);

                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        });

        mBluetoothAdapter   =   BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }


        printbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list.size(); i++) {
                    String id         = list.get(i).getId();
                    String farmername = list.get(i).getFarmer_name();
                    String farmerno   = list.get(i).getFarmer_no();
                    String shift      = list.get(i).getShift();
                    String weight     = list.get(i).getWeight();
                    String fat       = list.get(i).getFat();
                    String snf       = list.get(i).getSnf();
                    String lacto   = list.get(i).getLacto();
                    String rate   = list.get(i).getRate();
                    String amount   = list.get(i).getAmount();


//                    sendMessage(id+"\\n"+farmername+"\\n"+farmerno+"\\n"+shift+"\\n"+weight+""+fat+"\\n"+snf+"\\n"+lacto+
//                            "\\n"+rate+"\\n"+amount);

                    String message = ("Id:"+id+", "+"Farmer name:"+farmername+", "+"Farmer No:"+farmerno+", "
                            +"Shift:"+shift+", "+"Weight:"+weight+","+"Fat:"+fat+", "
                            +"Snf:"+snf+", "+"Lacto:"+lacto+", "+"Rate:"+rate+", "+"Amount"+amount);

                    sendMessage(message);

                }
            }
        });


        mChatService = new BluetoothChatService2(CollectionReportListActivity.this, mHandler);
        mOutStringBuffer = new StringBuffer("");


        Calendar calendar    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        currentdate = simpleDateFormat.format(today);

        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.US);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        Log.d("hour", String.valueOf(hour));
        Date datess = null;
        try {
            datess = sdf.parse(hour+ ":" +minute);
        } catch (ParseException e) {
            e.printStackTrace();
        }


//        SimpleDateFormat sdf =   new SimpleDateFormat("HH");
//        int temp             =   Integer.parseInt(sdf.format(calendar.getTimeInMillis()));
//        if(temp>12){
//            shift = "Evening";
//        }else{
//            shift  = "Morning";
//        }

        db = new DatabaseHelper(this);
        final SQLiteDatabase dataBased   =   db.getWritableDatabase();
        Cursor mCursord = dataBased.rawQuery("SELECT * FROM "
                + DatabaseHelper.shifttime, null);
        if (mCursord.moveToFirst()) {
            do {
                String  mor_starttime = mCursord.getString(mCursord.getColumnIndex("morningstarttime"));
                String  mor_endtime = mCursord.getString(mCursord.getColumnIndex("morningendtime"));
                String  eve_starttime = mCursord.getString(mCursord.getColumnIndex("eveningstarttime"));
                String  eve_endtime = mCursord.getString(mCursord.getColumnIndex("eveningendtime"));

                Date d1  = parseDate(mor_starttime);
                Date d2  = parseDate(mor_endtime);
                Date d3  = parseDate(eve_starttime);
                Date d4  = parseDate(eve_endtime);


                if(d1.before(datess) && d2.after(datess)){
                    shift  = "Morning";
                }

                if(d3.before(datess) && d4.after(datess)){
                    shift = "Evening";
                }
            } while (mCursord.moveToNext());
        }




        db = new DatabaseHelper(CollectionReportListActivity.this);
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.COLLECTIONDATAREPORT, null);

        if (mCursor.moveToFirst()) {
            do {

                if(currentdate.equals(mCursor.getString(mCursor.getColumnIndex("dates"))) &&
                        shift.equals(mCursor.getString(mCursor.getColumnIndex("shift")))) {

                    String id = mCursor.getString(mCursor.getColumnIndex("id"));
                    String farmer_no = mCursor.getString(mCursor.getColumnIndex("farmer_no"));
                    String farmer_name = mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                    String user_cattle = mCursor.getString(mCursor.getColumnIndex("userCattle"));
                    String dates = mCursor.getString(mCursor.getColumnIndex("dates"));
                    String shift = mCursor.getString(mCursor.getColumnIndex("shift"));
                    String weight = mCursor.getString(mCursor.getColumnIndex("weightss"));
                    String fat = mCursor.getString(mCursor.getColumnIndex("fatsss"));
                    String snf = mCursor.getString(mCursor.getColumnIndex("snffss"));
                    String lacto = mCursor.getString(mCursor.getColumnIndex("lactoo"));
                    String rate = mCursor.getString(mCursor.getColumnIndex("ratesss"));
                    String amount = mCursor.getString(mCursor.getColumnIndex("amountss"));

                    if(user_cattle.equals("buffelo")){
                        user_cattle = "Buffalo";
                    }else if(user_cattle.equals("cow")){
                        user_cattle = "Cow";
                    }

                    CollectionReport collectionReport = new CollectionReport();

                    collectionReport.setId(id);
                    collectionReport.setFarmer_no(farmer_no);
                    collectionReport.setFarmer_name(farmer_name);
                    collectionReport.setCattletype(user_cattle);
                    collectionReport.setShift(shift);
                    collectionReport.setWeight(weight);
                    collectionReport.setFat(fat);
                    collectionReport.setSnf(snf);
                    collectionReport.setLacto(lacto);
                    collectionReport.setRate(rate);
                    collectionReport.setAmount(amount);
                    collectionReport.setDates(dates);

                    list.add(collectionReport);
                    adapter = new CollectionReportListAdapter(CollectionReportListActivity.this, list);
                    recyclerView.setAdapter(adapter);
                }
            } while (mCursor.moveToNext());
        }
    }

    private Date parseDate(String Date){
        try{
            return inputParser.parse(Date);
        }catch(ParseException e ){
            return new Date(0);
        }
    }


    public void openDialog(){
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.popupbutton,null);
        Button popupbutton = (Button) view.findViewById(R.id.popupbutton);

        popupbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = "Mrunali";
                sendMessage(str);
            }
        });

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.create();
        builder.show();

    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null) openDialog();
        }
    }
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService2.STATE_NONE) {
                mChatService.start();
            }
        }
    }

    private void setupChat() {
//        weight.setOnEditorActionListener(mWriteListener);
//        mSendButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                TextView view1    =   (TextView)findViewById(R.id.weight);
//                TextView view2    =   (TextView)findViewById(R.id.fat);
//                TextView view3    =   (TextView)findViewById(R.id.lacto);
//                TextView view5    =   (TextView)findViewById(R.id.protein);
//                TextView view6    =   (TextView)findViewById(R.id.water);
//
//                String messa1   = view1.getText().toString();
//                String message2 = view2.getText().toString();
//                String message3 = view3.getText().toString();
//                String message5 = view5.getText().toString();
//                String message6 = view6.getText().toString();
//
//                String message = (messa1+"\\n"+message2+"\\n"+message3+"\\n"+message5+"\\n"+message6+"\\n");
//                Log.d("message:",message);
//                sendMessage(message);
//            }
//        });
        mChatService = new BluetoothChatService2(CollectionReportListActivity.this, mHandler);
        mOutStringBuffer = new StringBuffer("");
    }


    @Override
    public synchronized void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) mChatService.stop();
    }



    private void sendMessage(String message) {
        if (mChatService.getState() != BluetoothChatService2.STATE_CONNECTED) {
            Toast.makeText(CollectionReportListActivity.this, "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mChatService.write(send);
            mOutStringBuffer.setLength(0);
//            weight.setText(mOutStringBuffer);
//            fat.setText(mOutStringBuffer);
//            lacto.setText(mOutStringBuffer);
//            snf.setText(mOutStringBuffer);
//            protien.setText(mOutStringBuffer);
//            water.setText(mOutStringBuffer);
        }
    }


    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    return true;
                }
            };

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.d("recv data:",readMessage);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(CollectionReportListActivity.this, "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(CollectionReportListActivity.this, msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(DeviceListActivity2.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    mChatService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    openDialog();
//                    setupChat();
                } else {
                    Toast.makeText(CollectionReportListActivity.this, "Bluetooth was not enabled", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}