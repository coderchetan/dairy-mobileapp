package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.HomeFragments.CallibrationFragment;
import com.dairycenter.dairycenterapp.HomeFragments.CollectionFragment;
import com.dairycenter.dairycenterapp.HomeFragments.DeductionFragment;
import com.dairycenter.dairycenterapp.HomeFragments.FarmerRegiFragment;
import com.dairycenter.dairycenterapp.HomeFragments.PurchaseRequiFragment;
import com.dairycenter.dairycenterapp.HomeFragments.ReportFragment;
import com.dairycenter.dairycenterapp.HomeFragments.SalesTransactionFragment;
import com.dairycenter.dairycenterapp.HomeFragments.SettingsFragment;
import com.dairycenter.dairycenterapp.HomeFragments.SynchronizationFragment;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.dairycenter.dairycenterapp.MainActivity.KEY_USERNAME;
import static com.dairycenter.dairycenterapp.MainActivity.SHARED_PREFS;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private View navHeader;

    public final static String COLLECTION_FRAG = "com.mind.dairy.collection";
    public final static String FARMER_FRAG = "com.mind.dairy.farmer";
    public final static String SALES_FRAG = "com.mind.dairy.sales";
    public final static String DEDUCTION_FRAG = "com.mind.dairy.deduction";
    public final static String PURCHASE_FRAG = "com.mind.dairy.purchase";
    public final static String REPORTS_FRAG = "com.mind.dairy.reports";
    public final static String SETTINGS_FRAG = "com.mind.dairy.settings";

    public final static String SELECTED_TAG = "selected_index";
    public static int selectedIndex;
    public final static int COLLECTION = 1;
    public final static int FARMER = 2;
    public final static int SALES = 3;
    public final static int PURCHASE = 4;
    public final static int DEDUCTION = 5;
    public final static int REPORTS = 6;
    public final static int SETTINGS = 7;
    public final static int CALIBRATION = 8;
    public final static int SYNCHRONIZATION = 9;
    public final static int LOGOUT = 10;


    private long backPressedTime;
    private Toast backToast;
    Bitmap bmp;
    private ProgressDialog progressDialog;

    TextView center_operator;
    DatabaseHelper db;
    ImageView img_authorImage;
    String tra_id, str, expiredate1, expiredate2;
    Date d1, d2, d3;
    public static AlertDialog dialog1;

    public static final String SHARED_PREFS = "shared_prefs";
    SharedPreferences sharedPreferences;



    String e, p, currentdate;
    String banksuccess,centersuccess,societysuccess,farmersuccess,deductionsuccess,ratechartsuccess,
            productsuccess,unitsuccess,shifttimesuccess,purchasesuccess,sppliersuccess,rangesuccess,usersuccess,
            settingcentersuccess,settingallsuccess,ratechartsuccess2,ratechartsuccess3,ratechartsuccess4;

    public static final String UPDATED_DATA_API = "http://mcsoft.whitegoldtech.com:8888/api/updatedate";

    String updated_data_date, centerids,rate_chartid,currenttime,fromdate,todate,center_number,rmcu_number,center_num,
            cow_morning,buffelo_morning,cow_evening,buffelo_evening,update_data_time;
    Date fromcurrenttime;
    Date tocuutime;
    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        navigationView              =      (NavigationView) findViewById(R.id.nav_view);
        navHeader                   =      navigationView.getHeaderView(0);
        center_operator             =      (TextView) navHeader.findViewById(R.id.center_operator);
        img_authorImage             =      (ImageView) navHeader.findViewById(R.id.img_authorImage);
        loading                     =      (ProgressBar) findViewById(R.id.loading);

        progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        loading.setVisibility(View.GONE);

        str = "";

//        sharedPreferences = getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
//        e = sharedPreferences.getString(KEY_USERNAME, null);

        SimpleDateFormat simpleDateFormats = new SimpleDateFormat("dd/MM/yyyy");
        Date todays = new Date();
        currentdate = simpleDateFormats.format(todays);

        Calendar calendarr    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormatr = new SimpleDateFormat("HH:mm:ss");
        currenttime = simpleDateFormatr.format(calendarr.getTime());

        db = new DatabaseHelper(this);
        final SQLiteDatabase dataBases = db.getWritableDatabase();
        final Cursor mCursordate = dataBases.rawQuery("SELECT * FROM updateddatadate", null);
        if (mCursordate.moveToFirst()) {
            do {
                updated_data_date   = mCursordate.getString(mCursordate.getColumnIndex("currentdate"));
                update_data_time    = mCursordate.getString(mCursordate.getColumnIndex("currenttime"));
            } while (mCursordate.moveToNext());
        }

        SimpleDateFormat formatrr = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            fromcurrenttime = formatrr.parse(updated_data_date+" "+update_data_time);
//            fromcurrenttime = formatrr.parse("07/05/2021 10:11:11");
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }

        SimpleDateFormat simpleDateFormaghhh = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormaghhh.setTimeZone(TimeZone.getTimeZone("UTC"));
         fromdate = simpleDateFormaghhh.format(fromcurrenttime);
        Log.d("ffff", fromdate);


        SimpleDateFormat formatrr2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        try {
            tocuutime  = formatrr2.parse( currentdate+" "+currenttime);
//            tocuutime  = formatrr2.parse( "30/04/2021 10:11:11");
        } catch (ParseException parseException) {
            parseException.printStackTrace();
        }
        SimpleDateFormat simpleDateFormaghhh2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        simpleDateFormaghhh2.setTimeZone(TimeZone.getTimeZone("UTC"));
         todate = simpleDateFormaghhh2.format(tocuutime);
        Log.d("sssss", todate);

        final Cursor mCursors = dataBases.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursors.moveToFirst()) {
            do {
                String center_operatorname = mCursors.getString(mCursors.getColumnIndex("center_operator"));
                String urlImage            = mCursors.getString(mCursors.getColumnIndex("image_url"));
                center_number              = mCursors.getString(mCursors.getColumnIndex("center_number"));

                center_operator.setText(center_operatorname);
                new HomeActivity.GetImageFromUrl(img_authorImage).execute("http://mcsoft.whitegoldtech.com:8888/centerimages/" + urlImage);
            } while (mCursors.moveToNext());
        }

        final Cursor mCursorsss = dataBases.rawQuery("SELECT * FROM "
                + DatabaseHelper.rmcumaster, null);
        if (mCursorsss.moveToFirst()) {
            do {
                rmcu_number = mCursorsss.getString(mCursorsss.getColumnIndex("society_number"));
            } while (mCursors.moveToNext());
        }

        final Cursor mCursorssss = dataBases.rawQuery("SELECT * FROM "
                + DatabaseHelper.centeruser, null);
        if (mCursorssss.moveToFirst()) {
            do {
                center_num = mCursorssss.getString(mCursorssss.getColumnIndex("centerid"));
            } while (mCursorssss.moveToNext());
        }

        final Cursor mCursorstra = dataBases.rawQuery("SELECT * FROM "
                + DatabaseHelper.downloadsettingdata, null);
        if (mCursorstra.moveToFirst()) {
            do {
                cow_morning        =     mCursorstra.getString(mCursorstra.getColumnIndex("cowmorning"));
                buffelo_morning    =     mCursorstra.getString(mCursorstra.getColumnIndex("buffelomorning"));
                cow_evening        =     mCursorstra.getString(mCursorstra.getColumnIndex("cowevening"));
                buffelo_evening    =     mCursorstra.getString(mCursorstra.getColumnIndex("buffeloevening"));

            } while (mCursorstra.moveToNext());
        }

        Intent intent = getIntent();
        str = intent.getStringExtra("demo");

//        if (str.equals("Alert")) {
//            getRespUpdatedBank();
//        }


        db = new DatabaseHelper(HomeActivity.this);
        SQLiteDatabase dataBasess = db.getWritableDatabase();

        final Cursor mCursorn = dataBasess.rawQuery("SELECT * FROM "
                + DatabaseHelper.downloadsettingdata, null);
        if (mCursorn.moveToFirst()) {
            do {
                tra_id = mCursorn.getString(mCursorn.getColumnIndex("id"));
                Cursor mCursorss = dataBases.rawQuery("SELECT * FROM listRatechart " +
                        "where id_R = ?", new String[]{(tra_id)}, null);
                if (mCursorss.moveToFirst() && mCursorss != null) {
                    do {
                        String expiration_date = mCursorss.getString(mCursorss.getColumnIndex("expiration_date"));

                        String strt = expiration_date;
                        SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        try {
                            d1 = formats.parse(strt);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Calendar calendar1 = Calendar.getInstance();
                        calendar1.setTime(d1);
                        calendar1.add(Calendar.DATE, -2);
                        String twodaybefore = DateFormat.getDateInstance().format(calendar1.getTime());


                        calendar1.setTime(d1);
                        calendar1.add(Calendar.DATE, -1);
                        String onedaybefore = DateFormat.getDateInstance().format(calendar1.getTime());


                        SimpleDateFormat formatss = new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault());
                        try {
                            d2 = formatss.parse(twodaybefore);
                            SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
                            expiredate1 = simpleDate.format(d2);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        try {
                            d3 = formatss.parse(onedaybefore);
                            SimpleDateFormat simpleDate2 = new SimpleDateFormat("dd/MM/yyyy");
                            expiredate2 = simpleDate2.format(d3);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Calendar calendar = Calendar.getInstance();
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                        Date today = new Date();
                        String currentdate = simpleDateFormat.format(today);

                        if (str.equals("Alert")) {
                            if (currentdate.equals(expiredate1) || currentdate.equals(expiredate2)) {
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(HomeActivity.this);
                                builder1.setMessage("Rate chart has been expired, would like to proceed with existing rate chart?");
                                builder1.setCancelable(true);

                                builder1.setPositiveButton(
                                        "yes",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                AlertDialog alertDialog = builder1.create();
                                alertDialog.show();
                            }
                        }

                    } while (mCursorss.moveToNext());
                }
            } while (mCursorn.moveToNext());
        }


        navigationView.setNavigationItemSelectedListener(this);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (savedInstanceState != null) {
            navigationView.getMenu().getItem(savedInstanceState.getInt(SELECTED_TAG)).setCheckable(true);
            return;
        }
        selectedIndex = COLLECTION;
        getSupportFragmentManager().beginTransaction().
                add(R.id.fragment_container,
                        new CollectionFragment(), COLLECTION_FRAG).commit();
    }

    public static class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;

        public GetImageFromUrl(ImageView img) {
            this.imageView = img;
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            Bitmap bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_collection:
                if (!item.isChecked()) {
                    selectedIndex = COLLECTION;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            replace(R.id.fragment_container, new CollectionFragment(), COLLECTION_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_farmer_registration:
                if (!item.isChecked()) {
                    selectedIndex = FARMER;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new FarmerRegiFragment(), FARMER_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_sale_transaction:
                if (!item.isChecked()) {
                    selectedIndex = SALES;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new SalesTransactionFragment(), SALES_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_purchase_requisition:
                if (!item.isChecked()) {
                    selectedIndex = PURCHASE;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new PurchaseRequiFragment(), PURCHASE_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_deduction:
                if (!item.isChecked()) {
                    selectedIndex = DEDUCTION;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new DeductionFragment(), DEDUCTION_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_reports:
                if (!item.isChecked()) {
                    selectedIndex = REPORTS;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new ReportFragment(), DEDUCTION_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_settings:
                if (!item.isChecked()) {
                    selectedIndex = SETTINGS;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new SettingsFragment(), SETTINGS_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_calibration:
                if (!item.isChecked()) {
                    selectedIndex = CALIBRATION;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new CollectionFragment(), "CollectionFragment").
                            addToBackStack("CollectionFragment").
                            replace(R.id.fragment_container, new CallibrationFragment(), SETTINGS_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_synchronization:
                if (!item.isChecked()) {
                    selectedIndex = SYNCHRONIZATION;
                    item.setChecked(true);
                    getSupportFragmentManager().
                            beginTransaction().
                            add(new SynchronizationFragment(), "SychronizationFragment").
                            addToBackStack("SychronizationFragment").
                            replace(R.id.fragment_container, new SynchronizationFragment(), SETTINGS_FRAG).commit();
                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
            case R.id.item_logout:
                if (!item.isChecked()) {
                    selectedIndex = LOGOUT;
                    item.setChecked(true);

//                    SharedPreferences.Editor editor = sharedPreferences.edit();
//                    editor.clear();
//                    editor.apply();

                    Intent i = new Intent(HomeActivity.this, MainActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();


                }
                drawerLayout.closeDrawer(GravityCompat.START);
                return true;
        }
        return false;
    }


    public void setupNavigationDrawer(Toolbar toolbar) {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.indigo));
//        drawerToggle.setHomeAsUpIndicator(R.drawable.menuicon);

//        toolbar.setNavigationIcon(R.drawable.menuicon);
        drawerToggle.syncState();
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menuicon);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_TAG, selectedIndex);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else if (backPressedTime + 2000 > System.currentTimeMillis()) {
            backToast = Toast.makeText(getBaseContext(), "Press back again top to exit", Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime = System.currentTimeMillis();

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return true;
    }

    public void getRespUpdatedBank() {
        StringRequest eventoReq = new StringRequest(Request.Method.POST,UPDATED_DATA_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("REsponce", response.toString());
                        try{
                            JSONArray k= new JSONArray(response);

//                            for (int i = 0; i < k.length(); i++) {
                                try {
                                    JSONObject obj = k.getJSONObject(0);
                                    banksuccess =  obj.getString("banksuccess");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
//                            }

                            try {
                                JSONObject obj = k.getJSONObject(1);
                                centersuccess = obj.getString("centersuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(2);
                                societysuccess = obj.getString("societysuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(3);
                                farmersuccess = obj.getString("farmersuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(4);
                                ratechartsuccess = obj.getString("ratechartsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(5);
                                sppliersuccess = obj.getString("sppliersuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(6);
                                unitsuccess = obj.getString("unitsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            try {
                                JSONObject obj = k.getJSONObject(7);
                                productsuccess = obj.getString("productsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(8);
                                rangesuccess = obj.getString("rangesuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(9);
                                usersuccess = obj.getString("usersuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            try {
                                JSONObject obj = k.getJSONObject(10);
                                purchasesuccess = obj.getString("purchasesuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(11);
                                shifttimesuccess = obj.getString("superadminsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(12);
                                settingcentersuccess = obj.getString("settingsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(13);
                                settingallsuccess = obj.getString("settingsuccessall");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(14);
                                ratechartsuccess2 = obj.getString("ratechartsuccess2");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject obj = k.getJSONObject(15);
                                ratechartsuccess3 = obj.getString("ratechartsuccess3");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject obj = k.getJSONObject(16);
                                ratechartsuccess4 = obj.getString("ratechartsuccess4");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject obj = k.getJSONObject(17);
                                deductionsuccess = obj.getString("deductionsuccess");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            Log.d("banksuccess",banksuccess); //0
                            Log.d("centersuccess",centersuccess); //1
                            Log.d("societysuccess",societysuccess); //2
                            Log.d("farmersuccess",farmersuccess);  //3
                            Log.d("deductionsuccess",deductionsuccess); //17
                            Log.d("ratechartsuccess",ratechartsuccess); //4
                            Log.d("unitsuccess",unitsuccess); //5
                            Log.d("purchasesuccess",purchasesuccess); //10
                            Log.d("shifttimesuccess",shifttimesuccess); //11
                            Log.d("productsuccess",productsuccess); //7
                            Log.d("settingallsuccess",settingallsuccess); //13
                            Log.d("settingcentersuccess",settingcentersuccess); //12
                            Log.d("ratechartsuccess2",ratechartsuccess2); //14
                            Log.d("ratechartsuccess3",ratechartsuccess3); //15
                            Log.d("ratechartsuccess4",ratechartsuccess4); //16


                            if(banksuccess.equals("true") || centersuccess.equals("true") || societysuccess.equals("true") || farmersuccess.equals("true") ||
                                    deductionsuccess.equals("true") || ratechartsuccess.equals("true") || productsuccess.equals("true") || purchasesuccess.equals("true")
                                    || shifttimesuccess.equals("true") || unitsuccess.equals("true") || settingallsuccess.equals("true") || settingcentersuccess.equals("true")
                            || ratechartsuccess2.equals("true") || ratechartsuccess3.equals("true") || ratechartsuccess4.equals("true")){
                                checkAlertUpdatedData();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Errorsss: " + error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("fromdate", fromdate);
                params.put("todate", todate);

                params.put("center_number", center_number);  //021
                params.put("rmcu_number", rmcu_number);    //053
                params.put("center1", center_num);   //21

                params.put("id1", cow_morning); //3
                params.put("id2", buffelo_morning); //42
                params.put("id3", cow_evening);  //44
                params.put("id4", buffelo_evening);  //54

                return params;
            }
        };

        eventoReq.setShouldCache(false);
        eventoReq.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
        requestQueue.add(eventoReq);
    }


    public void checkAlertUpdatedData() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Data is updated on server, please download the data...");
        builder.setCancelable(false);
        builder.create();

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                builder.setCancelable(true);

                SimpleDateFormat simpleDateFormats = new SimpleDateFormat("dd/MM/yyyy");
                Date todays = new Date();
                String currentdate = simpleDateFormats.format(todays);

                Calendar calendarr    =    Calendar.getInstance();
                SimpleDateFormat simpleDateFormatr = new SimpleDateFormat("HH:mm:ss");
                String currenttime = simpleDateFormatr.format(calendarr.getTime());

                db.adddownload_datadate(currentdate,currenttime);

                updatebank();
                progressDialog.show();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                builder.setCancelable(true);
            }
        });
        builder.show();
    }


    public void updatebank(){
        StringRequest eventoReq = new StringRequest(Request.Method.POST,UPDATED_DATA_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("REsponce", response.toString());
                        try{
                            JSONArray k= new JSONArray(response);

                            // bank data....................................................
//                            for (int i = 0; i < k.length(); i++) {
                                try {
                                    JSONObject obj = k.getJSONObject(0);
                                    banksuccess = obj.getString("banksuccess");

                                    JSONArray jsonArray= obj.getJSONArray("bankdata");
                                    for(int j= 0; j< jsonArray.length();j++) {
                                        JSONObject data = jsonArray.getJSONObject(j);

                                        String id = data.getString("id");
                                        String bank_name = data.getString("bank_name");

                                        SQLiteDatabase dataBase = db.getWritableDatabase();
                                        Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                                + DatabaseHelper.BANKLIST, null);
                                        if (mCursor1.moveToFirst()) {
                                            do {
                                              String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                                if(id.equals(ids)){
                                                    db.updatedatabank(bank_name,id);
                                                }else{
                                                    db.insertbanklist(bank_name,id);
                                                }
                                            } while (mCursor1.moveToNext());
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
//                            }
                         //center data................................................
                            try {
                                JSONObject obj = k.getJSONObject(1);
                                centersuccess = obj.getString("centersuccess");

                                JSONArray jsonArray= obj.getJSONArray("centerdata");
                                for(int j= 0; j< jsonArray.length();j++) {

                                    JSONObject data = jsonArray.getJSONObject(j);
                                    String id                   =    data.getString("id");
                                    String center_name          =    data.getString("center_name");
                                    String center_name1         =    data.getString("center_name1");
                                    String center_operator      =    data.getString("center_operator");
                                    String center_operator1     =    data.getString("center_operator1");
                                    String center_pin           =    data.getString("center_pin");
                                    String bank_details         =    data.getString("bank_details");
                                    String rate_chart           =    data.getString("rate_chart");
                                    String status               =    data.getString("status");
                                    String created_at           =    data.getString("created_at");
                                    String updated_at           =    data.getString("updated_at");
                                    String user_id              =    data.getString("user_id");
                                    String bank_address         =    data.getString("bank_address");
                                    String branch_name          =    data.getString("branch_name");
                                    String ifsc_code            =    data.getString("ifsc_code");
                                    String account_number       =    data.getString("account_number");
                                    String center_number        =    data.getString("center_number");
                                    String image_url            =    data.getString("image_url");

                                    db.updateCenterMaster(id, center_name, center_name1, center_operator, center_operator1,
                                            center_pin, bank_details, rate_chart, status, created_at, updated_at, user_id, bank_address,
                                            branch_name, ifsc_code, account_number, center_number,image_url);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                         //society data.............................................
                            try {
                                JSONObject obj = k.getJSONObject(2);
                                societysuccess = obj.getString("societysuccess");

                                JSONArray jsonArray= obj.getJSONArray("societydata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id              =   data.getString("id");
                                    String societyName     =   data.getString("society_name");
                                    String societyName1    =   data.getString("society_name1");
                                    String societyAddress  =   data.getString("society_address");
                                    String pinCode         =   data.getString("pin_code");
                                    String unionName       =   data.getString("union_name");
                                    String mobileNumber    =   data.getString("mobile_number");
                                    String bankDetails     =   data.getString("bank_details");
                                    String status          =   data.getString("status");
                                    String createdAt       =   data.getString("created_at");
                                    String updatedAt       =   data.getString("updated_at");
                                    String centername      =   data.getString("center_name");
                                    String society_number  =   data.getString("society_number");

                                    db.updateRMCUmaster(id,societyName,societyName1,societyAddress,pinCode,unionName,
                                            mobileNumber,bankDetails,status,createdAt,updatedAt,centername,society_number);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //farmer data.............................................
                            try {
                                JSONObject obj = k.getJSONObject(3);
                                farmersuccess = obj.getString("farmersuccess");

                                JSONArray jsonArray= obj.getJSONArray("farmerydata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id                =    data.getString("id");
                                    String farmername        =    data.getString("farmername");
                                    String farmer_number     =    data.getString("farmer_number");
                                    String image             =    data.getString("image");
                                    String app_image         =    data.getString("app_image");
                                    String address           =    data.getString("address");
                                    String mobilenumber      =    data.getString("mobilenumber");
                                    String cattlevalue       =    data.getString("cattlevalue");
                                    String dscno             =    data.getString("dscno");
                                    String centernumber      =    data.getString("centernumber");
                                    String rmcunumber        =    data.getString("rmcunumber");
                                    String bankname          =    data.getString("bankname");
                                    String branch            =    data.getString("branch");
                                    String ifscvalue         =    data.getString("ifscvalue");
                                    String accountnumber     =    data.getString("accountnumber");
                                    String state             =    data.getString("state");
                                    String pincode           =    data.getString("pincode");
                                    String country           =    data.getString("country");
                                    String aadharcardnumber  =    data.getString("aadharcardnumber");
                                    String rfid              =    data.getString("rfid");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.TABLE_NAME, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                            if(id.equals(ids)){
                                                db.updateDataFarmer(id,farmername,farmer_number,image,app_image,address,mobilenumber,
                                            cattlevalue,dscno,centernumber,rmcunumber,bankname,branch,ifscvalue,accountnumber,state,pincode,
                                            country, aadharcardnumber,rfid);
                                            }else{
                                                db.insertDataFarmer(id,farmername,farmer_number,image,app_image,address,mobilenumber,
                                            cattlevalue,dscno,centernumber,rmcunumber,bankname,branch,ifscvalue,accountnumber,state,pincode,
                                            country, aadharcardnumber,rfid);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //ratechart1 data.........
                            try {
                                JSONObject obj = k.getJSONObject(4);
                                ratechartsuccess = obj.getString("ratechartsuccess");

                                JSONArray jsonArray= obj.getJSONArray("ratechartdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id_R             =    data.getString("id");
                                    String chartName        =    data.getString("chartname");
                                    String shift            =    data.getString("shift");
                                    String ratechart        =    data.getString("ratechart");
                                    String cattletype       =    data.getString("cattletype");
                                    String charttype        =    data.getString("charttype");
                                    String range            =    data.getString("range");
                                    String fixedrate        =    data.getString("fixedrate");
                                    String ratedate         =    data.getString("ratedate");
                                    String status           =    data.getString("status");
                                    String created_at       =    data.getString("created_at");
                                    String updated_at       =    data.getString("updated_at");
                                    String fatrange_from    =    data.getString("fatrange_from");
                                    String fatrange_to      =    data.getString("fatrange_to");
                                    String fatsnfrange_from =    data.getString("fatsnfrange_from");
                                    String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                                    String effective_date   =    data.getString("effective_date");
                                    String expiration_date  =    data.getString("expiration_date");
                                    String fat_value        =    data.getString("fat_value");
                                    String snf_value        =    data.getString("snf_value");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.listRatechart, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                                            if(id_R.equals(ids)){
                                                db.updateListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }else{
                                                db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                            status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                            effective_date,expiration_date,fat_value,snf_value);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                         // unit master data.......................................
                            try {
                                JSONObject obj = k.getJSONObject(5);
                                unitsuccess = obj.getString("unitsuccess");

                                JSONArray jsonArray= obj.getJSONArray("unitdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id          =   data.getString("id");
                                    String unitname    =   data.getString("unit_name");
                                    String unitname1   =   data.getString("unit_name1");
                                    String scalefactor =   data.getString("scale_factor");
                                    String factorunit  =   data.getString("factor_unit");
                                    String createdunit =   data.getString("created_at");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.unitmaster, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                            if(id.equals(ids)){
                                                db.insertunitmaster(id,unitname,unitname1,scalefactor,factorunit,createdunit);
                                            }else{
                                                db.insertunitmaster(id,unitname,unitname1,scalefactor,factorunit,createdunit);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        //product master...............................................
                            try {
                                JSONObject obj = k.getJSONObject(7);
                                productsuccess = obj.getString("productsuccess");

                                JSONArray jsonArray= obj.getJSONArray("productdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id                  =    data.getString("id");
                                    String product_name        =    data.getString("product_name");
                                    String product_name1       =    data.getString("product_name1");
                                    String record_list         =    data.getString("record_list");
                                    String manufacture_details =    data.getString("manufacture_details");
                                    String unit_scale          =    data.getString("unit_scale");
                                    String subsidy_percentage  =    data.getString("subsidy_percentage");
                                    String created_at          =    data.getString("created_at");
                                    String updated_at          =    data.getString("updated_at");
                                    String status              =    data.getString("status");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.PRODUCT_MASTER, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                            if(id.equals(ids)){
                                               db.updateProductMaster(id,product_name,product_name1,record_list,manufacture_details,
                                                unit_scale,subsidy_percentage,created_at,updated_at,status);
                                            }else{
                                                db.insertProductMaster(id,product_name,product_name1,record_list,manufacture_details,
                                                        unit_scale,subsidy_percentage,created_at,updated_at,status);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        //purchase data........
                            try {
                                JSONObject obj = k.getJSONObject(10);
                                purchasesuccess = obj.getString("purchasesuccess");

                                JSONArray jsonArray= obj.getJSONArray("purchasedata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id               =    data.getString("id");
                                    String purchaseid       =    data.getString("purchaseid");
                                    String productname      =    data.getString("productname");
                                    String productunit      =    data.getString("productunit");
                                    String productquantity  =    data.getString("productquantity");
                                    String productnumber    =    data.getString("productnumber");
                                    String flag             =    data.getString("flag");
                                    String allocatedstock   =    data.getString("allocatedstock");
                                    String sale_rate        =    data.getString("salerate");
                                    String center_id        =    data.getString("center_id");
                                    String created_at       =    data.getString("created_at");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.PURCHASELIST, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                            if(id.equals(ids)){
                                                db.updatePurchaseList(id, purchaseid, productname, productunit, productquantity,
                                                        productnumber, flag, allocatedstock, sale_rate,created_at);
                                            }else{
                                                db.insertPurchaseList(id, purchaseid, productname, productunit, productquantity,
                                                     productnumber, flag, allocatedstock, sale_rate,created_at);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        //shift time................
                            try {
                                JSONObject obj = k.getJSONObject(11);
                                shifttimesuccess = obj.getString("superadminsuccess");

                                JSONArray jsonArray= obj.getJSONArray("superadmindata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);
                                    String id                  =   data.getString("id");
                                    String morningstarttime    =   data.getString("morningstarttime");
                                    String morningendtime      =   data.getString("morningendtime");
                                    String eveningstarttime    =   data.getString("eveningstarttime");
                                    String eveningendtime      =   data.getString("eveningendtime");

                                    db.updateshifttime(id,morningstarttime,morningendtime,eveningstarttime,eveningendtime);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //deduction data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(17);
                                deductionsuccess = obj.getString("deductionsuccess");

                                JSONArray jsonArray= obj.getJSONArray("deductiondata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id              =   data.getString("id");
                                    String deductionName   =   data.getString("deduction_name");
                                    String deductionName1  =   data.getString("deduction_name1");
                                    String deductionType   =   data.getString("deduction_type");
                                    String deductionCycle  =   data.getString("deduction_cycle");
                                    String activationPeriod=   data.getString("activation_period");
                                    String deductionMethod =   data.getString("deduction_method");
                                    String deductionActive =   data.getString("deduction_active");
                                    String createdAt       =   data.getString("created_at");
                                    String updatedAt       =   data.getString("updated_at");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM deductionmaster", null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id"));
                                            if(id.equals(ids)){
                                                 db.updatedeductionmaster(id,deductionName,deductionName1,deductionType,deductionCycle,
                                                   activationPeriod,deductionMethod,deductionActive,createdAt,updatedAt);
                                            }else{
                                                db.insertdeductionmaster(id,deductionName,deductionName1,deductionType,deductionCycle,
                                                 activationPeriod,deductionMethod,deductionActive,createdAt,updatedAt);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //settingcenter data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(12);
                                deductionsuccess = obj.getString("settingsuccess");

                                JSONArray jsonArray= obj.getJSONArray("settingsdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String idss           = data.getString("id");
                                    String rmcuname       = data.getString("rmcuname");
                                    String centername     = data.getString("centername");
                                    String scale          = data.getString("scale");
                                    String analyzer       = data.getString("analyzer");
                                    String cowmorning     = data.getString("cowmorning");
                                    String buffelomorning = data.getString("buffelomorning");
                                    String cowevening     = data.getString("cowevening");
                                    String buffeloevening = data.getString("buffeloevening");
                                    String printingvalue  = data.getString("printingvalue");
                                    String status         = data.getString("status");
                                    String settingvalue   = data.getString("settingvalue");
                                    String headermessage  = data.getString("headermessage");
                                    String footermessage  = data.getString("footermessage");
                                    String snfformula     = data.getString("snfformula");
                                    String littervalue    = data.getString("littervalue");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM listTransactionSetting", null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String centernames = mCursor1.getString(mCursor1.getColumnIndex("centername"));
                                            if(centername.equals(centernames)){
                                                db.updateTransactionSetting(rmcuname, centername, scale, analyzer, cowmorning, buffelomorning, cowevening,
                                            buffeloevening, printingvalue, status, settingvalue, headermessage, footermessage, snfformula,
                                            littervalue,idss);
                                            }else{
                                                db.insertTransactionSetting(rmcuname, centername, scale, analyzer, cowmorning, buffelomorning, cowevening,
                                            buffeloevening, printingvalue, status, settingvalue, headermessage, footermessage, snfformula,
                                            littervalue,idss);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //settingall data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(13);
                                deductionsuccess = obj.getString("settingsuccessall");

                                JSONArray jsonArray= obj.getJSONArray("settingsdataall");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String idss           = data.getString("id");
                                    String rmcuname       = data.getString("rmcuname");
                                    String centername     = data.getString("centername");
                                    String scale          = data.getString("scale");
                                    String analyzer       = data.getString("analyzer");
                                    String cowmorning     = data.getString("cowmorning");
                                    String buffelomorning = data.getString("buffelomorning");
                                    String cowevening     = data.getString("cowevening");
                                    String buffeloevening = data.getString("buffeloevening");
                                    String printingvalue  = data.getString("printingvalue");
                                    String status         = data.getString("status");
                                    String settingvalue   = data.getString("settingvalue");
                                    String headermessage  = data.getString("headermessage");
                                    String footermessage  = data.getString("footermessage");
                                    String snfformula     = data.getString("snfformula");
                                    String littervalue    = data.getString("littervalue");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM listTransactionSetting", null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String centernames = mCursor1.getString(mCursor1.getColumnIndex("centername"));
                                            if(centername.equals(centernames)){
                                                db.updateTransactionSetting(rmcuname, centername, scale, analyzer, cowmorning, buffelomorning, cowevening,
                                                        buffeloevening, printingvalue, status, settingvalue, headermessage, footermessage, snfformula,
                                                        littervalue,idss);
                                            }else{
                                                db.insertTransactionSetting(rmcuname, centername, scale, analyzer, cowmorning, buffelomorning, cowevening,
                                                        buffeloevening, printingvalue, status, settingvalue, headermessage, footermessage, snfformula,
                                                        littervalue,idss);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //ratechart2 data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(14);
                                deductionsuccess = obj.getString("ratechartsuccess2");

                                JSONArray jsonArray= obj.getJSONArray("ratechartdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id_R             =    data.getString("id");
                                    String chartName        =    data.getString("chartname");
                                    String shift            =    data.getString("shift");
                                    String ratechart        =    data.getString("ratechart");
                                    String cattletype       =    data.getString("cattletype");
                                    String charttype        =    data.getString("charttype");
                                    String range            =    data.getString("range");
                                    String fixedrate        =    data.getString("fixedrate");
                                    String ratedate         =    data.getString("ratedate");
                                    String status           =    data.getString("status");
                                    String created_at       =    data.getString("created_at");
                                    String updated_at       =    data.getString("updated_at");
                                    String fatrange_from    =    data.getString("fatrange_from");
                                    String fatrange_to      =    data.getString("fatrange_to");
                                    String fatsnfrange_from =    data.getString("fatsnfrange_from");
                                    String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                                    String effective_date   =    data.getString("effective_date");
                                    String expiration_date  =    data.getString("expiration_date");
                                    String fat_value        =    data.getString("fat_value");
                                    String snf_value        =    data.getString("snf_value");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.listRatechart, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                                            if(id_R.equals(ids)){
                                                db.updateListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }else{
                                                db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //ratechart3 data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(15);
                                deductionsuccess = obj.getString("ratechartsuccess3");

                                JSONArray jsonArray= obj.getJSONArray("ratechartdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id_R             =    data.getString("id");
                                    String chartName        =    data.getString("chartname");
                                    String shift            =    data.getString("shift");
                                    String ratechart        =    data.getString("ratechart");
                                    String cattletype       =    data.getString("cattletype");
                                    String charttype        =    data.getString("charttype");
                                    String range            =    data.getString("range");
                                    String fixedrate        =    data.getString("fixedrate");
                                    String ratedate         =    data.getString("ratedate");
                                    String status           =    data.getString("status");
                                    String created_at       =    data.getString("created_at");
                                    String updated_at       =    data.getString("updated_at");
                                    String fatrange_from    =    data.getString("fatrange_from");
                                    String fatrange_to      =    data.getString("fatrange_to");
                                    String fatsnfrange_from =    data.getString("fatsnfrange_from");
                                    String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                                    String effective_date   =    data.getString("effective_date");
                                    String expiration_date  =    data.getString("expiration_date");
                                    String fat_value        =    data.getString("fat_value");
                                    String snf_value        =    data.getString("snf_value");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.listRatechart, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                                            if(id_R.equals(ids)){
                                                db.updateListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }else{
                                                db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            //ratechart4 data....................................................
                            try {
                                JSONObject obj = k.getJSONObject(16);
                                deductionsuccess = obj.getString("ratechartsuccess4");

                                JSONArray jsonArray= obj.getJSONArray("ratechartdata");
                                for(int j= 0; j< jsonArray.length();j++) {
                                    JSONObject data = jsonArray.getJSONObject(j);

                                    String id_R             =    data.getString("id");
                                    String chartName        =    data.getString("chartname");
                                    String shift            =    data.getString("shift");
                                    String ratechart        =    data.getString("ratechart");
                                    String cattletype       =    data.getString("cattletype");
                                    String charttype        =    data.getString("charttype");
                                    String range            =    data.getString("range");
                                    String fixedrate        =    data.getString("fixedrate");
                                    String ratedate         =    data.getString("ratedate");
                                    String status           =    data.getString("status");
                                    String created_at       =    data.getString("created_at");
                                    String updated_at       =    data.getString("updated_at");
                                    String fatrange_from    =    data.getString("fatrange_from");
                                    String fatrange_to      =    data.getString("fatrange_to");
                                    String fatsnfrange_from =    data.getString("fatsnfrange_from");
                                    String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                                    String effective_date   =    data.getString("effective_date");
                                    String expiration_date  =    data.getString("expiration_date");
                                    String fat_value        =    data.getString("fat_value");
                                    String snf_value        =    data.getString("snf_value");

                                    SQLiteDatabase dataBase = db.getWritableDatabase();
                                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                                            + DatabaseHelper.listRatechart, null);
                                    if (mCursor1.moveToFirst()) {
                                        do {
                                            String ids = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                                            if(id_R.equals(ids)){
                                                db.updateListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }else{
                                                db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                                        status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                                        effective_date,expiration_date,fat_value,snf_value);
                                            }
                                        } while (mCursor1.moveToNext());
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                        Toast.makeText(HomeActivity.this,"Data is updated",Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error: " + error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                params.put("fromdate", fromdate);
                params.put("todate", todate);

                params.put("center_number", center_number);  //021
                params.put("rmcu_number", rmcu_number);    //053
                params.put("center1", center_num);   //21

                params.put("id1", cow_morning); //3
                params.put("id2", buffelo_morning); //42
                params.put("id3", cow_evening);  //44
                params.put("id4", buffelo_evening);  //54

                return params;
            }
        };

        eventoReq.setShouldCache(false);
        eventoReq.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
        requestQueue.add(eventoReq);
    }
}

















