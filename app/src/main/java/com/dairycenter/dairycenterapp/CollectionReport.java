package com.dairycenter.dairycenterapp;

public class CollectionReport {

    String id,farmer_name,farmer_no,cattletype,shift,weight,fat,snf,lacto,rate,amount,dates;


    public String getDates() {
        return dates;
    }

    public void setDates(String dates) {
        this.dates = dates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getFarmer_no() {
        return farmer_no;
    }

    public void setFarmer_no(String farmer_no) {
        this.farmer_no = farmer_no;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getCattletype() {
        return cattletype;
    }

    public void setCattletype(String cattletype) {
        this.cattletype = cattletype;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getSnf() {
        return snf;
    }

    public void setSnf(String snf) {
        this.snf = snf;
    }

    public String getLacto() {
        return lacto;
    }

    public void setLacto(String lacto) {
        this.lacto = lacto;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
