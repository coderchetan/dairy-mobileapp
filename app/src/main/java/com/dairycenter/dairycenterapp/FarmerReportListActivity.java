package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.FarmerListAdapter;
import com.dairycenter.dairycenterapp.Adapter.FarmerReportListAdapter;

import java.util.ArrayList;
import java.util.List;

public class FarmerReportListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    FarmerListAdapter adapter;
    DatabaseHelper db;
    List<Farmerreport> list = new ArrayList<>();

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private EditText mOutEditText;
    private String mConnectedDeviceName = null;
    private StringBuffer mOutStringBuffer;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService4 mChatService = null;
    Button printbutton,connect;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_report_list);

        getSupportActionBar().setTitle("Farmer List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView           = (RecyclerView)findViewById(R.id.recycler_view);
        printbutton            = (Button)findViewById(R.id.print_btn);
        connect                = (Button)findViewById(R.id.connect);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serverIntent = new Intent(FarmerReportListActivity.this, DeviceListActivity4.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        });

        mBluetoothAdapter   =   BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }


        printbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < list.size(); i++) {
                    String farmername = list.get(i).getFarmer_name();
                    String farmerno   = list.get(i).getFarmer_no();
                    String mobno       = list.get(i).getMobno();
                    String address   = list.get(i).getAddress();

                    String message = ("Farmer No:"+farmerno+", "+"Farmer name:"+farmername+", "
                            +"Mobile No:"+mobno+", "+"Address:"+address);

                    sendMessage(message);

                }
            }
        });


        mChatService = new BluetoothChatService4(FarmerReportListActivity.this, mHandler);
        mOutStringBuffer = new StringBuffer("");


        db = new DatabaseHelper(FarmerReportListActivity.this);
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);

        if (mCursor.moveToFirst()) {
            do {

                String farmer_no          = mCursor.getString(mCursor.getColumnIndex("farmer_number"));
                String farmername         =(mCursor.getString(mCursor.getColumnIndex("farmername")));
                String address            =(mCursor.getString(mCursor.getColumnIndex("address")));
                String mobileno           =(mCursor.getString(mCursor.getColumnIndex("mobilenumber")));
                String cattlevalue        =(mCursor.getString(mCursor.getColumnIndex("cattlevalue")));
                String centernumbers       =(mCursor.getString(mCursor.getColumnIndex("centernumber")));
                String rmcunumber         =(mCursor.getString(mCursor.getColumnIndex("rmcunumber")));
                String bankname           =(mCursor.getString(mCursor.getColumnIndex("bankname")));
                String branch             =(mCursor.getString(mCursor.getColumnIndex("branch")));
                String ifscvalue          =(mCursor.getString(mCursor.getColumnIndex("ifscvalue")));
                String accountnumber      =(mCursor.getString(mCursor.getColumnIndex("accountnumber")));
                String state              =(mCursor.getString(mCursor.getColumnIndex("state")));
                String pincode            =(mCursor.getString(mCursor.getColumnIndex("pincode")));
                String country            =(mCursor.getString(mCursor.getColumnIndex("country")));
                String aadharcardnumber   =(mCursor.getString(mCursor.getColumnIndex("aadharcardnumber")));

                if(cattlevalue.equals("buffelo")){
                    cattlevalue = "Buffalo";
                }else if(cattlevalue.equals("cow")){
                    cattlevalue = "Cow";
                }



                Farmerreport farmerreport = new Farmerreport();

                farmerreport.setFarmer_no(farmer_no);
                farmerreport.setFarmer_name(farmername);
                farmerreport.setAddress(address);
                farmerreport.setMobno(mobileno);
                farmerreport.setCattlevalue(cattlevalue);
                farmerreport.setCenternumber(centernumbers);
                farmerreport.setRmcunumber(rmcunumber);
                farmerreport.setBankname(bankname);
                farmerreport.setBranch(branch);
                farmerreport.setIfscvalue(ifscvalue);
                farmerreport.setAccountnumber(accountnumber);
                farmerreport.setState(state);
                farmerreport.setPincode(pincode);
                farmerreport.setCountry(country);
                farmerreport.setAadharcardnumber(aadharcardnumber);


                list.add(farmerreport);
                adapter = new FarmerListAdapter(FarmerReportListActivity.this, list);
                recyclerView.setAdapter(adapter);
            } while (mCursor.moveToNext());
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null);
        }
    }
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService4.STATE_NONE) {
                mChatService.start();
            }
        }
    }

    private void setupChat() {
        mChatService = new BluetoothChatService4(FarmerReportListActivity.this, mHandler);
        mOutStringBuffer = new StringBuffer("");
    }


    @Override
    public synchronized void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) mChatService.stop();
    }



    private void sendMessage(String message) {
        if (mChatService.getState() != BluetoothChatService4.STATE_CONNECTED) {
            Toast.makeText(FarmerReportListActivity.this, "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mChatService.write(send);
            mOutStringBuffer.setLength(0);
        }
    }


    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    return true;
                }
            };

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.d("recv data:",readMessage);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(FarmerReportListActivity.this, "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(FarmerReportListActivity.this, msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(DeviceListActivity4.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    mChatService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {

//                    setupChat();
                } else {
                    Toast.makeText(FarmerReportListActivity.this, "Bluetooth was not enabled", Toast.LENGTH_SHORT).show();
                }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}
