package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.CollectionReportListAdapter;
import com.dairycenter.dairycenterapp.Adapter.FarmerSelectionAdapter;
import com.dairycenter.dairycenterapp.Adapter.NoOfDaysMilkReportAdapter;
import com.dairycenter.dairycenterapp.Adapter.RateChartReportListAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NoOfDaysMilkReportActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    NoOfDaysMilkReportAdapter adapter;
    DatabaseHelper db;
    List<CollectionReport> list = new ArrayList<>();
    String shift;
    String currentdate;
    TextView farmer_name;
    public static EditText textViewtext1;
    public static EditText textViewtext2;
    public static String GetID;
    public static ArrayList<String> id = new ArrayList<String>();
    public static ArrayList<String> firstname= new ArrayList<String>();
    public static ArrayList<String> lastname= new ArrayList<String>();
    public static ListView userList1;
    String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_of_days_milk_report);

        getSupportActionBar().setTitle("No Of Days Milk Supplied");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView           = (RecyclerView)findViewById(R.id.recycler_view);
        farmer_name            = (TextView) findViewById(R.id.farmernamefilter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        farmer_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.clear();
                openDiologue();
            }
        });


        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        farmer_name.addTextChangedListener(textWatcher2);
    }

    private void openDiologue(){
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate(R.layout.layout_dialog,null);
        textViewtext1 = (EditText)view.findViewById(R.id.farmerno);
        textViewtext2 = (EditText) view.findViewById(R.id.farmername12);
        userList1 = (ListView)view.findViewById(R.id.listdialog);

        db = new DatabaseHelper(this);
        db.getReadableDatabase();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.create();
        displayData();
        ListData();
        ClearText();
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(textViewtext2.getText().toString().length() >0) {
                    User u ;
                    u = new User();
                    farmer_name.setText(textViewtext2.getText().toString());

                    db = new DatabaseHelper(NoOfDaysMilkReportActivity.this);
                    SQLiteDatabase dataBase = db.getWritableDatabase();

                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM addcollectionpagedata " +
                            "where farmer_name = ?", new String[]{farmer_name.getText().toString()}, null);
                    if (mCursor.moveToFirst()) {
                        do {
                            String id = mCursor.getString(mCursor.getColumnIndex("id"));
                            String farmer_no = mCursor.getString(mCursor.getColumnIndex("farmer_no"));
                            String farmer_name = mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                            String user_cattle = mCursor.getString(mCursor.getColumnIndex("userCattle"));
                            String shift = mCursor.getString(mCursor.getColumnIndex("shift"));
                            String weight = mCursor.getString(mCursor.getColumnIndex("weightss"));
                            String fat = mCursor.getString(mCursor.getColumnIndex("fatsss"));
                            String snf = mCursor.getString(mCursor.getColumnIndex("snffss"));
                            String lacto = mCursor.getString(mCursor.getColumnIndex("lactoo"));
                            String rate = mCursor.getString(mCursor.getColumnIndex("ratesss"));
                            String amount = mCursor.getString(mCursor.getColumnIndex("amountss"));
                            String dates = mCursor.getString(mCursor.getColumnIndex("dates"));

                            if(user_cattle.equals("buffelo")){
                                user_cattle = "Buffalo";
                            }else if(user_cattle.equals("cow")){
                                user_cattle = "Cow";
                            }

                            CollectionReport collectionReport = new CollectionReport();

                            collectionReport.setId(id);
                            collectionReport.setFarmer_no(farmer_no);
                            collectionReport.setFarmer_name(farmer_name);
                            collectionReport.setCattletype(user_cattle);
                            collectionReport.setShift(shift);
                            collectionReport.setWeight(weight);
                            collectionReport.setFat(fat);
                            collectionReport.setSnf(snf);
                            collectionReport.setLacto(lacto);
                            collectionReport.setRate(rate);
                            collectionReport.setAmount(amount);
                            collectionReport.setDates(dates);

                            list.add(collectionReport);
                            adapter = new NoOfDaysMilkReportAdapter(NoOfDaysMilkReportActivity.this, list);
                            recyclerView.setAdapter(adapter);

                        } while (mCursor.moveToNext());
                    }else if(mCursor.getCount() == 0){
                        list.clear();
                        adapter.notifyDataSetChanged();
                        Toast.makeText(NoOfDaysMilkReportActivity.this, "Record not available", Toast.LENGTH_LONG).show();
                    }
                }else{
                    ClearText();
                }
            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }


    private void displayData() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                GetID = textViewtext1.getText().toString();
                SQLiteDatabase dataBase = db.getWritableDatabase();
                User u;

                Cursor cursor = dataBase.rawQuery("select * from farmermaster where substr(farmer_number,-4) =?",
                        new String[]{String.valueOf(GetID)}, null);

                if (cursor !=null && cursor.getCount() > 0){
                    cursor.moveToFirst();

                    u = new User();
                    u.setCource(cursor.getString(cursor.getColumnIndex("farmername")));


                    textViewtext2.setText(u.getCource());
                    name= textViewtext2.getText().toString();

                    cursor.close();
                    db.close();

                }else {
                    cursor.close();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        textViewtext1.addTextChangedListener(textWatcher);

    }

    public void ClearText() {
        farmer_name.setText("");

    }


    private void ListData() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);

        id.clear();
        firstname.clear();

        if (mCursor.moveToFirst()) {
            do {
                id.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNUMBER)));
                firstname.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNAME)));
            } while (mCursor.moveToNext());
        }

        FarmerSelectionAdapter disadpt = new FarmerSelectionAdapter(this, id, firstname);
        userList1.setAdapter(disadpt);
        userList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ViewGroup vg = (ViewGroup)view;
                TextView txt = (TextView)vg.findViewById(R.id.textviewdg1);

                String no = txt.getText().toString();
                String lastdigit = no.substring((no.length() -4));

                textViewtext1.setText(lastdigit);
            }
        });
        mCursor.close();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}
