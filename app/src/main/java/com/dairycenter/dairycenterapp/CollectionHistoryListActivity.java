package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.CollectionHistoryListAdapter;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabBankDetailes3;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabContactDetailes2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CollectionHistoryListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    CollectionHistoryListAdapter adapter;
    DatabaseHelper db;
    List<CollectionReport> list = new ArrayList<>();
    String shift;
    String currentdate,select_shift;
    TextView date,local,localcattle;
    Spinner s1,cattletype;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private EditText mOutEditText;
    private String mConnectedDeviceName = null;
    private StringBuffer mOutStringBuffer;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService3 mChatService = null;
    Button printbutton,connect;
    String days,Months;
    TextView start_date,end_date,localcattle_local;
    ImageView Simg, Eimg;
    Button Viewreport;
    private DatePickerDialog.OnDateSetListener mDateSetListener1;
    private DatePickerDialog.OnDateSetListener mDateSetListener2;

    Date mdate;
    String datess1,datess2;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_history_list);

        getSupportActionBar().setTitle("Collection Transaction History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView      =     (RecyclerView) findViewById(R.id.recycler_view);
        start_date        =     (TextView) findViewById(R.id.sdte);
        end_date          =     (TextView) findViewById(R.id.enddte);
        Simg              =     (ImageView) findViewById(R.id.startimg);
        Eimg              =     (ImageView) findViewById(R.id.endimg);
        Viewreport        =     (Button) findViewById(R.id.viewreport);
        local             =     (TextView) findViewById(R.id.local);
        localcattle       =     (TextView) findViewById(R.id.localcattle);
        s1                =     (Spinner) findViewById(R.id.shift_spinner);
        cattletype        =     (Spinner) findViewById(R.id.cattle_spinner);
        printbutton       =     (Button) findViewById(R.id.print_btn);
        connect           =     (Button) findViewById(R.id.connect);
        localcattle_local =     (TextView) findViewById(R.id.localcattle_local);


        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serverIntent = new Intent(CollectionHistoryListActivity.this, DeviceListActivity3.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        });

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
            finish();
        }

        printbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < list.size(); i++) {
                    String id = list.get(i).getId();
                    String farmername = list.get(i).getFarmer_name();
                    String farmerno = list.get(i).getFarmer_no();
                    String shift = list.get(i).getShift();
                    String weight = list.get(i).getWeight();
                    String fat = list.get(i).getFat();
                    String snf = list.get(i).getSnf();
                    String lacto = list.get(i).getLacto();
                    String rate = list.get(i).getRate();
                    String amount = list.get(i).getAmount();


                    String message = ("Id:" + id + ", " + "Farmer name:" + farmername + ", " + "Farmer No:" + farmerno + ", "
                            + "Shift:" + shift + ", " + "Weight:" + weight + "," + "Fat:" + fat + ", "
                            + "Snf:" + snf + ", " + "Lacto:" + lacto + ", " + "Rate:" + rate + ", " + "Amount" + amount);

                    sendMessage(message);

                }
            }
        });


        mChatService = new BluetoothChatService3(CollectionHistoryListActivity.this, mHandler);
        mOutStringBuffer = new StringBuffer("");

        Simg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CollectionHistoryListActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                if (month < 10) {
                    Months = "0" + month;
                } else {
                    Months = String.valueOf(month);
                }

                if (day < 10) {
                    days = "0" + day;
                } else {
                    days = String.valueOf(day);
                }
                String dates = days + "-" + Months + "-" + year;
                start_date.setText(dates);


                String str = start_date.getText().toString();
                SimpleDateFormat formats = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    mdate = formats.parse(str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                datess1 = simpleDateFormat.format(mdate);

            }
        };

        Eimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        CollectionHistoryListActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener2,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                if (month < 10) {
                    Months = "0" + month;
                } else {
                    Months = String.valueOf(month);
                }
                if (day < 10) {
                    days = "0" + day;
                } else {
                    days = String.valueOf(day);
                }

                String dates = days + "-" + Months + "-" + year;
                end_date.setText(dates);

                String str = end_date.getText().toString();
                SimpleDateFormat formats = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    mdate = formats.parse(str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                datess2 = simpleDateFormat.format(mdate);
            }
        };


        List<String> list2 = new ArrayList<String>();
        list2.add(0, "Select");
        list2.add("Morning");
        list2.add("Evening");
        list2.add("Morning/Evening");
        final ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(dataAdapter2);
        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                select_shift = parent.getItemAtPosition(position).toString();
                local.setText(select_shift);
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        List<String> list3 = new ArrayList<String>();
        list3.add(0, "Select");
        list3.add("Cow");
        list3.add("Buffalo");
        list3.add("Both");
        final ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, list3);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cattletype.setAdapter(dataAdapter3);
        cattletype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
              String  select_cattletype = parent.getItemAtPosition(position).toString();
              if(select_cattletype.equals("Cow")){
                    localcattle.setText("cow");
                }else if(select_cattletype.equals("Buffalo")){
                  localcattle.setText("buffelo");
              }else if(select_cattletype.equals("Both")){
                  localcattle.setText("Both");
              }else{
                  localcattle.setText(select_cattletype);
              }

                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        localcattle_local.setText(local.toString()+localcattle.toString());



        Viewreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (start_date.getText().toString().isEmpty()) {
                    start_date.setError("Please select start date");
                    start_date.requestFocus();
                } else if (end_date.getText().toString().isEmpty()) {
                    end_date.setError("Please select end date");
                    end_date.requestFocus();
                } else {
                    db = new DatabaseHelper(CollectionHistoryListActivity.this);
                    SQLiteDatabase dataBase = db.getWritableDatabase();

                    list.clear();

                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM addcollectionpagedata " +
                            "where reversedate between? AND?", new String[]{datess1,
                            datess2}, null);

                    if (mCursor.getCount() == 0) {
                        Toast.makeText(CollectionHistoryListActivity.this, "Record not available", Toast.LENGTH_LONG).show();
                    }
                    else if(!local.getText().toString().equals("Select") && localcattle.getText().toString().equals("Select")) {
                        if (mCursor.moveToFirst()) {
                            do {
                                if (local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("shift"))) ||
                                        local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("localstring")))) {

                                    String id           = mCursor.getString(mCursor.getColumnIndex("id"));
                                    String farmer_no    = mCursor.getString(mCursor.getColumnIndex("farmer_no"));
                                    String farmer_name  = mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                                    String user_cattle  = mCursor.getString(mCursor.getColumnIndex("userCattle"));
                                    String date         = mCursor.getString(mCursor.getColumnIndex("dates"));
                                    String shift        = mCursor.getString(mCursor.getColumnIndex("shift"));
                                    String weight       = mCursor.getString(mCursor.getColumnIndex("weightss"));
                                    String fat          = mCursor.getString(mCursor.getColumnIndex("fatsss"));
                                    String snf          = mCursor.getString(mCursor.getColumnIndex("snffss"));
                                    String lacto        = mCursor.getString(mCursor.getColumnIndex("lactoo"));
                                    String rate         = mCursor.getString(mCursor.getColumnIndex("ratesss"));
                                    String amount       = mCursor.getString(mCursor.getColumnIndex("amountss"));

                                    if(user_cattle.equals("buffelo")){
                                        user_cattle = "Buffalo";
                                    }else if(user_cattle.equals("cow")){
                                        user_cattle = "Cow";
                                    }

                                    CollectionReport collectionReport = new CollectionReport();

                                    collectionReport.setId(id);
                                    collectionReport.setFarmer_no(farmer_no);
                                    collectionReport.setFarmer_name(farmer_name);
                                    collectionReport.setCattletype(user_cattle);
                                    collectionReport.setShift(shift);
                                    collectionReport.setWeight(weight);
                                    collectionReport.setFat(fat);
                                    collectionReport.setSnf(snf);
                                    collectionReport.setLacto(lacto);
                                    collectionReport.setRate(rate);
                                    collectionReport.setAmount(amount);
                                    collectionReport.setDates(date);

                                    list.add(collectionReport);
                                    adapter = new CollectionHistoryListAdapter(CollectionHistoryListActivity.this, list);
                                    recyclerView.setAdapter(adapter);

                                    start_date.setError(null);
                                    end_date.setError(null);
                                    ((TextView) s1.getChildAt(0)).setError(null);
                                }
                            } while (mCursor.moveToNext());
                        }
                    }else if(!localcattle.getText().toString().equals("Select") && local.getText().toString().equals("Select")){
                        if (mCursor.moveToFirst()) {
                            do {
                                if (localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("userCattle"))) ||
                                        localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("cattlebothstring")))) {

                                    String id           = mCursor.getString(mCursor.getColumnIndex("id"));
                                    String farmer_no    = mCursor.getString(mCursor.getColumnIndex("farmer_no"));
                                    String farmer_name  = mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                                    String user_cattle  = mCursor.getString(mCursor.getColumnIndex("userCattle"));
                                    String date         = mCursor.getString(mCursor.getColumnIndex("dates"));
                                    String shift        = mCursor.getString(mCursor.getColumnIndex("shift"));
                                    String weight       = mCursor.getString(mCursor.getColumnIndex("weightss"));
                                    String fat          = mCursor.getString(mCursor.getColumnIndex("fatsss"));
                                    String snf          = mCursor.getString(mCursor.getColumnIndex("snffss"));
                                    String lacto        = mCursor.getString(mCursor.getColumnIndex("lactoo"));
                                    String rate         = mCursor.getString(mCursor.getColumnIndex("ratesss"));
                                    String amount       = mCursor.getString(mCursor.getColumnIndex("amountss"));

                                    if(user_cattle.equals("buffelo")){
                                        user_cattle = "Buffalo";
                                    }else if(user_cattle.equals("cow")){
                                        user_cattle = "Cow";
                                    }

                                    CollectionReport collectionReport = new CollectionReport();

                                    collectionReport.setId(id);
                                    collectionReport.setFarmer_no(farmer_no);
                                    collectionReport.setFarmer_name(farmer_name);
                                    collectionReport.setCattletype(user_cattle);
                                    collectionReport.setShift(shift);
                                    collectionReport.setWeight(weight);
                                    collectionReport.setFat(fat);
                                    collectionReport.setSnf(snf);
                                    collectionReport.setLacto(lacto);
                                    collectionReport.setRate(rate);
                                    collectionReport.setAmount(amount);
                                    collectionReport.setDates(date);

                                    list.add(collectionReport);
                                    adapter = new CollectionHistoryListAdapter(CollectionHistoryListActivity.this, list);
                                    recyclerView.setAdapter(adapter);

                                    start_date.setError(null);
                                    end_date.setError(null);
                                    ((TextView) cattletype.getChildAt(0)).setError(null);
                                }
                            } while (mCursor.moveToNext());
                        }
                    }else if(!localcattle.getText().toString().equals("Select") && !local.getText().toString().equals("Select")){
                        if (mCursor.moveToFirst()) {
                            do {
                                if(local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("shift")))
                                        && localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("userCattle")))
                                            || local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("localstring")))
                                        && localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("userCattle")))
                                            || local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("shift")))
                                        && localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("cattlebothstring")))
                                            ||local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("localstring")))
                                        && localcattle.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("cattlebothstring")))){

                                    Log.d("www","www");

                                    String id           = mCursor.getString(mCursor.getColumnIndex("id"));
                                    String farmer_no    = mCursor.getString(mCursor.getColumnIndex("farmer_no"));
                                    String farmer_name  = mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                                    String user_cattle  = mCursor.getString(mCursor.getColumnIndex("userCattle"));
                                    String date         = mCursor.getString(mCursor.getColumnIndex("dates"));
                                    String shift        = mCursor.getString(mCursor.getColumnIndex("shift"));
                                    String weight       = mCursor.getString(mCursor.getColumnIndex("weightss"));
                                    String fat          = mCursor.getString(mCursor.getColumnIndex("fatsss"));
                                    String snf          = mCursor.getString(mCursor.getColumnIndex("snffss"));
                                    String lacto        = mCursor.getString(mCursor.getColumnIndex("lactoo"));
                                    String rate         = mCursor.getString(mCursor.getColumnIndex("ratesss"));
                                    String amount       = mCursor.getString(mCursor.getColumnIndex("amountss"));

                                    if(user_cattle.equals("buffelo")){
                                        user_cattle = "Buffalo";
                                    }else if(user_cattle.equals("cow")){
                                        user_cattle = "Cow";
                                    }

                                    CollectionReport collectionReport = new CollectionReport();

                                    collectionReport.setId(id);
                                    collectionReport.setFarmer_no(farmer_no);
                                    collectionReport.setFarmer_name(farmer_name);
                                    collectionReport.setCattletype(user_cattle);
                                    collectionReport.setShift(shift);
                                    collectionReport.setWeight(weight);
                                    collectionReport.setFat(fat);
                                    collectionReport.setSnf(snf);
                                    collectionReport.setLacto(lacto);
                                    collectionReport.setRate(rate);
                                    collectionReport.setAmount(amount);
                                    collectionReport.setDates(date);

                                    list.add(collectionReport);
                                    adapter = new CollectionHistoryListAdapter(CollectionHistoryListActivity.this, list);
                                    recyclerView.setAdapter(adapter);

                                    start_date.setError(null);
                                    end_date.setError(null);
                                    ((TextView) cattletype.getChildAt(0)).setError(null);
                                }
                            } while (mCursor.moveToNext());
                        }
                    }
                }
            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        } else {
            if (mChatService == null);
        }
    }
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mChatService != null) {
            if (mChatService.getState() == BluetoothChatService3.STATE_NONE) {
                mChatService.start();
            }
        }
    }


    @Override
    public synchronized void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) mChatService.stop();
    }



    private void sendMessage(String message) {
        if (mChatService.getState() != BluetoothChatService3.STATE_CONNECTED) {
            Toast.makeText(CollectionHistoryListActivity.this, "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mChatService.write(send);
            mOutStringBuffer.setLength(0);

        }
    }


    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    return true;
                }
            };

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.d("recv data:",readMessage);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(CollectionHistoryListActivity.this, "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(CollectionHistoryListActivity.this, msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(DeviceListActivity3.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    mChatService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
//                    openDialog();
//                    setupChat();
                } else {
                    Toast.makeText(CollectionHistoryListActivity.this, "Bluetooth was not enabled", Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}