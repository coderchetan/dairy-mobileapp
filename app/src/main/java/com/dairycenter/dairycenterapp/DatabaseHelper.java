package com.dairycenter.dairycenterapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String DEDU_REGI = "deductionregi";
    private static final String TAG = "DatabaseHelper";
    public static final String TABLE_NAME = "farmermaster";
    public static final String centeruser = "centeruser";
    public static final String unitmaster = "unitmaster";
    public static final String rmcumaster = "rmcumaster";
    public static final String FARMER_RAGI = "farmerregi";
    private static final String deductionmaster = "deductionmaster";
    public static final String listRatechart = "listRatechart";
    public static final String listTransactionSetting = "listTransactionSetting";
    public static final String ratechartmatrix = "ratechartmatrix";
    public static final String addCollectionData = "addcollectionpagedata";
    public static final String closeShiftData = "closeshift";
    public static final String PRODUCT_MASTER = "productmaster";
    public static final String CENTER_MASTER = "centermaster";
    public static final String COLLECTIONMODE = "collectionmode";
    public static final String COLLECTIONDATAREPORT= "addcollectionpagedata";
    public static final String PURCHASELIST= "purchaselist";
    public static final String ADDPURCHASEDATA= "addpurchasedata";
    public static final String TRUCKSHEET= "trucksheet";
    public static final String BANKLIST= "banklist";
    public static final String downloadsettingdata= "downloadsettingdata";
    public static final String Salesavailablequantity= "Salesavailablequantity";
    public static final String trucksheet= "trucksheet";
    public static final String updateddate= "updateddatadate";

    public static final String shifttime= "shifttime";

    public static final String FARMERNAME = "farmername";
    public static final String FARMERNUMBER = "farmer_number";

    public static final String ViewPurchase = "savepurchase";
    public static final String ViewProname = "productname";
    public static final String ViewProno = "productno";
    public static final String Viewquan = "quantity";
    public static final String Viewproduct = "productunit";


    public static final String IDs = "id";
    public static final String DUDUCTION_TYPE = "typeofdeduction";
    public static final String FARMER_NUMBER = "farmerNumber";
    public static final String DEDUCTION_CYCLE = "deductioncycle";
    public static final String DEDUCTION_Method = "deductionmethod";
    public static final String DEDUCTION_AMOUNT = "amount";
    public static final String DEDUCTION_BILLING_CYCLE = "billingcycle";
    public static final String DEDUCTION_REMARK = "remark";


    public static final String weight = "weightss";
    public static final String fat = "fatsss";
    public static final String lacto = "lactoo";
    public static final String snf = "snffss";
    public static final String protien = "protiens";
    public static final String water = "waterss";
    public static final String date = "dates";
    public static final String cattletype = "userCattle";
    public static final String shift = "shift";
    public static final String farmer_no = "farmer_no";

    public static final String rmcuname = "societyName";
    public static final String cenetrname = "center_name";
    public static final String Bluetooth = "bluetooth";
    public static final String text = "result";


    public DatabaseHelper(@Nullable Context context) {
        super(context, "DairyDatabase1.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String tableCenteruser = "create table centeruser(id text,username text,centerid text,centername text)";

        String tableFarmerMaster = "create table farmermaster(id text,farmername text,farmer_number text," +
                "image text,app_image text,address text,mobilenumber text,cattlevalue text," +
                "dscno text,centernumber text,rmcunumber text,bankname text,branch text,ifscvalue text," +
                "accountnumber text,state text,pincode text,\n" +
                "                country text, aadharcardnumber text,rfid text)";
        String tableUnitMaster = "create table unitmaster(id text, unitname text,unitname1 text,scalefactor text,factorunit text,createdunit text)";
        String tableRMCUMaster = "create table rmcumaster(id text,societyName text ,societyName1 text," +
                "societyAddress text,pinCode text,unionName text,mobileNumber text,bankDetails text," +
                "status text,createdAt text,updatedAt text,centername text,society_number text)";
        String tableDeductionMaster = "create table deductionmaster(id text,deductionName text,deductionName1 text,deductionType text,deductionCycle text,activationPeriod text,deductionMethod text,deductionActive text,createdAt text,updatedAt text)";
        String tableListRateChart = "create table listRatechart(id_R text,chartName text,shift text," +
                "ratechart text,cattletype text,charttype text,range text,fixedrate text,ratedate text,status text," +
                "created_at text,updated_at text,fatrange_from text,fatrange_to text,fatsnfrange_from text,fatsnfrange_to text," +
                "effective_date text,expiration_date text,fat_value text,snf_value text)";
        String tableTransactionSetting = "create table listTransactionSetting(rmcuname text,centername text,scale text," +
                "analyzer text,cowmorning text,buffelomorning text,cowevening text," +
                "buffeloevening text,printingvalue text,status text,settingvalue text,headermessage text," +
                "footermessage text,snfformula text,littervalue text, idss text)";
        String tableRateChartMatrix = "create table ratechartmatrix(id text,ratechart_id text,fat_range text," +
                "snf_lacto_range text,rate_value text,status text,created_at text," +
                "updated_at text,fatnewrange text)";

        String tableFarmerRegi = "create table farmerregi(id integer primary key autoincrement,firstname text,middlename text,lastname text,firstReligionName text,middleReligionName text,lastReligionName text,\n" +
                  "address text,contactNo text,adhar_no text,city text,state text,pincode text,bank_name text,branch_name text,account_no text,ifsc_code text,noofcow text,noofbuffelo text,noofcattle text,disable text,datas blob)";
        String tabledb = "create table addimg(data blob)";

        String tablepos = "create table posadds(pos text)";

        String tableadds = "create table adds(id integer primary key autoincrement,productname text," +
                "productno text,productunit text,quantity text)";

        String tableDeductionregi = "create table deductionregi(id integer primary key autoincrement,nameofdeductions text,typeofdeduction text," +
                "deductioncycle text,deductionmethod text,amount text,billingcycle text,remark text,farmerNumber text,status text)";
        String tableRepayment = "create table deductionrepayment(Reid integer primary key autoincrement,Retypeofdeduction text," +
                "Redeductioncycle text,Redeductionmethod text,Reamount text,Rebillingcycle text,Reremark text,RefarmerNumber text," +
                "Retransactionid text,RedeductionRepayment text)";

        String tableAddCollectionData = "create table addcollectionpagedata(id integer primary key autoincrement,userCattle text," +
                "dates text,shift text,farmer_no text,farmer_name text,weightss text,fatsss text,lactoo text,snffss text,protiens text," +
                "waterss text,ratesss text,amountss text,localstring text,reversedate text,date_shift text,cattlebothstring text)";

        String tableCloseShift = "create table closeshift(CLOSE_SHIFT text,dateClose text,shiftclose text,dateshift text)";

        String tableProductMaster = "create table productmaster(id text,product_name text,product_name1 text," +
                "record_list text,manufacture_details text,unit_scale text,subsidy_percentage text,created_at text," +
                "updated_at text,status text)";

        String tableCenterMaster = "create table centermaster(id text,center_name text,center_name1 text," +
                "center_operator text,center_operator1 text,center_pin text,bank_details text,rate_chart text," +
                "status text,created_at text,updated_at text,user_id text,bank_address text,branch_name text,ifsc_code text," +
                "account_number text,center_number text,image_url text)";

        String tablepurchaseMaster = "create table purchase(id integer primary key autoincrement,proname text,prono text,quan text)";
        String tableinsetsave = "create table savepurchase(id integer primary key autoincrement,productname text," +
                "productno text,productunit text,quantity text)";

        String tablesalestransaction = "create table savesalestrans(id integer primary key autoincrement,productname text," +
                "productno text,rate text,quantity text)";

         String tablelistpurchase = "create table purchaselist(id text,purchaseid text," +
                "productname text,productunit text,productquantity text," +
                "productnumber text,flag text,allocatedstock text,sale_rate text,created_at text)";

         String bluetooths = "create table bluetooth(pos integer,result text)";
        String collectionmode = "create table collectionmode(st1 text,st2 text)";
        String collectionmode2 = "create table collectionmode2(wenscale2 text)";

        String tablesecondpurchase = "create table secondpurchase(id integer primary key autoincrement,purchase_centerid text," +
                "purchase_rmcuid text,purchase_centernames text,purchase_rmcunames text,listsize text)";


        String tableaddpurchasedata = "create table addpurchasedata(id integer primary key autoincrement,proname text," +
                "prounit text,quan text,prono text)";

        String tablecalibrationdata = "create table calibrationdata(id integer primary key autoincrement,times text," +
                "dates text,centernames text,rmcunames text,caliremarks text ,person_names text)";

        String tablebanklist = "create table banklist(bank_name text,id text)";

        String tabletruckshhet = "create table trucksheet(vehicle_nos text ,driver_names text ,contact_nos text ,shiftclose_dates text," + "shiftclose_times text ,shift_wt_cows text,\n" +
                "                        shift_wt_buffalos text,shift_fat_cows text,shift_fat_buffalos text,shift_snf_cows text," +
                "shift_snf_buffalos text,\n" +
                "                        shift_sum_cow_buffalos text ,shiftclose text)";

        String tabledownloadsetting = "create table downloadsettingdata(id text, weighting_scale text,analyzer_scale text,cowmorning text,buffelomorning text," +
                "cowevening text,buffeloevening text,littervalue text)";

        String tableavailbalequantity = "create table Salesavailablequantity(id integer primary key autoincrement,productname text,productnumber text,rate text," +
                "avilable_quantity text,created_at text,sumavilable_quantity text,status text)";

        String tablesalesaddinactivedata = "create table salesaddinactivedata(count text,proname text,productnumber text,rate text,quantity text,amount text,status text)";

        String tableshifttime = "create table shifttime(id text, morningstarttime text,morningendtime text,eveningstarttime text,eveningendtime text)";

        String tablecollectionsynchronizedata = "create table collectionsynch(id integer primary key autoincrement, reversedate text," +
                "userCattles text,shift_small text,farmer_name text,weightss text,fatsss text,lactoo text,snffss text,protiens text," +
                "waterss text,ratesss text,amountss text,farmreNoCompltes text)";

        String tablecloseshiftsynchronizedata = "create table closeshiftsynch(id integer primary key autoincrement, vehicle_nos text," +
                "driver_names text,contact_nos text,shiftclose_dates text,shiftclose_times text,shift_wt_cows text," +
                "shift_wt_buffalos text,shift_fat_cows text,shift_fat_buffalos text,shift_snf_cows text,shift_snf_buffalos text," +
                "shift_sum_cow_buffalos text,shiftclose text,Shistcenter_number text)";

        String tablefarmerregisynchronizedata = "create table farmerregisynch(id integer primary key autoincrement, firstname text," +
                "lastname text,address text,contactNo text,adhar_no text,city text,state text,pincode text,bankname text," +
                "branchname text,accountno text,ifsccode text,noofcow text,noofbuffelo text,noofcattle text,centernos text," +
                "rmcunos text,imgString,cattletype,rfid text)";

        String tabledate = "create table updateddatadate(currentdate text,currenttime text)";


        db.execSQL(tabledate);
        db.execSQL(tableshifttime);
        db.execSQL(tablecloseshiftsynchronizedata);
        db.execSQL(tablefarmerregisynchronizedata);
        db.execSQL(tableavailbalequantity);
        db.execSQL(tablesalesaddinactivedata);
        db.execSQL(tableCenteruser);
        db.execSQL(tableFarmerMaster);
        db.execSQL(tableUnitMaster);
        db.execSQL(tableRMCUMaster);
        db.execSQL(tableDeductionMaster);
        db.execSQL(tableFarmerRegi);
        db.execSQL(tabledb);
        db.execSQL(tableDeductionregi);
        db.execSQL(tableRepayment);
        db.execSQL(tableListRateChart);
        db.execSQL(tableTransactionSetting);
        db.execSQL(tableRateChartMatrix);
        db.execSQL(tableAddCollectionData);
        db.execSQL(tableCloseShift);
        db.execSQL(tableProductMaster);
        db.execSQL(tableCenterMaster);
        db.execSQL(tablepurchaseMaster);
        db.execSQL(tablelistpurchase);
        db.execSQL(tableinsetsave);
        db.execSQL(bluetooths);
        db.execSQL(collectionmode);
        db.execSQL(collectionmode2);
        db.execSQL(tablesalestransaction);
        db.execSQL(tablesecondpurchase);
        db.execSQL(tableaddpurchasedata);
        db.execSQL(tablecalibrationdata);
        db.execSQL(tableadds);
        db.execSQL(tablepos);
        db.execSQL(tablebanklist);
        db.execSQL(tabletruckshhet);
        db.execSQL(tabledownloadsetting);
        db.execSQL(tablecollectionsynchronizedata);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    @Override
    public void onOpen(SQLiteDatabase db){
        super.onOpen(db);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            db.disableWriteAheadLogging();
        }
    }

    public void insertblue(Integer pos,String result){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("pos", pos);
        values.put("result", result);

        sqLiteDatabase.insert("bluetooth", null, values);
    }


    public void adddownload_datadate(String currentdate,String currenttime){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("currentdate", currentdate);
        values.put("currenttime", currenttime);

        sqLiteDatabase.insert("updateddatadate", null, values);
    }



    public void insertshifttime(String id,String morningstarttime,String morningendtime,String eveningstarttime,String eveningendtime){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("morningstarttime", morningstarttime);
        values.put("morningendtime", morningendtime);
        values.put("eveningstarttime", eveningstarttime);
        values.put("eveningendtime", eveningendtime);

        sqLiteDatabase.insert("shifttime", null, values);
    }

    public void salesaddinactivedata(int count,String proname,String productnumber,String rate,String quantity,String amount,
                                     String status){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("count", count);
        values.put("proname", proname);
        values.put("productnumber", productnumber);
        values.put("rate", rate);
        values.put("quantity", quantity);
        values.put("amount", amount);
        values.put("status", status);

        sqLiteDatabase.insert("salesaddinactivedata", null, values);
    }

    public void addsychronizedata(Integer id,String reversedate,String userCattle,String shift_small,String farmer_name,String weightss,
                                     String fatsss, String lactoo,String snffss, String protiens,String waterss,String ratesss,String amountss,
                                     String farmreNoCompltes){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("reversedate", reversedate);
        values.put("userCattles", userCattle);
        values.put("shift_small", shift_small);
        values.put("farmer_name", farmer_name);
        values.put("weightss", weightss);
        values.put("fatsss", fatsss);
        values.put("lactoo", lactoo);
        values.put("snffss", snffss);
        values.put("protiens", protiens);
        values.put("waterss", waterss);
        values.put("ratesss", ratesss);
        values.put("amountss", amountss);
        values.put("farmreNoCompltes", farmreNoCompltes);

        sqLiteDatabase.insert("collectionsynch", null, values);
    }

    public void addcloseShiftSynchronizedata(Integer id,String vehicle_nos,String driver_names,String contact_nos,String shiftclose_dates,
                                             String shiftclose_times,String shift_wt_cows, String shift_wt_buffalos,
                                             String shift_fat_cows, String shift_fat_buffalos,String shift_snf_cows,
                                             String shift_snf_buffalos,String shift_sum_cow_buffalos, String shiftclose,String Shistcenter_number){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("vehicle_nos", vehicle_nos);
        values.put("driver_names", driver_names);
        values.put("contact_nos", contact_nos);
        values.put("shiftclose_dates", shiftclose_dates);
        values.put("shiftclose_times", shiftclose_times);
        values.put("shift_wt_cows", shift_wt_cows);
        values.put("shift_wt_buffalos", shift_wt_buffalos);
        values.put("shift_fat_cows", shift_fat_cows);
        values.put("shift_fat_buffalos", shift_fat_buffalos);
        values.put("shift_snf_cows", shift_snf_cows);
        values.put("shift_snf_buffalos", shift_snf_buffalos);
        values.put("shift_sum_cow_buffalos", shift_sum_cow_buffalos);
        values.put("shiftclose", shiftclose);
        values.put("Shistcenter_number", Shistcenter_number);

        sqLiteDatabase.insert("closeshiftsynch", null, values);
    }



    public void addfarmerregisynch(Integer id,String firstname,String lastname,String address,String contactNo,String adhar_no,String city, String state,
                                   String pincode, String bankname,String branchname,String accountno,String ifsccode, String noofcow,
                                   String noofbuffelo, String noofcattle, String centernos, String rmcunos,String imgString,String cattletype,String rfid){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("firstname", firstname);
        values.put("lastname", lastname);
        values.put("rfid", rfid);
        values.put("address", address);
        values.put("contactNo", contactNo);
        values.put("adhar_no", adhar_no);
        values.put("city", city);
        values.put("state", state);
        values.put("pincode", pincode);
        values.put("bankname", bankname);
        values.put("branchname", branchname);
        values.put("accountno", accountno);
        values.put("ifsccode", ifsccode);
        values.put("noofcow", noofcow);
        values.put("noofbuffelo", noofbuffelo);
        values.put("noofcattle", noofcattle);
        values.put("centernos", centernos);
        values.put("rmcunos", rmcunos);
        values.put("imgString", imgString);
        values.put("cattletype", cattletype);

        sqLiteDatabase.insert("farmerregisynch", null, values);
    }



    public void insertavailbalequantity(Integer id,String productname,String productnumber,String rate,String avilable_quantity,String created_at,
                                        Integer sumavilable_quantity,String status){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("productname", productname);
        values.put("productnumber", productnumber);
        values.put("rate", rate);
        values.put("avilable_quantity", avilable_quantity);
        values.put("created_at", created_at);
        values.put("sumavilable_quantity", sumavilable_quantity);
        values.put("status", status);

        sqLiteDatabase.insert("Salesavailablequantity", null, values);
    }

    public void insertbanklist(String bank_name,String id){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("bank_name", bank_name);
        values.put("id", id);

        sqLiteDatabase.insert("banklist", null, values);
    }

    public void insertdownloadsettingdata(String id,String weighting_scale,String analyzer_scale,String cowmorning,String buffelomorning,
                                          String cowevening,String buffeloevening,String littervalue){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("id", id);
        values.put("weighting_scale", weighting_scale);
        values.put("analyzer_scale", analyzer_scale);
        values.put("cowmorning", cowmorning);
        values.put("buffelomorning", buffelomorning);
        values.put("cowevening", cowevening);
        values.put("buffeloevening", buffeloevening);
        values.put("littervalue", littervalue);

        sqLiteDatabase.insert("downloadsettingdata", null, values);
    }


    public void insertcallibrationreport(Integer id,String times,String dates,String centernames,String rmcunames,
                                         String caliremarks, String person_names){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("times", times);
        values.put("dates", dates);
        values.put("centernames", centernames);
        values.put("rmcunames", rmcunames);
        values.put("caliremarks", caliremarks);
        values.put("person_names", person_names);

        sqLiteDatabase.insert("calibrationdata", null, values);
    }

    public void inserttrucksheetreport(String vehicle_nos,String driver_names,String contact_nos,String shiftclose_dates,String shiftclose_times,
                                       String shift_wt_cows,String shift_wt_buffalos,String shift_fat_cows,String shift_fat_buffalos,String shift_snf_cows,
                                       String shift_snf_buffalos, String shift_sum_cow_buffalos,String shiftclose){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("vehicle_nos", vehicle_nos);
        values.put("driver_names", driver_names);
        values.put("contact_nos", contact_nos);
        values.put("shiftclose_dates", shiftclose_dates);
        values.put("shiftclose_times", shiftclose_times);
        values.put("shift_wt_cows", shift_wt_cows);
        values.put("shift_wt_buffalos", shift_wt_buffalos);
        values.put("shift_fat_cows", shift_fat_cows);
        values.put("shift_fat_buffalos", shift_fat_buffalos);
        values.put("shift_snf_cows", shift_snf_cows);
        values.put("shift_snf_buffalos", shift_snf_buffalos);
        values.put("shift_sum_cow_buffalos", shift_sum_cow_buffalos);
        values.put("shiftclose", shiftclose);

        sqLiteDatabase.insert("trucksheet", null, values);
    }

    public void insetcollectionmode(String st1,String st2){

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("st1", st1);
        values.put("st2", st2);

        sqLiteDatabase.insert("collectionmode", null, values);

        Log.d("dddd", String.valueOf(values));


    }

    public void insertPurchaseList(String id,String purchaseid,String productname,String productunit,String productquantity,
                                   String productnumber, String flag, String allocatedstock,String sale_rate,String created_at){


        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("purchaseid", purchaseid);
        values.put("productname", productname);
        values.put("productunit", productunit);
        values.put("productquantity", productquantity);
        values.put("productnumber", productnumber);
        values.put("flag", flag);
        values.put("allocatedstock", allocatedstock);
        values.put("sale_rate", sale_rate);
        values.put("created_at", created_at);

        sqLiteDatabase.insert("purchaselist", null, values);

    }


    public void insertCloseShift(String CLOSE_SHIFT,String dateClose,String shiftclose, String dateshift){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("CLOSE_SHIFT", CLOSE_SHIFT);
        values.put("dateClose", dateClose);
        values.put("shiftclose", shiftclose);
        values.put("dateshift", dateshift);

        sqLiteDatabase.insert("closeshift", null, values);

        Log.d("close shift", String.valueOf(values));

    }

    public void insertsecondpurchase(Integer id,String purchase_centerid,String purchase_rmcuid,
                                          String purchase_centernames,String purchase_rmcunames,int listsize ){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();

            values.put("id", id);
            values.put("purchase_centerid", purchase_centerid);
            values.put("purchase_rmcuid", purchase_rmcuid);
            values.put("purchase_centernames", purchase_centernames);
            values.put("purchase_rmcunames", purchase_rmcunames);
            values.put("listsize", listsize);
            sqLiteDatabase.insert("secondpurchase", null, values);
    }

    public void insertsave(Integer id,String prodcutname,String productno,String productunit,String quantity){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("productname", prodcutname);
        values.put("productno", productno);
        values.put("productunit", productunit);
        values.put("quantity", quantity);

        sqLiteDatabase.insert("savepurchase", null, values);
        Log.d("savepurchase", String.valueOf(values));

    }


    public void insertsales_transaction(Integer id,String prodcutname,String productno,String rate,String quantity){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("productname", prodcutname);
        values.put("productno", productno);
        values.put("rate", rate);
        values.put("quantity", quantity);

        sqLiteDatabase.insert("savesalestrans", null, values);
    }


    public void insertData(String id, String username, String centerid, String centername) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("username", username);
        values.put("centerid", centerid);
        values.put("centername", centername);

        sqLiteDatabase.insert("centeruser", null, values);
    }

    public void insertDataFarmer(String id, String farmername, String farmer_number,String image,
                                 String app_image,String address,String mobilenumber,String cattlevalue,String dscno,String centernumber,
                                 String rmcunumber,String bankname,String branch,String ifscvalue,String accountnumber,String state,
                                 String pincode, String country,String aadharcardnumber,String rfid) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("farmername", farmername);
        values.put("farmer_number", farmer_number);
        values.put("image", image);
        values.put("app_image", app_image);
        values.put("address", address);
        values.put("mobilenumber", mobilenumber);
        values.put("cattlevalue", cattlevalue);
        values.put("dscno", dscno);
        values.put("centernumber", centernumber);
        values.put("rmcunumber", rmcunumber);
        values.put("bankname", bankname);
        values.put("branch", branch);
        values.put("ifscvalue", ifscvalue);
        values.put("accountnumber", accountnumber);
        values.put("state", state);
        values.put("pincode", pincode);
        values.put("country", country);
        values.put("aadharcardnumber", aadharcardnumber);
        values.put("rfid", rfid);

        sqLiteDatabase.insert("farmermaster", null, values);
    }



    public void insertunitmaster(String id, String unitname, String unitname1, String scalefactor, String factorunit, String createdunit) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("unitname", unitname);
        values.put("unitname1", unitname1);
        values.put("scalefactor", scalefactor);
        values.put("factorunit", factorunit);
        values.put("createdunit", createdunit);

        sqLiteDatabase.insert("unitmaster", null, values);
    }

    public void insertRMCUmaster(String id, String societyName, String societyName1, String societyAddress,
                                 String pinCode, String unionName, String mobileNumber, String bankDetails,
                                 String status, String createdAt, String updatedAt, String centername,
                                 String society_number) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("societyName", societyName);
        values.put("societyName1", societyName1);
        values.put("societyAddress", societyAddress);
        values.put("pinCode", pinCode);
        values.put("unionName", unionName);
        values.put("mobileNumber", mobileNumber);
        values.put("bankDetails", bankDetails);
        values.put("status", status);
        values.put("createdAt", createdAt);
        values.put("updatedAt", updatedAt);
        values.put("centername", centername);
        values.put("society_number", society_number);
        Log.d("aaaa",society_number);

        sqLiteDatabase.insert("rmcumaster", null, values);
    }


    public void insertCenterMaster(String id, String center_name, String center_name1, String center_operator,
                                   String center_operator1, String center_pin, String bank_details, String rate_chart,
                                   String status, String created_at, String updated_at, String user_id,String bank_address,
                                   String branch_name,String ifsc_code,String account_number,String center_number,String image_url) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("center_name", center_name);
        values.put("center_name1", center_name1);
        values.put("center_operator", center_operator);
        values.put("center_operator1", center_operator1);
        values.put("center_pin", center_pin);
        values.put("bank_details", bank_details);
        values.put("rate_chart", rate_chart);
        values.put("status", status);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("user_id", user_id);
        values.put("bank_address", bank_address);
        values.put("branch_name", branch_name);
        values.put("ifsc_code", ifsc_code);
        values.put("account_number", account_number);
        values.put("center_number", center_number);
        values.put("image_url", image_url);

        sqLiteDatabase.insert("centermaster", null, values);
    }


    public void insertListRatechart(String id_R, String chartName, String shift, String ratechart, String cattletype,
                                    String charttype, String range, String fixedrate, String ratedate,
                                    String status,String created_at,String updated_at,String fatrange_from,String fatrange_to,
                                    String fatsnfrange_from,String fatsnfrange_to,String effective_date,
                                    String expiration_date,String fat_value,String snf_value) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id_R", id_R);
        values.put("chartName", chartName);
        values.put("shift", shift);
        values.put("ratechart", ratechart);
        values.put("cattletype", cattletype);
        values.put("charttype", charttype);
        values.put("range", range);
        values.put("fixedrate", fixedrate);
        values.put("ratedate", ratedate);
        values.put("status", status);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("fatrange_from", fatrange_from);
        values.put("fatrange_to", fatrange_to);
        values.put("fatsnfrange_from", fatsnfrange_from);
        values.put("fatsnfrange_to", fatsnfrange_to);
        values.put("effective_date", effective_date);
        values.put("expiration_date", expiration_date);
        values.put("fat_value", fat_value);
        values.put("snf_value", snf_value);

        sqLiteDatabase.insert("listRatechart", null, values);

    }

    public void insertdeductionmaster(String id, String deductionName, String deductionName1, String deductionType,
                                      String deductionCycle, String activationPeriod, String deductionMethod,
                                      String deductionActive, String createdAt, String updatedAt) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("deductionName", deductionName);
        values.put("deductionName1", deductionName1);
        values.put("deductionType", deductionType);
        values.put("deductionCycle", deductionCycle);
        values.put("activationPeriod", activationPeriod);
        values.put("deductionMethod", deductionMethod);
        values.put("deductionActive", deductionActive);
        values.put("createdAt", createdAt);
        values.put("updatedAt", updatedAt);

        sqLiteDatabase.insert("deductionmaster", null, values);

    }


    public void insertTransactionSetting(String rmcuname, String centername, String scale, String analyzer,
                                         String cowmorning, String buffelomorning, String cowevening,
                                         String buffeloevening, String printingvalue, String status,
                                         String settingvalue, String headermessage,String footermessage,String snfformula,
                                          String littervalue,String idss) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("rmcuname", rmcuname);
        values.put("centername", centername);
        values.put("scale", scale);
        values.put("analyzer", analyzer);
        values.put("cowmorning", cowmorning);
        values.put("buffelomorning", buffelomorning);
        values.put("cowevening", cowevening);
        values.put("buffeloevening", buffeloevening);
        values.put("printingvalue", printingvalue);
        values.put("status", status);
        values.put("settingvalue", settingvalue);
        values.put("headermessage", headermessage);
        values.put("footermessage", footermessage);
        values.put("snfformula", snfformula);
        values.put("littervalue", littervalue);
        values.put("idss", idss);

        sqLiteDatabase.insert("listTransactionSetting", null, values);


    }

    public void insertRatechartMatrix(String id, String ratechart_id, String fat_range, String snf_lacto_range,
                                         String rate_value, String status, String created_at,
                                         String updated_at,String fatnewrange) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("ratechart_id", ratechart_id);
        values.put("fat_range", fat_range);
        values.put("snf_lacto_range", snf_lacto_range);
        values.put("rate_value", rate_value);
        values.put("status", status);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("fatnewrange", fatnewrange);


        sqLiteDatabase.insert("ratechartmatrix", null, values);

    }



    public void insertProductMaster(String id, String product_name, String product_name1, String record_list,
                                      String manufacture_details, String unit_scale, String subsidy_percentage,
                                      String created_at,String updated_at,String status) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("product_name", product_name);
        values.put("product_name1", product_name1);
        values.put("record_list", record_list);
        values.put("manufacture_details", manufacture_details);
        values.put("unit_scale", unit_scale);
        values.put("subsidy_percentage", subsidy_percentage);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("status", status);


        sqLiteDatabase.insert("productmaster", null, values);

    }



    public void insertFarmerRegiData(Integer id,String firstname, String middlename, String lastname, String firstReligionName, String middleReligionName,
                                     String lastReligionName, String address, String contactNo, String adhar_no, String city, String state, String pincode, String bank_name, String branch_name,
                                     String account_no, String ifsc_code, String noofcow, String noofbuffelo, String noofcattle,byte[] datas,String disable)
    {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("firstname", firstname);
        values.put("middlename", middlename);
        values.put("lastname", lastname);
        values.put("firstReligionName", firstReligionName);
        values.put("middleReligionName", middleReligionName);
        values.put("lastReligionName", lastReligionName);

        values.put("address", address);
        values.put("contactNo", contactNo);
        values.put("adhar_no", adhar_no);

        values.put("city", city);
        values.put("state", state);
        values.put("pincode", pincode);

        values.put("bank_name", bank_name);
        values.put("branch_name", branch_name);
        values.put("account_no", account_no);
        values.put("ifsc_code", ifsc_code);
        values.put("noofcow", noofcow);
        values.put("noofbuffelo", noofbuffelo);
        values.put("noofcattle", noofcattle);
        values.put("datas",datas);
        values.put("disable",disable);


       sqLiteDatabase.insert("farmerregi", null, values);
    }


    public void insertDeductionRegi(Integer id,String nameofdeductions ,String typeofdeductions, String deductioncycle ,String deductionmethod,
                                    String amounts,String billingcycle,String remark,String farmerNumber,String status) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("nameofdeductions", nameofdeductions);
        values.put("typeofdeduction", typeofdeductions);
        values.put("deductioncycle", deductioncycle);
        values.put("deductionmethod", deductionmethod);
        values.put("amount", amounts);
        values.put("billingcycle", billingcycle);
        values.put("remark", remark);
        values.put("farmerNumber", farmerNumber);
        values.put("status", status);

        sqLiteDatabase.insert("deductionregi", null, values);
        Log.d("status", String.valueOf(values));
    }


    public void AddCollectionPageData(Integer id, String userCattle, String dates ,String shift,
                                String farmer_no,String farmer_name,String weightss,String fatsss,String lactoo,
                                String snffss,String protiens,String waterss,String ratesss,String amountss,
                                      String localstring,String reversedate,String date_shift,String cattlebothstring) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("userCattle", userCattle);
        values.put("dates", dates);
        values.put("shift", shift);
        values.put("farmer_no", farmer_no);
        values.put("farmer_name", farmer_name);
        values.put("weightss", weightss);
        values.put("fatsss", fatsss);
        values.put("lactoo", lactoo);
        values.put("snffss", snffss);
        values.put("protiens", protiens);
        values.put("waterss", waterss);
        values.put("ratesss", ratesss);
        values.put("amountss", amountss);
        values.put("localstring", localstring);
        values.put("reversedate", reversedate);
        values.put("date_shift", date_shift);
        values.put("cattlebothstring", cattlebothstring);

        sqLiteDatabase.insert("addcollectionpagedata", null, values);
    }


    public List<String> getAllTypeDeduction() {
        List<String> labels = new ArrayList<String>();
        // Select All Query
        String selectQuery = "select * from deductionmaster";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
//                   labels.add(new String(cursor.getString(cursor.getColumnIndex("fullname1"))));
                labels.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return labels;
    }

    public List<String> getAllNameDeduction() {
        List<String> labels = new ArrayList<String>();
        // Select All Query
        String selectQuery = "select * from deductionmaster";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return labels;
    }

    public List<String> getAllProductName() {
        List<String> labels = new ArrayList<String>();
        String selectQuery = "select * from productmaster";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return labels;
    }

    public List<String> getAllProductName2() {
        List<String> labels = new ArrayList<String>();
        String selectQuery = "select * from purchaselist";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(cursor.getColumnIndex("productname")));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return labels;
    }


    public List<String> getAllbanklist() {
        List<String> labels = new ArrayList<String>();
        String selectQuery = "select * from banklist";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return labels;
    }

    public void deleteFarmerRegRecord() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("delete from " + centeruser);
//            db.execSQL("delete from " + TABLE_NAME);
//            db.execSQL("delete from " + unitmaster);
//            db.execSQL("delete from " + rmcumaster);
//            db.execSQL("delete from " + deductionmaster);
//            db.execSQL("delete from " + listRatechart);
//            db.execSQL("delete from " + listTransactionSetting);
//            db.execSQL("delete from " + ratechartmatrix);
//            db.execSQL("delete from " + PRODUCT_MASTER);
//            db.execSQL("delete from " + CENTER_MASTER);
//            db.execSQL("delete from " + PURCHASELIST);
//            db.execSQL("delete from " + BANKLIST);
//            db.execSQL("delete from " + downloadsettingdata);
//            db.execSQL("delete from " + shifttime);
//            db.execSQL("delete from " +  updateddate);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteSynchronizedata() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("delete from " + TABLE_NAME);
            db.execSQL("delete from " + unitmaster);
            db.execSQL("delete from " + rmcumaster);
            db.execSQL("delete from " + deductionmaster);
            db.execSQL("delete from " + listRatechart);
            db.execSQL("delete from " + listTransactionSetting);
            db.execSQL("delete from " + ratechartmatrix);
            db.execSQL("delete from " + PRODUCT_MASTER);
            db.execSQL("delete from " + CENTER_MASTER);
            db.execSQL("delete from " + PURCHASELIST);
            db.execSQL("delete from " + BANKLIST);
            db.execSQL("delete from " + downloadsettingdata);
            db.execSQL("delete from " + shifttime);
            db.execSQL("delete from " +  updateddate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void deleteFarmerRegRecord2() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("delete from " + Bluetooth);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deletepurchasedata() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("delete from " + ADDPURCHASEDATA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteFarmerRegRecord3() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            db.execSQL("delete from " + COLLECTIONMODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void delete(int count){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete("Salesavailablequantity", "id=?",new String[]{String.valueOf(count)});
    }

    public void deletefarmerid(String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete("farmerregisynch", "id=?",new String[]{String.valueOf(id)});
    }

    public void deletefarmerid1(String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete("collectionsynch", "id=?",new String[]{String.valueOf(id)});
    }

    public void deletefarmerid2(String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete("closeshiftsynch", "id=?",new String[]{String.valueOf(id)});
    }


    public void updatedata(String updateid,String Renameofdeductions,String Retypeofdeductions,String Redeductioncycle,String Redeductionmethod,String amounts,
                           String Rebillingcycle,String Reremark,String farmerNumber,String statusdeactivate){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", updateid);
        values.put("nameofdeductions", Renameofdeductions);
        values.put("typeofdeduction", Retypeofdeductions);
        values.put("deductioncycle", Redeductioncycle);
        values.put("deductionmethod", Redeductionmethod);
        values.put("amount", amounts);
        values.put("billingcycle", Rebillingcycle);
        values.put("remark", Reremark);
        values.put("farmerNumber", farmerNumber);
        values.put("status", statusdeactivate);


        sqLiteDatabase.update("deductionregi", values,"id=?",new String[]{updateid});

        Log.d("values", String.valueOf(values));
    }

    public void updatedatasales(String available_quan,Integer count,String status){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("avilable_quantity", available_quan);
        values.put("status", status);

//        Log.d("countdatabase", String.valueOf(count));


        sqLiteDatabase.update("Salesavailablequantity", values,"id=?",new String[]{String.valueOf(count)});
    }

    public void updatedatabank(String bank_name,String id){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("bank_name", bank_name);
        values.put("id", id);

        sqLiteDatabase.update("banklist", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateRMCUmaster(String id,String societyName,String societyName1,String societyAddress,String pinCode,
                                 String unionName,String mobileNumber,String bankDetails,String status,String createdAt,String updatedAt,
                                 String centername,String society_number){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("societyName", societyName);
        values.put("societyName1", societyName1);
        values.put("societyAddress", societyAddress);
        values.put("pinCode", pinCode);
        values.put("unionName", unionName);
        values.put("mobileNumber", mobileNumber);
        values.put("bankDetails", bankDetails);
        values.put("status", status);
        values.put("createdAt", createdAt);
        values.put("updatedAt", updatedAt);
        values.put("centername", centername);
        values.put("society_number", society_number);

        sqLiteDatabase.update("rmcumaster", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateCenterMaster(String id,String center_name,String center_name1,String center_operator,String center_operator1,
                                   String center_pin,String bank_details,String rate_chart,String status,String created_at,
                                   String updated_at,String user_id,String bank_address,String branch_name,String ifsc_code,String account_number,
                                   String center_number,String image_url){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("center_name", center_name);
        values.put("center_name1", center_name1);
        values.put("center_operator", center_operator);
        values.put("center_operator1", center_operator1);
        values.put("center_pin", center_pin);
        values.put("bank_details", bank_details);
        values.put("rate_chart", rate_chart);
        values.put("status", status);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("user_id", user_id);
        values.put("bank_address", bank_address);
        values.put("branch_name", branch_name);
        values.put("ifsc_code", ifsc_code);
        values.put("account_number", account_number);
        values.put("center_number", center_number);
        values.put("image_url", image_url);

        sqLiteDatabase.update("centermaster", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateDataFarmer(String id,String farmername,String farmer_number,String image,String app_image,String address,
                                 String mobilenumber,String cattlevalue,String dscno,String centernumber,String rmcunumber,String bankname,
                                 String branch,String ifscvalue,String accountnumber,String state,String pincode,
                                 String country,String aadharcardnumber,String rfid){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("farmername", farmername);
        values.put("farmer_number", farmer_number);
        values.put("image", image);
        values.put("app_image", app_image);
        values.put("address", address);
        values.put("mobilenumber", mobilenumber);
        values.put("cattlevalue", cattlevalue);
        values.put("dscno", dscno);
        values.put("centernumber", centernumber);
        values.put("rmcunumber", rmcunumber);
        values.put("bankname", bankname);
        values.put("branch", branch);
        values.put("ifscvalue", ifscvalue);
        values.put("accountnumber", accountnumber);
        values.put("state", state);
        values.put("pincode", pincode);
        values.put("country", country);
        values.put("aadharcardnumber", aadharcardnumber);
        values.put("rfid", rfid);

        sqLiteDatabase.update("farmermaster", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateListRatechart(String id_R,String chartName,String shift,String ratechart,String cattletype,String charttype,
                                    String range,String fixedrate,String ratedate,String status,String created_at,String updated_at,
                                    String fatrange_from,String fatrange_to,String fatsnfrange_from,String fatsnfrange_to,
                                    String effective_date,String expiration_date,String fat_value,String snf_value){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id_R", id_R);
        values.put("chartName", chartName);
        values.put("shift", shift);
        values.put("ratechart", ratechart);
        values.put("cattletype", cattletype);
        values.put("charttype", charttype);
        values.put("range", range);
        values.put("fixedrate", fixedrate);
        values.put("ratedate", ratedate);
        values.put("status", status);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("fatrange_from", fatrange_from);
        values.put("fatrange_to", fatrange_to);
        values.put("fatsnfrange_from", fatsnfrange_from);
        values.put("fatsnfrange_to", fatsnfrange_to);
        values.put("effective_date", effective_date);
        values.put("expiration_date", expiration_date);
        values.put("fat_value", fat_value);
        values.put("snf_value", snf_value);

        sqLiteDatabase.update("listRatechart", values,"id_R=?",new String[]{String.valueOf(id_R)});
    }

    public void updateProductMaster(String id,String product_name,String product_name1,String record_list,String manufacture_details,
                                    String unit_scale,String subsidy_percentage,String created_at,String updated_at,String status){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("product_name", product_name);
        values.put("product_name1", product_name1);
        values.put("record_list", record_list);
        values.put("manufacture_details", manufacture_details);
        values.put("unit_scale", unit_scale);
        values.put("subsidy_percentage", subsidy_percentage);
        values.put("created_at", created_at);
        values.put("updated_at", updated_at);
        values.put("status", status);

        sqLiteDatabase.update("productmaster", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updatePurchaseList(String id,String purchaseid,String productname,String productunit,String productquantity,
                                  String productnumber,String flag,String allocatedstock,String sale_rate,String created_at){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("purchaseid", purchaseid);
        values.put("productname", productname);
        values.put("productunit", productunit);
        values.put("productquantity", productquantity);
        values.put("productnumber", productnumber);
        values.put("flag", flag);
        values.put("allocatedstock", allocatedstock);
        values.put("sale_rate", sale_rate);
        values.put("created_at", created_at);

        sqLiteDatabase.update("purchaselist", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateshifttime(String id,String morningstarttime,String morningendtime,String eveningstarttime,String eveningendtime){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("morningstarttime", morningstarttime);
        values.put("morningendtime", morningendtime);
        values.put("eveningstarttime", eveningstarttime);
        values.put("eveningendtime", eveningendtime);

        sqLiteDatabase.update("shifttime", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updatedeductionmaster(String id,String deductionName,String deductionName1,String deductionType,String deductionCycle,
                                      String activationPeriod,String deductionMethod,String deductionActive,String createdAt,String updatedAt){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("id", id);
        values.put("deductionName", deductionName);
        values.put("deductionName1", deductionName1);
        values.put("deductionType", deductionType);
        values.put("deductionCycle", deductionCycle);
        values.put("activationPeriod", activationPeriod);
        values.put("deductionMethod", deductionMethod);
        values.put("deductionActive", deductionActive);
        values.put("createdAt", createdAt);
        values.put("updatedAt", updatedAt);

        sqLiteDatabase.update("deductionmaster", values,"id=?",new String[]{String.valueOf(id)});
    }

    public void updateTransactionSetting(String rmcuname,String centername,String scale,String analyzer,String cowmorning,String buffelomorning,
                                         String cowevening,String buffeloevening,String printingvalue,String status,String settingvalue,String headermessage,
                                         String footermessage,String snfformula,String littervalue,String idss){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("rmcuname", rmcuname);
        values.put("centername", centername);
        values.put("scale", scale);
        values.put("analyzer", analyzer);
        values.put("cowmorning", cowmorning);
        values.put("buffelomorning", buffelomorning);
        values.put("cowevening", cowevening);
        values.put("buffeloevening", buffeloevening);
        values.put("printingvalue", printingvalue);
        values.put("status", status);
        values.put("settingvalue", settingvalue);
        values.put("headermessage", headermessage);
        values.put("footermessage", footermessage);
        values.put("snfformula", snfformula);
        values.put("littervalue", littervalue);
        values.put("idss", idss);

        sqLiteDatabase.update("listTransactionSetting", values,"centername=?",new String[]{String.valueOf(centername)});


    }









}




