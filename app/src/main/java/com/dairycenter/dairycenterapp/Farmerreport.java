package com.dairycenter.dairycenterapp;

public class Farmerreport {

    String farmer_no,farmer_name,address,mobno,cattlevalue,centernumber,rmcunumber,bankname,branch,ifscvalue,accountnumber,
            state,pincode,country,aadharcardnumber;


    public String getFarmer_no() {
        return farmer_no;
    }

    public void setFarmer_no(String farmer_no) {
        this.farmer_no = farmer_no;
    }

    public String getFarmer_name() {
        return farmer_name;
    }

    public void setFarmer_name(String farmer_name) {
        this.farmer_name = farmer_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMobno() {
        return mobno;
    }

    public void setMobno(String mobno) {
        this.mobno = mobno;
    }

    public String getCattlevalue() {
        return cattlevalue;
    }

    public void setCattlevalue(String cattlevalue) {
        this.cattlevalue = cattlevalue;
    }

    public String getCenternumber() {
        return centernumber;
    }

    public void setCenternumber(String centernumber) {
        this.centernumber = centernumber;
    }

    public String getRmcunumber() {
        return rmcunumber;
    }

    public void setRmcunumber(String rmcunumber) {
        this.rmcunumber = rmcunumber;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getIfscvalue() {
        return ifscvalue;
    }

    public void setIfscvalue(String ifscvalue) {
        this.ifscvalue = ifscvalue;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAadharcardnumber() {
        return aadharcardnumber;
    }

    public void setAadharcardnumber(String aadharcardnumber) {
        this.aadharcardnumber = aadharcardnumber;
    }
}
