package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.FarmerListAdapter;
import com.dairycenter.dairycenterapp.Adapter.FarmerReportListAdapter;

import java.util.ArrayList;
import java.util.List;

public class FarmerListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    FarmerListAdapter adapter;
    DatabaseHelper db;
    List<Farmerreport> list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_list);

        getSupportActionBar().setTitle("Farmer Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView           = (RecyclerView)findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        db = new DatabaseHelper(FarmerListActivity.this);
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);

        if (mCursor.moveToFirst()) {
            do {

                String farmer_no          = mCursor.getString(mCursor.getColumnIndex("farmer_number"));
                String farmername         =(mCursor.getString(mCursor.getColumnIndex("farmername")));
                String address            =(mCursor.getString(mCursor.getColumnIndex("address")));
                String mobileno           =(mCursor.getString(mCursor.getColumnIndex("mobilenumber")));
                String cattlevalue        =(mCursor.getString(mCursor.getColumnIndex("cattlevalue")));
                String centernumbers       =(mCursor.getString(mCursor.getColumnIndex("centernumber")));
                String rmcunumber         =(mCursor.getString(mCursor.getColumnIndex("rmcunumber")));
                String bankname           =(mCursor.getString(mCursor.getColumnIndex("bankname")));
                String branch             =(mCursor.getString(mCursor.getColumnIndex("branch")));
                String ifscvalue          =(mCursor.getString(mCursor.getColumnIndex("ifscvalue")));
                String accountnumber      =(mCursor.getString(mCursor.getColumnIndex("accountnumber")));
                String state              =(mCursor.getString(mCursor.getColumnIndex("state")));
                String pincode            =(mCursor.getString(mCursor.getColumnIndex("pincode")));
                String country            =(mCursor.getString(mCursor.getColumnIndex("country")));
                String aadharcardnumber   =(mCursor.getString(mCursor.getColumnIndex("aadharcardnumber")));

                if(cattlevalue.equals("buffelo")){
                    cattlevalue = "Buffalo";
                }else if(cattlevalue.equals("cow")){
                    cattlevalue = "Cow";
                }



                Farmerreport farmerreport = new Farmerreport();

                farmerreport.setFarmer_no(farmer_no);
                farmerreport.setFarmer_name(farmername);
                farmerreport.setAddress(address);
                farmerreport.setMobno(mobileno);
                farmerreport.setCattlevalue(cattlevalue);
                farmerreport.setCenternumber(centernumbers);
                farmerreport.setRmcunumber(rmcunumber);
                farmerreport.setBankname(bankname);
                farmerreport.setBranch(branch);
                farmerreport.setIfscvalue(ifscvalue);
                farmerreport.setAccountnumber(accountnumber);
                farmerreport.setState(state);
                farmerreport.setPincode(pincode);
                farmerreport.setCountry(country);
                farmerreport.setAadharcardnumber(aadharcardnumber);


                list.add(farmerreport);
                adapter = new FarmerListAdapter(FarmerListActivity.this, list);
                recyclerView.setAdapter(adapter);

            } while (mCursor.moveToNext());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}

