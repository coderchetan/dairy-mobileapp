package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import me.aflak.bluetooth.Bluetooth;

public class OpenBluetoothActivity extends AppCompatActivity implements Bluetooth.DiscoveryCallback, AdapterView.OnItemClickListener {

    Switch switchButton;
        BluetoothAdapter bluetoothAdapter;
        int i = 1;
        Intent bluetoothIntent;
        TextView textView;
        ListView listView;
        Set<BluetoothDevice> paireddevice;

        Bluetooth bluetooth;
        ListView listView2;
        ArrayAdapter<String> adapter;
        TextView state;
        ProgressBar progress;
        Button scan;
        List<BluetoothDevice> devices;
        public static int posi;
        public static String userstring;


        @Override
        protected void onCreate (Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_bluetooth);
        getSupportActionBar().setTitle("Bluetooth App");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        switchButton = (Switch) findViewById(R.id.switch1);
        textView = (TextView) findViewById(R.id.tv3);
        listView = (ListView) findViewById(R.id.listviewPaired);
        listView2 = (ListView) findViewById(R.id.listviewAvailable);

        state = (TextView) findViewById(R.id.scan_state);
        progress = (ProgressBar) findViewById(R.id.scan_progress);
        scan = (Button) findViewById(R.id.scan);


        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        listView2.setAdapter(adapter);
        listView2.setOnItemClickListener(this);

        bluetooth = new Bluetooth(this);
        bluetooth.setDiscoveryCallback(this);

        bluetooth.scanDevices();
        progress.setVisibility(View.VISIBLE);
        state.setText("Scanning...");
        listView2.setEnabled(false);

        scan.setEnabled(false);
        devices = new ArrayList<>();

        userstring = "Demo";

        scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.clear();
                        scan.setEnabled(false);
                    }
                });

                Bt bt = new Bt();
                bt.out("~BUTN");


                devices = new ArrayList<>();
                progress.setVisibility(View.VISIBLE);
                state.setText("Scanning...");
                bluetooth.scanDevices();
            }
        });


        SetBluettothDeviceName();
        getPairedDevices();
//        bluettothscanning();
        bluetoothAdapter.disable();
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(bluetoothIntent, i);
                } else {
                    bluetoothAdapter.disable();
                }
            }
        });


    }

        public String SetBluettothDeviceName () {
        if (bluetoothAdapter == null) {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }
        String name = bluetoothAdapter.getName();
        textView.setText(name);
        return name;
    }

        public void getPairedDevices () {
        paireddevice = bluetoothAdapter.getBondedDevices();
        ArrayList list = new ArrayList();
        for (BluetoothDevice bluetoothDevice : paireddevice)
            list.add(bluetoothDevice.getName());
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //                Intent i = new Intent(MainActivity.this, DataCommActivity.class);
//                i.putExtra("pos", position);
                posi = position;
                Log.d("posi", String.valueOf(posi));
                Bt bt = new Bt();
                bt.obj(OpenBluetoothActivity.this);
                bt.enable(OpenBluetoothActivity.this);
                bt.comm(OpenBluetoothActivity.this);


//                startActivity(i);
//                finish();
            }
        });
    }

        public static int getpos () {
        return posi;
    }

        private void setText ( final String txt){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                state.setText(txt);
            }
        });
    }

        private void setProgressVisibility ( final int id){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progress.setVisibility(id);
            }
        });
    }

        @Override
        public void onFinish () {
        setProgressVisibility(View.INVISIBLE);
        setText("Scan finished!");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                scan.setEnabled(true);
                listView2.setEnabled(true);
            }
        });
    }

        @Override
        public void onDevice ( final BluetoothDevice device){
        final BluetoothDevice tmp = device;
        devices.add(device);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.add(tmp.getName());
            }
        });
    }

        @Override
        public void onPair (BluetoothDevice device){
        setProgressVisibility(View.INVISIBLE);
        setText("Paired!");
        finish();
    }

        @Override
        public void onUnpair (BluetoothDevice device){
        setProgressVisibility(View.INVISIBLE);
        setText("Paired!");
    }

        @Override
        public void onError (String message){
        setProgressVisibility(View.INVISIBLE);
        setText("Error: " + message);
    }


        @Override
        public void onItemClick (AdapterView < ? > parent, View view,int position, long id){
        setProgressVisibility(View.VISIBLE);
        setText("Pairing...");
        bluetooth.pair(devices.get(position));
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id=item.getItemId();
//        switch (item.getItemId()) {
//            case android.R.id.home:
//
//                userstring = "Demo";
//
//
//
////                Intent intent = new Intent(OpenBluetoothActivity.this,HomeActivity.class);
////                intent.putExtra("userstring","Demo");
////                startActivity(intent);
//                return true;
//        }
//        return true;
////    }

        public static String getdemo () {
        return userstring;

    }
    }