package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.UserData;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyHolder> {

    List<UserData> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;
    public static String edit,produnit,prodname,prodno;
    public static int pos;



    public MyAdapter( Context mcts, List<UserData> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyHolder holder, final int position) {

        final UserData userData = list.get(position);

        holder.tv_name.setText(userData.getName());
        holder.tv_email.setText(userData.getEmail());
        holder.tv_unit.setText(userData.getUnit());
        holder.tv_quanti.setText(userData.getEtc());

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                itemClickListener.OnItemClick(position,userData,produnit,prodname,prodno,edit);
//            }
//        });

        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                Toast.makeText(mcts, "Deleted", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();
            }
        });

        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(mcts);
                View subView = inflater.inflate(R.layout.dialog_update, null);
                final TextView update = (TextView)subView.findViewById(R.id.Quantity_ed);

                AlertDialog.Builder builder = new AlertDialog.Builder(mcts);
                builder.setTitle("Edit product");
                builder.setView(subView);
                builder.create();
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Log.d( "quantity",holder.tv_quanti.getText().toString());
                        edit = update.getText().toString();
                        if(update.getText().toString().isEmpty()){

                        }else{
                            pos = position;

                            produnit = holder.tv_unit.getText().toString();
                            prodname = holder.tv_email.getText().toString();
                            prodno  = holder.tv_name.getText().toString();
                            holder.tv_quanti.setText(edit);

                            itemClickListener.OnItemClick(pos,userData,produnit,prodname,prodno,edit);

                        }
                    }
                });

                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{
        TextView tv_name,tv_email,tv_delete,tv_edit,tv_quanti,tv_unit;

        public MyHolder(View itemView) {
            super(itemView);

            tv_name = itemView.findViewById(R.id.tv_name_item);
            tv_email = itemView.findViewById(R.id.tv_email_item);
            tv_unit = itemView.findViewById(R.id.tv_unit_item);
            tv_quanti = itemView.findViewById(R.id.Quantitys);
            tv_edit = itemView.findViewById(R.id.edit);
            tv_delete = itemView.findViewById(R.id.tv_delete_item);
        }
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener){
        this.itemClickListener = itemClickListener;
    }


    public void UpdateData(int position,UserData userData1){
        list.remove(position);
        list.add(userData1);
        notifyItemChanged(position);
        notifyDataSetChanged();
    }

}
