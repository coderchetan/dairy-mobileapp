package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;

public class DeductionAdapter extends BaseAdapter {

    private Context mContext;

    private ArrayList<String> ids;
    private ArrayList<String> nameofdeductionss;
    private ArrayList<String> typeofdeductionss;
    private ArrayList<String> deductioncycless;
    private ArrayList<String> deductionmethodess;
    private ArrayList<String> deductionamounts;
    private ArrayList<String> deductionbillingcount;
    private ArrayList<String> deductionremark;



    public DeductionAdapter(Context c, ArrayList<String> ids,ArrayList<String> nameofdeductionss, ArrayList<String> typeofdeductionss,
                            ArrayList<String> deductioncycless, ArrayList<String> deductionmethodess, ArrayList<String> deductionamounts,
                            ArrayList<String> deductionbillingcount, ArrayList<String> deductionremark) {

        this.mContext = c;
        //transfer content from database to temporary memory
        this.ids = ids;
        this.nameofdeductionss = nameofdeductionss;
        this.typeofdeductionss = typeofdeductionss;
        this.deductioncycless = deductioncycless;
        this.deductionmethodess = deductionmethodess;
        this.deductionamounts = deductionamounts;
        this.deductionbillingcount = deductionbillingcount;
        this.deductionremark = deductionremark;

    }


    @Override
    public int getCount() {
        return ids.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int pos, View child, ViewGroup parent) {
        DeductionAdapter.Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.activity_deduction_adapter, null);
            mHolder = new DeductionAdapter.Holder();

            //link to TextView
            mHolder.txtid = (TextView) child.findViewById(R.id.dedu_id);
            mHolder.txtdeduname1 = (TextView) child.findViewById(R.id.dedu_name_1);
            mHolder.txtdeduname = (TextView) child.findViewById(R.id.dedu_name);
            mHolder.txtdeducycle = (TextView) child.findViewById(R.id.dedu_cycle);
            mHolder.txtdedumethod = (TextView) child.findViewById(R.id.dedu_method);
            mHolder.txtdeduamount = (TextView) child.findViewById(R.id.dedu_amount);
            mHolder.txtdedubillingcount = (TextView) child.findViewById(R.id.dedu_billing);
            mHolder.txtdeduremark = (TextView) child.findViewById(R.id.dedu_remark);

            child.setTag(mHolder);
        } else {
            mHolder = (DeductionAdapter.Holder) child.getTag();
        }
        //transfer to TextView in screen
        mHolder.txtid.setText(ids.get(pos));
        mHolder.txtdeduname1.setText(nameofdeductionss.get(pos));
        mHolder.txtdeduname.setText(typeofdeductionss.get(pos));
        mHolder.txtdeducycle.setText(deductioncycless.get(pos));
        mHolder.txtdedumethod.setText(deductionmethodess.get(pos));
        mHolder.txtdeduamount.setText(deductionamounts.get(pos));
        mHolder.txtdedubillingcount.setText(deductionbillingcount.get(pos));
        mHolder.txtdeduremark.setText(deductionremark.get(pos));

        return child;
    }

    public static class Holder {
        TextView txtid;
        TextView txtdeduname1;
        TextView txtdeduname;
        TextView txtdeducycle;
        TextView txtdedumethod;
        TextView txtdeduamount;
        TextView txtdedubillingcount;
        TextView txtdeduremark;
    }
}
