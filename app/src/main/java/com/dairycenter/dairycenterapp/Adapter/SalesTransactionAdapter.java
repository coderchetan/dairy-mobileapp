package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.ItemClickListener1;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.SalesTransaction;

import java.util.ArrayList;
import java.util.List;

public class SalesTransactionAdapter extends RecyclerView.Adapter<SalesTransactionAdapter.MyHolder>  {

    List<SalesTransaction> list = new ArrayList<>();
    public Context mcts;
    ItemClickListener1 itemClickListener1;
    String prodnumber,prodname,prodrate,edit,amount;
    public static int pos;

    public SalesTransactionAdapter( Context mcts, List<SalesTransaction> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public SalesTransactionAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_sales_transaction_adapter,parent,false);
        return new SalesTransactionAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final SalesTransactionAdapter.MyHolder holder, final int position) {

        final SalesTransaction salesTransaction = list.get(position);

        holder.tv_proname.setText(salesTransaction.getProductname());
        holder.tv_pronum.setText(salesTransaction.getProductnumber());
        holder.tv_rate.setText(salesTransaction.getRate());
        holder.tv_req_quanti.setText(salesTransaction.getAvailable_quan());
        holder.tv_amount.setText(salesTransaction.getAmount());


//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                itemClickListener1.OnItemClick(position,salesTransaction,prodnumber,prodname,prodrate,edit,amount);
//            }
//        });


        holder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                Toast.makeText(mcts, "Deleted", Toast.LENGTH_LONG).show();
                notifyDataSetChanged();

            }
        });
        holder.tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(mcts);
                View subView = inflater.inflate(R.layout.dialog_update, null);
                final TextView update = (TextView)subView.findViewById(R.id.Quantity_ed);
                AlertDialog.Builder builder = new AlertDialog.Builder(mcts);
                builder.setTitle("Edit product");
                builder.setView(subView);
                builder.create();

                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        edit = update.getText().toString();
                        if(update.getText().toString().isEmpty()){

                        }else{
                            pos = position;

                            prodnumber = holder.tv_pronum.getText().toString();
                            prodname = holder.tv_proname.getText().toString();
                            prodrate  = holder.tv_rate.getText().toString();
                            holder.tv_req_quanti.setText(edit);
                            amount =  holder.tv_amount.getText().toString();

                            itemClickListener1.OnItemClick(pos,salesTransaction,prodnumber,prodname,prodrate,edit,amount);


                        }
                    }
                });
                builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public static class MyHolder extends RecyclerView.ViewHolder{

        TextView tv_proname,tv_pronum,tv_rate,tv_req_quanti,tv_delete,tv_edit,tv_amount;
        public MyHolder(View itemView) {
            super(itemView);

            tv_proname = itemView.findViewById(R.id.tv_proname);
            tv_pronum = itemView.findViewById(R.id.tv_number);
            tv_rate = itemView.findViewById(R.id.tv_rate);
            tv_req_quanti = itemView.findViewById(R.id.Quantitys);
            tv_edit = itemView.findViewById(R.id.edit);
            tv_delete = itemView.findViewById(R.id.tv_delete_item);
            tv_amount = itemView.findViewById(R.id.amount);
        }
    }

    public void setOnItemClickListener(ItemClickListener1 itemClickListener1){
        this.itemClickListener1 = itemClickListener1;
    }

}
