package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.CollectionReport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.TruckSheetReport;

import java.util.ArrayList;
import java.util.List;

public class TruckSheetReportAdapter extends RecyclerView.Adapter<TruckSheetReportAdapter.MyHolder> {
    List<TruckSheetReport> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;

    public TruckSheetReportAdapter( Context mcts, List<TruckSheetReport> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public TruckSheetReportAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_truck_sheet_report_adapter,parent,false);
        return new TruckSheetReportAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final TruckSheetReportAdapter.MyHolder holder, final int position) {

        final TruckSheetReport truckSheetReport = list.get(position);

        holder.vehiNo.setText(truckSheetReport.getVehiNo());
        holder.Drivername.setText(truckSheetReport.getDrivername());
        holder.drivercon_no.setText(truckSheetReport.getDrivercon_no());
        holder.date.setText(truckSheetReport.getDate());
        holder.time.setText(truckSheetReport.getTime());
        holder.shift.setText(truckSheetReport.getShift());
        holder.totalwt_cow.setText(truckSheetReport.getTotalwt_cow());
        holder.totalwt_buffalo.setText(truckSheetReport.getTotalwt_buffalo());
        holder.avgfat_cow.setText(truckSheetReport.getAvgfat_cow());
        holder.avgfat_buffalo.setText(truckSheetReport.getAvgfat_buffalo());
        holder.avgsnf_cow.setText(truckSheetReport.getAvgsnf_cow());
        holder.avgsnf_buffalo.setText(truckSheetReport.getAvgsnf_buffalo());
        holder.totalwt.setText(truckSheetReport.getTotalwt());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{
        TextView vehiNo,Drivername,drivercon_no,date,time,shift,totalwt_cow,totalwt_buffalo,
                avgfat_cow,avgfat_buffalo,avgsnf_cow,avgsnf_buffalo,totalwt;

        public MyHolder(View itemView) {
            super(itemView);

            vehiNo = itemView.findViewById(R.id.vehi_no);
            Drivername = itemView.findViewById(R.id.driver_name);
            drivercon_no = itemView.findViewById(R.id.driver_contno);
            date = itemView.findViewById(R.id.date);
            time = itemView.findViewById(R.id.time);
            shift = itemView.findViewById(R.id.shift);
            totalwt_cow = itemView.findViewById(R.id.totalWt_cow);
            totalwt_buffalo = itemView.findViewById(R.id.totalWt_buffolo);
            avgfat_cow = itemView.findViewById(R.id.avgFat_Cow);
            avgfat_buffalo = itemView.findViewById(R.id.avgFat_Buffalo);
            avgsnf_cow = itemView.findViewById(R.id.AvgSnf_Cow);
            avgsnf_buffalo = itemView.findViewById(R.id.avgSnf_Buffalo);
            totalwt = itemView.findViewById(R.id.total_wt);
        }
    }
}

