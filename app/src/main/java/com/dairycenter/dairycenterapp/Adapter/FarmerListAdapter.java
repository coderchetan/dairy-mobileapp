package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.Farmerreport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;
import java.util.List;

public class FarmerListAdapter extends RecyclerView.Adapter<FarmerListAdapter.MyHolder> {

    List<Farmerreport> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;


    public FarmerListAdapter( Context mcts, List<Farmerreport> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public FarmerListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_farmer_list_adapter,parent,false);
        return new FarmerListAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final FarmerListAdapter.MyHolder holder, final int position) {

        final Farmerreport farmerreport = list.get(position);

        holder.tv_farmername.setText(farmerreport.getFarmer_name());
        holder.tv_farmerno.setText(farmerreport.getFarmer_no());
        holder.tv_address.setText(farmerreport.getAddress());
        holder.tv_mobno.setText(farmerreport.getMobno());
        holder.tv_cattlevalue.setText(farmerreport.getCattlevalue());
        holder.tv_centernumber.setText(farmerreport.getCenternumber());
        holder.tv_rmcunumber.setText(farmerreport.getRmcunumber());
        holder.tv_bankname.setText(farmerreport.getBankname());
        holder.tv_branch.setText(farmerreport.getBranch());
        holder.tv_ifscvalue.setText(farmerreport.getIfscvalue());
        holder.tv_accountnumber.setText(farmerreport.getAccountnumber());
        holder.tv_state.setText(farmerreport.getState());
        holder.tv_pincode.setText(farmerreport.getPincode());
        holder.tv_country.setText(farmerreport.getCountry());
        holder.tv_aadharcardnumber.setText(farmerreport.getAadharcardnumber());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public static class MyHolder extends RecyclerView.ViewHolder{
        TextView tv_farmerno,tv_farmername,tv_address,tv_mobno,tv_cattlevalue,tv_centernumber,tv_rmcunumber,tv_bankname,
        tv_branch,tv_ifscvalue,tv_accountnumber,tv_state,tv_pincode,tv_country,tv_aadharcardnumber;


        public MyHolder(View itemView) {
            super(itemView);

            tv_farmerno = itemView.findViewById(R.id.farmer_no);
            tv_farmername = itemView.findViewById(R.id.farmername);
            tv_address = itemView.findViewById(R.id.shift);
            tv_mobno = itemView.findViewById(R.id.weights);
            tv_cattlevalue = itemView.findViewById(R.id.cattletype);
            tv_centernumber = itemView.findViewById(R.id.centerno);
            tv_rmcunumber = itemView.findViewById(R.id.rmcuno);
            tv_bankname = itemView.findViewById(R.id.bankname);
            tv_branch = itemView.findViewById(R.id.branch);
            tv_ifscvalue = itemView.findViewById(R.id.ifsc_code);
            tv_accountnumber = itemView.findViewById(R.id.accountno);
            tv_state = itemView.findViewById(R.id.state);
            tv_pincode = itemView.findViewById(R.id.pincode);
            tv_country = itemView.findViewById(R.id.country);
            tv_aadharcardnumber = itemView.findViewById(R.id.adharcardno);

        }
    }
}





