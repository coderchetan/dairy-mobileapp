package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.Farmerreport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.ProductList;
import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;
import java.util.List;

public class ProductListAdapter extends  RecyclerView.Adapter<ProductListAdapter.MyHolder> {

    List<ProductList> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;


    public ProductListAdapter( Context mcts, List<ProductList> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public ProductListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_product_list_adapter,parent,false);
        return new ProductListAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final ProductListAdapter.MyHolder holder, final int position) {

        final ProductList productList = list.get(position);

        holder.tv_productname.setText(productList.getProductname());
        holder.tv_productnumber.setText(productList.getProdct_number());
        holder.tv_productunit.setText(productList.getProdct_unit());
        holder.tv_productquantity.setText(productList.getQuantity());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public static class MyHolder extends RecyclerView.ViewHolder{
        TextView tv_productname,tv_productnumber,tv_productunit,tv_productquantity;


        public MyHolder(View itemView) {
            super(itemView);

            tv_productname = itemView.findViewById(R.id.productname);
            tv_productnumber = itemView.findViewById(R.id.product_number);
            tv_productunit = itemView.findViewById(R.id.productunit);
            tv_productquantity = itemView.findViewById(R.id.quantity);


        }
    }

}





