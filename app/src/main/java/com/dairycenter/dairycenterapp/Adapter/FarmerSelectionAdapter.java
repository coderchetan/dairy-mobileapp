package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;

public class FarmerSelectionAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<String> id;
    private ArrayList<String> firstname;


    public FarmerSelectionAdapter(Context c, ArrayList<String> id, ArrayList<String> firstname) {
        this.mContext = c;
        this.id = id;
        this.firstname = firstname;
    }



    @Override
    public int getCount() {
        return id.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int pos, View child, ViewGroup parent) {
        FarmerSelectionAdapter.Holder mHolder;
        LayoutInflater layoutInflater;
        if (child == null) {
            layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            child = layoutInflater.inflate(R.layout.farmer_selection_adapter, null);
            mHolder = new Holder();


            mHolder.txtid = (TextView) child.findViewById(R.id.textviewdg1);
            mHolder.txtfirstname = (TextView) child.findViewById(R.id.textviewdg2);

            child.setTag(mHolder);
        } else {
            mHolder = (FarmerSelectionAdapter.Holder) child.getTag();
        }

        mHolder.txtid.setText(id.get(pos));
        mHolder.txtfirstname.setText(firstname.get(pos));

        return child;
    }

    public static class Holder {

        TextView txtid;
        TextView txtfirstname;
    }
}