package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.CollectionReport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;
import java.util.List;

public class NoOfDaysMilkReportAdapter extends RecyclerView.Adapter<NoOfDaysMilkReportAdapter.MyHolder> {
    List<CollectionReport> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;

    public NoOfDaysMilkReportAdapter( Context mcts, List<CollectionReport> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public NoOfDaysMilkReportAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_no_of_days_milk_adapter,parent,false);
        return new NoOfDaysMilkReportAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final NoOfDaysMilkReportAdapter.MyHolder holder, final int position) {

        final CollectionReport collectionReport = list.get(position);

        holder.tv_id.setText(collectionReport.getId());
        holder.tv_farmername.setText(collectionReport.getFarmer_name());
        holder.tv_farmerno.setText(collectionReport.getFarmer_no());
        holder.tv_cattletpe.setText(collectionReport.getCattletype());
        holder.tv_shift.setText(collectionReport.getShift());
        holder.tv_weight.setText(collectionReport.getWeight());
        holder.tv_fat.setText(collectionReport.getFat());
        holder.tv_lacto.setText(collectionReport.getLacto());
        holder.tv_snf.setText(collectionReport.getSnf());
        holder.tv_rate.setText(collectionReport.getRate());
        holder.tv_amount.setText(collectionReport.getAmount());
        holder.tv_date.setText(collectionReport.getDates());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        TextView tv_id,tv_farmerno,tv_farmername,tv_cattletpe,tv_shift,tv_weight,tv_fat,tv_snf,tv_lacto,tv_rate,tv_amount,tv_date;
        public MyHolder(View itemView) {
            super(itemView);

            tv_id = itemView.findViewById(R.id.id);
            tv_farmerno = itemView.findViewById(R.id.farmer_no);
            tv_farmername = itemView.findViewById(R.id.farmername);
            tv_cattletpe = itemView.findViewById(R.id.cattletype);
            tv_shift = itemView.findViewById(R.id.shift);
            tv_weight = itemView.findViewById(R.id.weights);
            tv_fat = itemView.findViewById(R.id.fat);
            tv_snf = itemView.findViewById(R.id.snf);
            tv_lacto = itemView.findViewById(R.id.lacto);
            tv_rate = itemView.findViewById(R.id.rate);
            tv_amount = itemView.findViewById(R.id.amount);
            tv_date = itemView.findViewById(R.id.date);
        }
    }
}

