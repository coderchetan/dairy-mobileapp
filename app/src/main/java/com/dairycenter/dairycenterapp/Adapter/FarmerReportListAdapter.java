package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.Farmerreport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;

import java.util.ArrayList;
import java.util.List;

public class FarmerReportListAdapter extends RecyclerView.Adapter<FarmerReportListAdapter.MyHolder> {

    List<Farmerreport> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;


    public FarmerReportListAdapter( Context mcts, List<Farmerreport> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public FarmerReportListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_farmer_report_list_adapter,parent,false);
        return new FarmerReportListAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final FarmerReportListAdapter.MyHolder holder, final int position) {

        final Farmerreport farmerreport = list.get(position);

        holder.tv_farmername.setText(farmerreport.getFarmer_name());
        holder.tv_farmerno.setText(farmerreport.getFarmer_no());
        holder.tv_address.setText(farmerreport.getAddress());
        holder.tv_mobno.setText(farmerreport.getMobno());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyHolder extends RecyclerView.ViewHolder{

        TextView tv_farmerno,tv_farmername,tv_address,tv_mobno;
        public MyHolder(View itemView) {
            super(itemView);


            tv_farmerno = itemView.findViewById(R.id.farmer_no);
            tv_farmername = itemView.findViewById(R.id.farmername);
            tv_address = itemView.findViewById(R.id.shift);
            tv_mobno = itemView.findViewById(R.id.weights);

        }
    }
}

