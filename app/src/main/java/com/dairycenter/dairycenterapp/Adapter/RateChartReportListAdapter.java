package com.dairycenter.dairycenterapp.Adapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.CollectionReport;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.RateChartReport;

import java.util.ArrayList;
import java.util.List;

public class RateChartReportListAdapter extends RecyclerView.Adapter<RateChartReportListAdapter.MyHolder> {
    List<RateChartReport> list = new ArrayList<>();
    ItemClickListener itemClickListener;
    public Context mcts;


    public RateChartReportListAdapter( Context mcts, List<RateChartReport> list) {
        this.list = list;
        this.mcts = mcts;
    }

    @Override
    public RateChartReportListAdapter.MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_rate_chart_report_list_adapter,parent,false);
        return new RateChartReportListAdapter.MyHolder(view);
    }

    @Override
    public void onBindViewHolder(final RateChartReportListAdapter.MyHolder holder, final int position) {

        final RateChartReport rateChartReport = list.get(position);

        holder.id.setText(rateChartReport.getId());
        holder.weightingscale.setText(rateChartReport.getWeightingscale());
        holder.analyaerscale.setText(rateChartReport.getAnalyaerscale());
        holder.chartname.setText(rateChartReport.getChartname());
        holder.ratechart.setText(rateChartReport.getRatechart());
        holder.chrttype.setText(rateChartReport.getChrttype());
        holder.cratedat.setText(rateChartReport.getCratedat());
        holder.fatrangefrm.setText(rateChartReport.getFatrangefrm());
        holder.fatrangeto.setText(rateChartReport.getFatrangeto());
        holder.fatsnfrangefrm.setText(rateChartReport.getFatsnfrangefrm());
        holder.fatsnfrangeto.setText(rateChartReport.getFatsnfrangeto());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
    public static class MyHolder extends RecyclerView.ViewHolder{
        TextView id,weightingscale,analyaerscale,chartname,ratechart,chrttype,cratedat,fatrangefrm,fatrangeto,fatsnfrangefrm,fatsnfrangeto;
        public MyHolder(View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.id);
            weightingscale = itemView.findViewById(R.id.wtscalemode);
            analyaerscale = itemView.findViewById(R.id.analyzer_mode);
            chartname = itemView.findViewById(R.id.chart_name);
            ratechart = itemView.findViewById(R.id.rate_chart);
            chrttype = itemView.findViewById(R.id.chart_type);
            cratedat = itemView.findViewById(R.id.createdat);
            fatrangefrm = itemView.findViewById(R.id.fat_rng_frm);
            fatrangeto = itemView.findViewById(R.id.fat_range_to);
            fatsnfrangefrm = itemView.findViewById(R.id.fatsnfrangefrm);
            fatsnfrangeto = itemView.findViewById(R.id.fatanfrange_to);
        }
    }
}
