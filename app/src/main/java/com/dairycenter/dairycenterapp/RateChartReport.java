package com.dairycenter.dairycenterapp;

public class RateChartReport {
    String id,weightingscale,analyaerscale,chartname,ratechart,chrttype,cratedat,fatrangefrm,fatrangeto,fatsnfrangefrm,fatsnfrangeto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWeightingscale() {
        return weightingscale;
    }

    public void setWeightingscale(String weightingscale) {
        this.weightingscale = weightingscale;
    }

    public String getAnalyaerscale() {
        return analyaerscale;
    }

    public void setAnalyaerscale(String analyaerscale) {
        this.analyaerscale = analyaerscale;
    }

    public String getChartname() {
        return chartname;
    }

    public void setChartname(String chartname) {
        this.chartname = chartname;
    }

    public String getRatechart() {
        return ratechart;
    }

    public void setRatechart(String ratechart) {
        this.ratechart = ratechart;
    }

    public String getChrttype() {
        return chrttype;
    }

    public void setChrttype(String chrttype) {
        this.chrttype = chrttype;
    }

    public String getCratedat() {
        return cratedat;
    }

    public void setCratedat(String cratedat) {
        this.cratedat = cratedat;
    }

    public String getFatrangefrm() {
        return fatrangefrm;
    }

    public void setFatrangefrm(String fatrangefrm) {
        this.fatrangefrm = fatrangefrm;
    }

    public String getFatrangeto() {
        return fatrangeto;
    }

    public void setFatrangeto(String fatrangeto) {
        this.fatrangeto = fatrangeto;
    }

    public String getFatsnfrangefrm() {
        return fatsnfrangefrm;
    }

    public void setFatsnfrangefrm(String fatsnfrangefrm) {
        this.fatsnfrangefrm = fatsnfrangefrm;
    }

    public String getFatsnfrangeto() {
        return fatsnfrangeto;
    }

    public void setFatsnfrangeto(String fatsnfrangeto) {
        this.fatsnfrangeto = fatsnfrangeto;
    }
}
