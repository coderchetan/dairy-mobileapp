package com.dairycenter.dairycenterapp;

public class SalesTransaction {

    String productname,productnumber,rate,available_quan,amount;

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductnumber() {
        return productnumber;
    }

    public void setProductnumber(String productnumber) {
        this.productnumber = productnumber;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAvailable_quan() {
        return available_quan;
    }

    public void setAvailable_quan(String available_quan) {
        this.available_quan = available_quan;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
