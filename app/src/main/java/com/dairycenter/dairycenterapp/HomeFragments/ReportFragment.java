package com.dairycenter.dairycenterapp.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.dairycenter.dairycenterapp.CollectionHistoryListActivity;
import com.dairycenter.dairycenterapp.CollectionReportListActivity;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.FarmerReportListActivity;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.NoOfDaysMilkReportActivity;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.RateChartReportListActivity;
import com.dairycenter.dairycenterapp.TruckSheetReportActivity;


public class ReportFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    LinearLayout l1,l2,l3,l4,l5,l6;
    Button close;
    DatabaseHelper db;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.home_toolbar);
        setupToolbar();

        l1 = (LinearLayout) view.findViewById(R.id.l1);
        l2 = (LinearLayout) view.findViewById(R.id.l2);
        l3 = (LinearLayout) view.findViewById(R.id.l3);
        l4 = (LinearLayout) view.findViewById(R.id.l4);
        l5 = (LinearLayout) view.findViewById(R.id.l5);
        l6 = (LinearLayout) view.findViewById(R.id.l6);
        close = (Button) view.findViewById(R.id.cls);

        l1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), CollectionReportListActivity.class);
                startActivity(i);
            }
        });

        l2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), FarmerReportListActivity.class);
                startActivity(i);
            }
        });

        l3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), CollectionHistoryListActivity.class);
                startActivity(i);
            }
        });

        l4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity().getApplicationContext(), RateChartReportListActivity.class);
                startActivity(i);
            }
        });

        l5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), TruckSheetReportActivity.class);
                startActivity(i);
            }
        });


        l6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), NoOfDaysMilkReportActivity.class);
                startActivity(i);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    private void setupToolbar() {
        toolbar.setTitle("Reports");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}
