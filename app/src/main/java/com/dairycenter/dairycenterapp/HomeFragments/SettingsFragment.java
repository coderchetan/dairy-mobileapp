package com.dairycenter.dairycenterapp.HomeFragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SettingsFragment extends Fragment {

    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static Spinner spinner_mode1,spinner_mode2;
    DatabaseHelper db;
    TextView chilling,centername,cow_morning,buffalo_morning,cow_evening,buffalo_evening;
    String chilling_name,center_name,cowmor,buffalomor,coweve,buffeve,rmcu_name,center_names;

    public static final String LOGIN_URL = "http://mcsoft.whitegoldtech.com:8888/api/loginform";

    Button save_settings,cancel,close;
    String wenscal1,wenscale2;
    private TextInputEditText editTextUsername;
    private TextInputEditText editTextPassword;
    private TextInputLayout emails;
    private TextInputLayout pass;
    public static final String KEY_USERNAME="email";
    public static final String KEY_PASSWORD="password";
    public static ProgressBar loading;

    public static String modes,modes2,mode,mode2;

    private int i = 0;
    private Handler handler = new Handler();
    private ProgressDialog progressDialog;
    private int progresstatus = 0;



    public SettingsFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        toolbar          = (Toolbar) view.findViewById(R.id.home_toolbar);
        spinner_mode1    = (Spinner) view.findViewById(R.id.spinner_mode1);
        spinner_mode2    = (Spinner) view.findViewById(R.id.spinner_mode2);
        cow_morning      = (TextView) view.findViewById(R.id.cow_morning);
        buffalo_morning  = (TextView) view.findViewById(R.id.buffalo_morning);
        cow_evening      = (TextView) view.findViewById(R.id.cow_evening);
        buffalo_evening  = (TextView) view.findViewById(R.id.buffalo_evening);
        chilling         = (TextView) view.findViewById(R.id.abc_chilling_center);
        centername       = (TextView) view.findViewById(R.id.abc_milk_collection_center);
        save_settings    = (Button) view.findViewById(R.id.save);
        cancel           = (Button) view.findViewById(R.id.cancel);
        close            = (Button) view.findViewById(R.id.cls);

        setupToolbar();


        modes = "";

        db = new DatabaseHelper(getActivity().getApplicationContext());
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.downloadsettingdata, null);
        if (mCursor.moveToFirst()) {
            do {
                cowmor        = mCursor.getString(mCursor.getColumnIndex("cowmorning"));
                buffalomor    = mCursor.getString(mCursor.getColumnIndex("buffelomorning"));
                coweve        = mCursor.getString(mCursor.getColumnIndex("cowevening"));
                buffeve       = mCursor.getString(mCursor.getColumnIndex("buffeloevening"));
                listratechart();
            } while (mCursor.moveToNext());
        }

        SQLiteDatabase dataBase2 = db.getWritableDatabase();
        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM "
                + DatabaseHelper.rmcumaster, null);
        if (mCursor2.moveToFirst()) {
            do {
                rmcu_name = mCursor2.getString(mCursor2.getColumnIndex(DatabaseHelper.rmcuname));
                chilling.setText(rmcu_name);
            } while (mCursor2.moveToNext());
        }

        SQLiteDatabase dataBase3 = db.getWritableDatabase();
        Cursor mCursor3 = dataBase3.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor3.moveToFirst()) {
            do {
                center_names = mCursor3.getString(mCursor3.getColumnIndex(DatabaseHelper.cenetrname));
                centername.setText(center_names);

            } while (mCursor3.moveToNext());
        }

        //Auto or Manual Selction Spinner....
        final List<String> list = new ArrayList<String>();
        SQLiteDatabase dataBasel = db.getWritableDatabase();
        final Cursor mCursors = dataBasel.rawQuery("SELECT * FROM "
                + DatabaseHelper.COLLECTIONMODE, null);
        if (mCursors.moveToFirst()) {
            do {
                mode = mCursors.getString(mCursors.getColumnIndex("st1"));
                if(mode.equals("Auto")){
                    list.add(0,"Auto");
                    list.add(1,"Manual");
                    Log.d("mrus",mode);
                }else if(mode.equals("Manual")){
                    list.add(0,"Manual");
                    list.add(1,"Auto");
                    Log.d("mrusd",mode);
                }
            } while (mCursors.moveToNext());
        } else if(mCursors.getCount() == 0) {
            list.add("Auto");
            list.add("Manual");

        }
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_mode1.setAdapter(dataAdapter);
        spinner_mode1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                wenscal1 = parent.getItemAtPosition(position1).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });



        //Auto or Manual Selction Spinner....2>
        final List<String> list2 = new ArrayList<String>();
        SQLiteDatabase dataBaseee = db.getWritableDatabase();
        Cursor mCursorss = dataBaseee.rawQuery("SELECT * FROM "
                + DatabaseHelper.COLLECTIONMODE, null);
        if (mCursorss.moveToFirst()) {
            do {
                mode2 = mCursorss.getString(mCursorss.getColumnIndex("st2"));
                if(mode2.equals("Auto")){
                    list2.add(0,"Auto");
                    list2.add(1,"Manual");
                }else if(mode2.equals("Manual")){
                    list2.add(0,"Manual");
                    list2.add(1,"Auto");
                }
            } while (mCursorss.moveToNext());
        } else if(mCursorss.getCount() == 0) {
            list2.add("Auto");
            list2.add("Manual");
        }

        final ArrayAdapter<String> dataAdapters =
                new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list2);
        dataAdapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_mode2.setAdapter(dataAdapters);
        spinner_mode2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                wenscale2 = parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        save_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase dataBaseee = db.getWritableDatabase();
                Cursor mCursorss = dataBaseee.rawQuery("SELECT * FROM "
                        + DatabaseHelper.COLLECTIONMODE, null);
                if (mCursorss.moveToFirst()) {
                    do {
                        mode = mCursorss.getString(mCursorss.getColumnIndex("st1"));
                        mode2 = mCursorss.getString(mCursorss.getColumnIndex("st2"));
                    } while (mCursorss.moveToNext());
                }
                if(mCursorss.getCount() == 0  && spinner_mode1.getSelectedItem().toString().equals("Auto") &&
                        spinner_mode2.getSelectedItem().toString().equals("Auto")) {
                    Toast.makeText(getActivity().getApplicationContext(),"Please select collection mode",Toast.LENGTH_LONG).show();
                }else if(mCursorss.getCount() == 0  && spinner_mode1.getSelectedItem().toString().equals("Manual") &&
                        spinner_mode2.getSelectedItem().toString().equals("Manual")) {
                    openDiologue();
                }else if(mode.equals(spinner_mode1.getSelectedItem().toString()) && mode2.equals(spinner_mode2.getSelectedItem().toString())) {
                    Toast.makeText(getActivity().getApplicationContext(),"Setting already saved",Toast.LENGTH_LONG).show();
                }else{
                    openDiologue();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner_mode1.setSelection(0);
                spinner_mode2.setSelection(0);
            }
        });
        return view;
    }

    public void listratechart(){
        db = new DatabaseHelper(getActivity().getApplicationContext());
        SQLiteDatabase dataBase1 = db.getWritableDatabase();
        Cursor mCursor1 = dataBase1.rawQuery("SELECT * FROM "
                + DatabaseHelper.listRatechart, null);
        if (mCursor1.moveToFirst()) {
            do {
                if (cowmor.equals(mCursor1.getString(mCursor1.getColumnIndex("id_R")))) {
                    String cowr = mCursor1.getString(mCursor1.getColumnIndex("chartName"));
                    cow_morning.setText(cowr);
                }
                if (buffalomor.equals(mCursor1.getString(mCursor1.getColumnIndex("id_R")))) {
                    String bufr = mCursor1.getString(mCursor1.getColumnIndex("chartName"));
                    buffalo_morning.setText(bufr);
                }
                if (coweve.equals(mCursor1.getString(mCursor1.getColumnIndex("id_R")))) {
                    String bufr = mCursor1.getString(mCursor1.getColumnIndex("chartName"));
                    cow_evening.setText(bufr);
                }
                if (buffeve.equals(mCursor1.getString(mCursor1.getColumnIndex("id_R")))) {
                    String bufr = mCursor1.getString(mCursor1.getColumnIndex("chartName"));
                    buffalo_evening.setText(bufr);
                }

            } while (mCursor1.moveToNext());

        }
    }

    public void openDiologue() {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.authontication_dialog, null);

        editTextUsername      =    (TextInputEditText) view.findViewById(R.id.email);
        editTextPassword      =    (TextInputEditText) view.findViewById(R.id.password);
        emails                =    (TextInputLayout)view.findViewById(R.id.em);
        pass                  =    (TextInputLayout)view.findViewById(R.id.pass);
        Button b1             =    (Button) view.findViewById(R.id.b1);
        loading               =    (ProgressBar)view.findViewById(R.id.loading);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        loading.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = editTextUsername.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                String st1;
                String st2;

                db    =   new DatabaseHelper(getActivity().getApplicationContext());
                db.deleteFarmerRegRecord3();
                if(!email.isEmpty() || !password.isEmpty()){
                    Login(email,password);
                    dialog.cancel();

                    st1 = spinner_mode1.getSelectedItem().toString();
                    st2 = spinner_mode2.getSelectedItem().toString();

                    db.insetcollectionmode(st1, st2);

                    SQLiteDatabase dataBasel = db.getWritableDatabase();
                    Cursor mCursors = dataBasel.rawQuery("SELECT * FROM "
                            + DatabaseHelper.COLLECTIONMODE, null);
                    if (mCursors.moveToFirst()) {
                        do {
                            modes = mCursors.getString(mCursors.getColumnIndex("st1"));
                            modes2 = mCursors.getString(mCursors.getColumnIndex("st2"));
                            Log.d("zzz",modes);
                        } while (mCursors.moveToNext());
                    }

                    final List<String> list = new ArrayList<String>();
                    if(modes.equals("Auto")){
                        list.add(0,"Auto");
                        list.add(1,"Manual");
                    }else if(modes.equals("Manual")){
                        list.add(0,"Manual");
                        list.add(1,"Auto");
                    }
                    final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_mode1.setAdapter(dataAdapter);
                    spinner_mode1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position1, long id) {
                            wenscal1 = parent.getItemAtPosition(position1).toString();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    final List<String> list2 = new ArrayList<String>();
                    if(modes2.equals("Auto")){
                        list2.add(0,"Auto");
                        list2.add(1,"Manual");
                    }else if(modes2.equals("Manual")){
                        list2.add(0,"Manual");
                        list2.add(1,"Auto");
                    }
                    final ArrayAdapter<String> dataAdapters =
                            new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list2);
                    dataAdapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_mode2.setAdapter(dataAdapters);
                    spinner_mode2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            wenscale2 = parent.getItemAtPosition(position).toString();
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });


                    clear();
                }else {
                    emails.setError("Please insert email");
                    requestFocus(editTextUsername);
                    pass.setError("Please insert password");
                    requestFocus(editTextPassword);
                }
            }
        });
    }

    private void clear(){
        spinner_mode1.setSelection(0);
        spinner_mode2.setSelection(0);
    }

    public static String getmode(){
        return modes;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void Login(final String editTextUsername, final String getEditTextPassword) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            boolean success = obj.getBoolean("success");
                            if(success == true ) {
                                JSONObject object= obj.getJSONObject("data");

                                if(object!=null) {

                                    if(object.getString("center_id").trim().equals("") && object.getString("center_name").trim().equals("")){
                                        Toast.makeText(getActivity().getApplicationContext(),"User not allocated to any center application.",Toast.LENGTH_LONG).show();

                                    }else{

                                        String id          =    object.getString("id").trim();
                                        String username    =    object.getString("user_name").trim();
                                        String centerid    =    object.getString("center_id").trim();
                                        String centername  =    object.getString("center_name").trim();

                                        Toast.makeText(getActivity().getApplicationContext(),"Settings save successfully",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            } else{
                                Toast.makeText(getActivity().getApplicationContext(),"Invalid user",Toast.LENGTH_LONG).show();
                            }
                        }catch(JSONException e){
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity().getApplicationContext(), "Network connection Failed, please try again.", Toast.LENGTH_LONG).show();

                    }
                })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put(KEY_USERNAME, editTextUsername);
                map.put(KEY_PASSWORD, getEditTextPassword);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setupToolbar() {
        toolbar.setTitle("Transaction Settings");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}



