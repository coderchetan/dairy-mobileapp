package com.dairycenter.dairycenterapp.HomeFragments;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.Adapter.FarmerSelectionAdapter;
import com.dairycenter.dairycenterapp.Adapter.SalesTransactionAdapter;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.ItemClickListener1;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.SalesTransaction;
import com.dairycenter.dairycenterapp.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SalesTransactionFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static TextView farmerNo,date,farmerNoComplete;
    public static TextView farmerName;
    public static EditText textViewtext1,farmernocomplete;
    public static EditText textViewtext2;
    DatabaseHelper db;
    public static String name;
    public static String GetID;
    public static ArrayList<String> id = new ArrayList<String>();
    public static ArrayList<String> firstname= new ArrayList<String>();
    public static ArrayList<String> lastname= new ArrayList<String>();
    public static ListView userList1;
    Spinner s1;
    String manufacture,rate,avilable_quantity,unit_of_scale,subsidy_per,productnumber;
    TextView manufactures,unitofscale,subsidy,rates,available_qua,local,amounts;
    Button sales_add,save,cancel,close;
    EditText Quantity;
    RecyclerView recyclerView;
    SalesTransactionAdapter adapter;
    List<SalesTransaction> list = new ArrayList<>();
    public static final String SALES_API = "http://mcsoft.whitegoldtech.com:8888/api/salesform";
    String Demo,rmcuname,rmcuid,farmernumber,reversedate,centername,centerid,unitname;
    Date mdate;
    double available_quantity,ratess,quantitys,avaliblequans,fisrtdigit,editvalue;
    TextView discount,editvalues;
    int sumavilable_quantity;
    TextView saletransaction_name,txtdate,txttime,farmer_notxt,farmer_nametxt,productnametxt,quantitytxt,
            amounttxt,manufactretxt,ratetxt,availablequantxt,unitofscaletxt,subsidytxt,discountxt,product_numbertxt,
            productnametxt2,ratetxt2,quantitytxt2,edittxt,deletetxt;

    public SalesTransactionFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sales_transaction, container, false);

        toolbar           = (Toolbar) view.findViewById(R.id.home_toolbar);
        farmerNo          = (TextView) view.findViewById(R.id.farmernumbersales);
        farmerNoComplete  = (TextView) view.findViewById(R.id.farmernumbersalescomplete);
        farmerName        = (TextView) view.findViewById(R.id.farmernamesale);
        date              = (TextView) view.findViewById(R.id.datesale);

        s1                = (Spinner) view.findViewById(R.id.spinner1);
        manufactures      = (TextView) view.findViewById(R.id.manufacture);
        unitofscale       = (TextView) view.findViewById(R.id.unitofsale);
        subsidy           = (TextView) view.findViewById(R.id.subsidy_per);
        rates             = (TextView) view.findViewById(R.id.rate);
        available_qua     = (TextView) view.findViewById(R.id.availquant);
        local             = (TextView) view.findViewById(R.id.local);
        amounts           = (TextView) view.findViewById(R.id.amount);
        discount          = (TextView) view.findViewById(R.id.discount);
        editvalues         = (TextView) view.findViewById(R.id.editvalue);

        sales_add         = (Button) view.findViewById(R.id.sales_add);
        save              = (Button) view.findViewById(R.id.save);
        Quantity          = (EditText) view.findViewById(R.id.Quantity);
        cancel            = (Button) view.findViewById(R.id.cancel);
        close             = (Button) view.findViewById(R.id.cls);

        saletransaction_name   = (TextView) view.findViewById(R.id.Prod_sale);        txtdate        = (TextView) view.findViewById(R.id.txtdate);
        txttime                = (TextView) view.findViewById(R.id.txttime);          farmer_notxt   = (TextView) view.findViewById(R.id.farmer_notxt);
        farmer_nametxt         = (TextView) view.findViewById(R.id.far_nametxt);      productnametxt = (TextView) view.findViewById(R.id.prod_nametxt);
        quantitytxt            = (TextView) view.findViewById(R.id.quant_txt);        amounttxt      = (TextView) view.findViewById(R.id.amount_txt);
        manufactretxt          = (TextView) view.findViewById(R.id.manuf_txt);        ratetxt        = (TextView) view.findViewById(R.id.ratetxt);
        availablequantxt       = (TextView) view.findViewById(R.id.avil_txt);         unitofscaletxt = (TextView) view.findViewById(R.id.unitofscaletxt);
        subsidytxt             = (TextView) view.findViewById(R.id.subsidy_txt);      discountxt     = (TextView) view.findViewById(R.id.disctxt);
        product_numbertxt      = (TextView) view.findViewById(R.id.pro);              productnametxt2 = (TextView) view.findViewById(R.id.pro2);
        ratetxt2               = (TextView) view.findViewById(R.id.prounit);          quantitytxt2    = (TextView) view.findViewById(R.id.pro3);
        edittxt                = (TextView) view.findViewById(R.id.pro5);             deletetxt      = (TextView) view.findViewById(R.id.deletetxt);


        recyclerView   =        (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        adapter = new SalesTransactionAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);


        addProductName();
        setvalue();
        setvalue2();
        calculateamount();
        setCenterName();


        Calendar calendar    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat.format(today);
        date.setText(currentdate);

        String strt = date.getText().toString();
        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mdate = formats.parse(strt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        reversedate = simpleDate.format(mdate);

        db = new DatabaseHelper(getActivity().getApplicationContext());

        farmerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDiologue();
            }
        });

        sales_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s1.getSelectedItem().toString().equals("Select")) {
                    if (s1.getChildAt(0) != null && s1.getChildAt(0) instanceof TextView) {
                        s1.getChildAt(0).setFocusable(true);
                        ((TextView) s1.getChildAt(0)).setError("error");
                        s1.getChildAt(0).requestFocus();
                    }
                } else if (Quantity.getText().toString().isEmpty()) {
                    Quantity.setError("please enter quantity");
                    Quantity.requestFocus();
                } else if (farmerNo.getText().toString().equals("")) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please select farmer", Toast.LENGTH_LONG).show();
                } else {
                    String proname = s1.getSelectedItem().toString();
                    String quantity = Quantity.getText().toString();
                    String rate = rates.getText().toString();
                    String amount = amounts.getText().toString();
                    String available_quan = available_qua.getText().toString();


                    Demo = "Data";
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).getProductname().contains(proname)) {
                            Toast.makeText(getActivity(), "Product already added", Toast.LENGTH_SHORT).show();
                            clear();
                            Demo = "Demo";
                        }
                    }


                    if (!Demo.toString().equals("Demo")) {
                        SalesTransaction salesTransaction = new SalesTransaction();
                        salesTransaction.setProductname(proname);
                        salesTransaction.setProductnumber(productnumber);
                        salesTransaction.setRate(rate);
                        salesTransaction.setAvailable_quan(quantity);
                        salesTransaction.setAmount(amount);

                        list.add(salesTransaction);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Add", Toast.LENGTH_SHORT).show();
                        clear();
                    }
                }
            }
        });


        adapter.setOnItemClickListener(new ItemClickListener1() {
            @Override
            public void OnItemClick(int position, SalesTransaction salesTransaction, String prodnumber, String prodname, String prodrate, String edit, String amount) {
                SalesTransaction salesTransaction1 = new SalesTransaction();
                salesTransaction1.setProductnumber(prodnumber);
                salesTransaction1.setProductname(prodname);
                salesTransaction1.setRate(prodrate);
                salesTransaction1.setAvailable_quan(edit);
                salesTransaction1.setAmount(amount);

                list.remove(position);
                list.add(position, salesTransaction1);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "Edited", Toast.LENGTH_SHORT).show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.isEmpty()) {
                    Toast.makeText(getActivity(), "Please add product", Toast.LENGTH_SHORT).show();
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        db.insertsales_transaction(null, list.get(i).getProductname(), list.get(i).getProductnumber(), list.get(i).getRate(), list.get(i).getAvailable_quan());
                    }
                    ragi();
                    Toast.makeText(getActivity(), "Save successfully", Toast.LENGTH_SHORT).show();
                    list.clear();
                    adapter.notifyDataSetChanged();
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s1.setSelection(0);
                Quantity.setText("");
                rates.setText("");
                amounts.setText("");
                manufactures.setText("");
                available_qua.setText("");
                unitofscale.setText("");
                subsidy.setText("");
                discount.setText("");
                farmerName.setText("");
                farmerNo.setText("");
                list.clear();
                adapter.notifyDataSetChanged();
            }
        });


        Quantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {

                String quantity = Quantity.getText().toString();
                if (quantity.length() == 1) {
                    quantitys = Double.parseDouble(quantity);
                }else if(quantity.length() == 2){
                    quantitys = Double.parseDouble(quantity);
                }else if(quantity.length() == 3){
                    quantitys = Double.parseDouble(quantity);
                }

                if(quantity.length() == 1){
                    fisrtdigit = Double.parseDouble(quantity);
                    Log.d("fisrtdigit",Double.toString(fisrtdigit));
                }

                String rate = rates.getText().toString();
                if (rate.length() > 0) {
                    ratess = Double.parseDouble(rate);
                }

                String editvaluess = editvalues.getText().toString();
                if (editvaluess.length() > 0) {
                    editvalue = Double.parseDouble(editvaluess);
                }

                String avaliblequan = available_qua.getText().toString();
                if (avaliblequan.length() > 0) {
                    avaliblequans = Double.parseDouble(avaliblequan);
                }

                double Calrate = calculateamounts();
                double Calavailble = calculateavailquantity();

                Log.d("Calavailble",Double.toString(Calavailble));

                amounts.setText(Double.toString(Calrate));
                available_qua.setText(Double.toString(Calavailble));
            }
        });


        setupToolbar();
        return view;
    }

    double calculateamounts(){
        return (ratess * (quantitys));
    }
    double calculateavailquantity(){
        return editvalue - (quantitys);
    }

    public void clear() {
        s1.setSelection(0);
        Quantity.setText("");
        rates.setText("");
        amounts.setText("");
        manufactures.setText("");
        available_qua.setText("");
        unitofscale.setText("");
        subsidy.setText("");
        discount.setText("");
    }


    public void setCenterName() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor2.moveToFirst()) {
            do {
                centername = mCursor2.getString(mCursor2.getColumnIndex(DatabaseHelper.cenetrname));
                centerid = mCursor2.getString(mCursor2.getColumnIndex("id"));
            } while (mCursor2.moveToNext());
        }
    }




    private void ragi() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, SALES_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePostPurchase:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getActivity().getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                VolleyLog.d("error",error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                JSONArray array = new JSONArray();
                for (int i = 0; i < list.size(); i++) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("ProductName", list.get(i).getProductname());
                        object.put("rate", list.get(i).getRate());
                        object.put("quantity", list.get(i).getAvailable_quan());
                        object.put("productnumber", list.get(i).getProductnumber());
                        object.put("farmername", farmerName.getText().toString());
                        object.put("farmernumber", farmerNoComplete.getText().toString());
                        object.put("date", reversedate);
                        object.put("amount", list.get(i).getAmount());
                        object.put("centerid", centerid);
                        object.put("centername", centername);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    array.put(object);
                }

                map.put("data", array.toString());
                Log.d("dataobject:",array.toString());

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public  void addProductName(){
        db = new DatabaseHelper(getActivity());
        db.getWritableDatabase();

        List<String> list = db.getAllProductName();
        list.add(0, "Select");

        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        s1.setAdapter(dataAdapter);
        s1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String type = parent.getItemAtPosition(position).toString();
                local.setText(type);
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void setvalue(){
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                db = new DatabaseHelper(getActivity().getApplicationContext());
                SQLiteDatabase dataBase = db.getWritableDatabase();
                Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                        + DatabaseHelper.PRODUCT_MASTER, null);
                if (mCursor.moveToFirst()) {
                    do {
                        if (local.getText().toString().equals(mCursor.getString(mCursor.getColumnIndex("product_name")))) {
                            manufacture = mCursor.getString(mCursor.getColumnIndex("manufacture_details"));
                            unit_of_scale = mCursor.getString(mCursor.getColumnIndex("unit_scale"));
                            subsidy_per = mCursor.getString(mCursor.getColumnIndex("subsidy_percentage"));

                            manufactures.setText(manufacture);
                            subsidy.setText(subsidy_per);

                            Quantity.setText("");
                            amounts.setText("");

                            SQLiteDatabase dataBase2 = db.getWritableDatabase();
                            Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM "
                                    + DatabaseHelper.unitmaster, null);
                            if (mCursor2.moveToFirst()) {
                                do {
                                    if(unit_of_scale.toString().equals(mCursor2.getString(mCursor2.getColumnIndex("id")))){
                                        unitname = mCursor2.getString(mCursor2.getColumnIndex("unitname"));
                                        unitofscale.setText(unitname);
                                    }
                                } while (mCursor2.moveToNext());
                            }
                        }
                    } while (mCursor.moveToNext());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        local.addTextChangedListener(textWatcher);
    }

    public void setvalue2(){
        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                db = new DatabaseHelper(getActivity().getApplicationContext());
                SQLiteDatabase dataBase1 = db.getWritableDatabase();

                Cursor mCursor1 = dataBase1.rawQuery("SELECT * FROM purchaselist order by created_at DESC",null);

//                Cursor  mCursor1  = dataBase1.rawQuery("SELECT * FROM purchaselist order by created_at DESC" +
//                        "where productname = ?", new String[]{local.getText().toString()}, null);
                if (mCursor1.moveToFirst()) {
                    do {
                        if (local.getText().toString().equals(mCursor1.getString(mCursor1.getColumnIndex("productname")))) {

                            rate = mCursor1.getString(mCursor1.getColumnIndex("sale_rate"));
                            productnumber = mCursor1.getString(mCursor1.getColumnIndex("productnumber"));
                            String productquantity = mCursor1.getString(mCursor1.getColumnIndex("productquantity"));
                            String productname = mCursor1.getString(mCursor1.getColumnIndex("productname"));
                            String avilable_quantity = mCursor1.getString(mCursor1.getColumnIndex("allocatedstock"));
                            String created_at = mCursor1.getString(mCursor1.getColumnIndex("created_at"));
                            String status = "active";

                            db = new DatabaseHelper(getActivity().getApplicationContext());
                            SQLiteDatabase dataBase2 = db.getWritableDatabase();
                            Cursor mCursorss = dataBase2.rawQuery("select * from salesaddinactivedata", null);
                            if (mCursorss.moveToFirst()) {
                                do {
                                    if (!local.getText().toString().equals(mCursorss.getString(mCursorss.getColumnIndex("productname")))) {

                                        db.insertavailbalequantity(null, productname, productnumber, rate, avilable_quantity, created_at, sumavilable_quantity,status);

                                            rates.setText(rate);
                                            available_qua.setText(avilable_quantity);
                                            editvalues.setText(avilable_quantity);

//                                        manufactures.setText(manufacture);
//                                        subsidy.setText(subsidy_per);
//                                        unitofscale.setText(unitname);

                                        Log.d("112",avilable_quantity);
                                    }
                                } while (mCursorss.moveToNext()) ;
                            }else if (mCursorss.getCount() == 0){
                                db.insertavailbalequantity(null, productname, productnumber, rate, avilable_quantity, created_at, sumavilable_quantity,status);

                                    rates.setText(rate);
                                    available_qua.setText(avilable_quantity);
                                    editvalues.setText(avilable_quantity);


                                Log.d("113", avilable_quantity);
                            }
                        }
                    } while (mCursor1.moveToNext());
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        local.addTextChangedListener(textWatcher1);
    }

    public void calculateamount(){
        //....calculate amount.....
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!rates.getText().toString().equals("") && !Quantity.getText().toString().equals("")) {
                    float rate      =    Float.parseFloat(rates.getText().toString());
                    float quan      =    Float.parseFloat(Quantity.getText().toString());
                    double amount   =    rate * quan;
                    amounts.setText(Double.toString(amount));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        rates.addTextChangedListener(textWatcher2);
        Quantity.addTextChangedListener(textWatcher2);
    }

    private void openDiologue(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.layout_dialog,null);
        textViewtext1 = (EditText)view.findViewById(R.id.farmerno);
        textViewtext2 = (EditText) view.findViewById(R.id.farmername12);
        userList1 = (ListView)view.findViewById(R.id.listdialog);
        farmernocomplete = (EditText) view.findViewById(R.id.farmernocomplete);

        db = new DatabaseHelper(getActivity().getApplicationContext());
        db.getReadableDatabase();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.create();
        displayData();
        ListData();
        ClearText();
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(textViewtext2.getText().toString().length() >0) {
                    User u ;
                    u = new User();
                    farmerNo.setText(GetID);
                    farmerName.setText(textViewtext2.getText().toString());
                    farmerNoComplete.setText(farmernocomplete.getText().toString());
                }else{
                    ClearText();
                }
            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void displayData() {

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                GetID = textViewtext1.getText().toString();
                SQLiteDatabase dataBase = db.getWritableDatabase();
                User u;

                Cursor cursor = dataBase.rawQuery("select * from farmermaster where substr(farmer_number,-4) =?",
                        new String[]{String.valueOf(GetID)}, null);

                if (cursor !=null && cursor.getCount() > 0){
                    cursor.moveToFirst();

                    u = new User();
                    u.setCource(cursor.getString(cursor.getColumnIndex("farmername")));
                    String farmernocompletes = cursor.getString(cursor.getColumnIndex("farmer_number"));

                    textViewtext2.setText(u.getCource());
                    name= textViewtext2.getText().toString();
                    farmernocomplete.setText(farmernocompletes);

                    cursor.close();
                    db.close();

                }else {
//                    Toast.makeText(getActivity(), "farmer number invalid", Toast.LENGTH_SHORT).show();
                    cursor.close();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        textViewtext1.addTextChangedListener(textWatcher);

    }

    public void ClearText() {
        farmerNo.setText("");
        farmerName.setText("");

    }


    private void ListData() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);

        id.clear();
        firstname.clear();
//        lastname.clear();

        //fetch each record
        if (mCursor.moveToFirst()) {
            do {
                id.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNUMBER)));
                firstname.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNAME)));
//                    lastname.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.LASTNAME)));

            } while (mCursor.moveToNext());
        }

        //display to screen
        FarmerSelectionAdapter disadpt = new FarmerSelectionAdapter(getActivity().getApplicationContext(), id, firstname);
        userList1.setAdapter(disadpt);
        userList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ViewGroup vg = (ViewGroup)view;
                TextView txt = (TextView)vg.findViewById(R.id.textviewdg1);

                String no = txt.getText().toString();
                String lastdigit = no.substring((no.length() -4));

                textViewtext1.setText(lastdigit);
                farmernocomplete.setText(no);
            }
        });
        mCursor.close();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    private void setupToolbar() {
        toolbar.setTitle("Sales Transaction");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}
