package com.dairycenter.dairycenterapp.HomeFragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.Adapter.DeductionAdapter;
import com.dairycenter.dairycenterapp.Adapter.FarmerSelectionAdapter;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DeductionFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static TextView farmerNo,date,farmerNocomplete;
    public static TextView farmerName;
    public static EditText textViewtext1,farmernocomplete;
    public static EditText textViewtext2;
    DatabaseHelper db;
    public static String name;
    public static String GetID;

    public static ArrayList<String> id = new ArrayList<String>();
    public static ArrayList<String> firstname= new ArrayList<String>();
    public static ArrayList<String> lastname= new ArrayList<String>();

    public static ListView userList1;
    public static ListView userList2;
    public static ArrayList<String> ids= new ArrayList<String>();
    public static ArrayList<String> nameofdeductionss= new ArrayList<String>();
    public static ArrayList<String> typeofdeductionss= new ArrayList<String>();
    public static ArrayList<String> deductioncycless= new ArrayList<String>();
    public static ArrayList<String> deductionmethodess= new ArrayList<String>();
    public static ArrayList<String> deductionamounts= new ArrayList<String>();
    public static ArrayList<String> deductionbillingcycle= new ArrayList<String>();
    public static ArrayList<String> deductionremark= new ArrayList<String>();

    CheckBox checkBoxdeduction,c1_check;
    EditText remarkdeduction;

    Button repayment_deduction ,addnew_deduction;
    TextView addnewdeductiontxt,typeofdeduction,deduction_cycle,deduction_method,amount,repaymentbilling_cycle,nameofdeduction;

    TextView r1,r2,r3,r4,r5,r7,total,blnce,renameofdeduction;

    EditText r6,r8,totaledit,blncedit;
    Spinner spinner_d1,spinner_d2,spinner_d3,spinner_d4,spinner_d5;
    Spinner spinner_deduction1,spinner_deduction2,spinner_deduction3,spinner_deduction4,spinner_deduction5;

    EditText amounttxt,repaymnet_billingcycletxt;
    String nameofdeductions,typeofdeductions,deductioncycle,deductionmethod,amounts,billingcycle,remark,transactionid,
            deductionRepayment,farmerNumber;
    String Renameofdeductions, Retypeofdeductions,Redeductioncycle,Redeductionmethod,Reamounts,Rebillingcycle,Reremark,Retransactionid,
            RedeductionRepayment,RefarmerNumber;
    public  static Button savedededuction,close,cancel;

    public static Cursor mCursor;
    public static String updateid,statusdeactivate,status;
    public static final String deduction_save_url = "http://mcsoft.whitegoldtech.com:8888/api/deductionformnew";
    Date mdate;
    String reversedate;
    String nameofdedu,typeofdedu ,deducycle,dedumenthod,amt,billcycle,rmk,idss;


    public DeductionFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_deduction, container, false);

        toolbar          =     (Toolbar) view.findViewById(R.id.home_toolbar);
        farmerNo         =     (TextView) view.findViewById(R.id.farmernumberdeduction);
        farmerNocomplete =     (TextView) view.findViewById(R.id.farmernumberdeductioncomplete);
        farmerName       =     (TextView) view.findViewById(R.id.farmernamededuction);
        date             =     (TextView)view.findViewById(R.id.datededuction);
        savedededuction  =     (Button) view.findViewById(R.id.save);

        checkBoxdeduction =     (CheckBox) view.findViewById(R.id.c1_deduction);
        remarkdeduction   =     (EditText) view.findViewById(R.id.remarks_deduction);
        c1_check          =     (CheckBox) view.findViewById(R.id.c1_de);
        r8                =     (EditText)view.findViewById(R.id.r8);
        close             =     (Button) view.findViewById(R.id.cls);
        cancel            =     (Button) view.findViewById(R.id.cancel);


        remarkdeduction.setVisibility(view.GONE);
        checkBoxdeduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkBoxdeduction.isChecked()){
                    remarkdeduction.setVisibility(view.VISIBLE);
                }else if(!checkBoxdeduction.isChecked()){
                    remarkdeduction.setVisibility(view.GONE);
                }
            }
        });

        c1_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(c1_check.isChecked()){
                    r8.setVisibility(view.VISIBLE);
                }else if(!c1_check.isChecked()){
                    r8.setVisibility(view.GONE);
                }
            }
        });


        repayment_deduction     =    (Button)view.findViewById(R.id.repayment_button);
        addnew_deduction        =    (Button)view.findViewById(R.id.add_new_deduction);
        addnewdeductiontxt      =    (TextView)view.findViewById(R.id.add_new_deductiontxt);
        typeofdeduction         =    (TextView)view.findViewById(R.id.typeof_deduction);
        deduction_cycle         =    (TextView)view.findViewById(R.id.deduction_cycle);
        deduction_method        =    (TextView)view.findViewById(R.id.deduction_method);
        amount                  =    (TextView)view.findViewById(R.id.amounttxt);
        nameofdeduction         =    (TextView)view.findViewById(R.id.nameof_deduction);
        repaymentbilling_cycle  =    (TextView)view.findViewById(R.id.billing_cycle);
        spinner_deduction1      =    (Spinner)view.findViewById(R.id.spinner_deduction1);
        spinner_deduction2      =    (Spinner)view.findViewById(R.id.spinner_deduction2);
        spinner_deduction3      =    (Spinner)view.findViewById(R.id.spinner_deduction3);
        spinner_deduction4      =    (Spinner)view.findViewById(R.id.spinner_deduction4);
        spinner_deduction5      =    (Spinner)view.findViewById(R.id.spinner_namededuction);
        amounttxt               =    (EditText)view.findViewById(R.id.amount);


        r1                     = (TextView)view.findViewById(R.id.r1);
        r2                     = (TextView)view.findViewById(R.id.r2);
        r3                     = (TextView)view.findViewById(R.id.r3);
        r4                     = (TextView)view.findViewById(R.id.r4);
        r5                     = (TextView)view.findViewById(R.id.r5);
        r7                     = (TextView)view.findViewById(R.id.r7);
        total                  = (TextView)view.findViewById(R.id.total);
        blnce                  = (TextView)view.findViewById(R.id.blnce);
        renameofdeduction      = (TextView)view.findViewById(R.id.renameof_deduction);
        spinner_d1             = (Spinner)view.findViewById(R.id.spinner_d1);
        spinner_d2             = (Spinner)view.findViewById(R.id.spinner_d2);
        spinner_d3             = (Spinner)view.findViewById(R.id.spinner_d3);
        spinner_d4             = (Spinner)view.findViewById(R.id.spinner_d4);
        spinner_d5             = (Spinner)view.findViewById(R.id.respinner_namededuction);
        r6                     = (EditText)view.findViewById(R.id.r6);
        totaledit              = (EditText)view.findViewById(R.id.totaledit);
        blncedit               = (EditText)view.findViewById(R.id.blnceedit);




        r1.setVisibility(view.GONE);
        r2.setVisibility(view.GONE);
        r3.setVisibility(view.GONE);
        r4.setVisibility(view.GONE);
        r5.setVisibility(view.GONE);
        r7.setVisibility(view.GONE);
        spinner_d1.setVisibility(view.GONE);
        spinner_d2.setVisibility(view.GONE);
        spinner_d3.setVisibility(view.GONE);
        spinner_d4.setVisibility(view.GONE);
        spinner_d5.setVisibility(view.GONE);
        r6.setVisibility(view.GONE);
        r8.setVisibility(view.GONE);
        c1_check.setVisibility(view.GONE);
        total.setVisibility(View.GONE);
        blnce.setVisibility(View.GONE);
        totaledit.setVisibility(View.GONE);
        blncedit.setVisibility(View.GONE);
        renameofdeduction.setVisibility(View.GONE);



        addnewdeductiontxt.setVisibility(view.GONE);
        typeofdeduction.setVisibility(view.GONE);
        deduction_cycle.setVisibility(view.GONE);
        deduction_method.setVisibility(view.GONE);
        amount.setVisibility(view.GONE);
        nameofdeduction.setVisibility(View.GONE);
        repaymentbilling_cycle.setVisibility(view.GONE);
        spinner_deduction1.setVisibility(view.GONE);
        spinner_deduction2.setVisibility(view.GONE);
        spinner_deduction3.setVisibility(view.GONE);
        spinner_deduction4.setVisibility(view.GONE);
        spinner_deduction5.setVisibility(view.GONE);
        amounttxt.setVisibility(view.GONE);
        checkBoxdeduction.setVisibility(view.GONE);


        repayment_deduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDiologue2();
                r1.setVisibility(view.VISIBLE);
                r2.setVisibility(view.VISIBLE);
                r3.setVisibility(view.VISIBLE);
                r4.setVisibility(view.VISIBLE);
                r5.setVisibility(view.VISIBLE);
                r6.setVisibility(view.VISIBLE);
                r7.setVisibility(view.VISIBLE);
                spinner_d1.setVisibility(view.VISIBLE);
                spinner_d2.setVisibility(view.VISIBLE);
                spinner_d3.setVisibility(view.VISIBLE);
                spinner_d4.setVisibility(view.VISIBLE);
                spinner_d5.setVisibility(view.VISIBLE);
                c1_check.setVisibility(view.VISIBLE);
                total.setVisibility(View.VISIBLE);
                blnce.setVisibility(View.VISIBLE);
                totaledit.setVisibility(View.VISIBLE);
                blncedit.setVisibility(View.VISIBLE);
                renameofdeduction.setVisibility(View.VISIBLE);


                addnewdeductiontxt.setVisibility(view.GONE);
                typeofdeduction.setVisibility(view.GONE);
                deduction_cycle.setVisibility(view.GONE);
                deduction_method.setVisibility(view.GONE);
                amount.setVisibility(view.GONE);
                nameofdeduction.setVisibility(View.GONE);
                repaymentbilling_cycle.setVisibility(view.GONE);
                spinner_deduction1.setVisibility(view.GONE);
                spinner_deduction2.setVisibility(view.GONE);
                spinner_deduction3.setVisibility(view.GONE);
                spinner_deduction4.setVisibility(view.GONE);
                spinner_deduction5.setVisibility(view.GONE);
                amounttxt.setVisibility(view.GONE);
                checkBoxdeduction.setVisibility(view.GONE);
                remarkdeduction.setVisibility(view.GONE);


            }
        });

        addnew_deduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addnewdeductiontxt.setVisibility(view.VISIBLE);
                typeofdeduction.setVisibility(view.VISIBLE);
                deduction_cycle.setVisibility(view.VISIBLE);
                deduction_method.setVisibility(view.VISIBLE);
                amount.setVisibility(view.VISIBLE);
                nameofdeduction.setVisibility(view.VISIBLE);
                repaymentbilling_cycle.setVisibility(view.VISIBLE);
                spinner_deduction1.setVisibility(view.VISIBLE);
                spinner_deduction2.setVisibility(view.VISIBLE);
                spinner_deduction3.setVisibility(view.VISIBLE);
                spinner_deduction4.setVisibility(view.VISIBLE);
                spinner_deduction5.setVisibility(view.VISIBLE);
                amounttxt.setVisibility(view.VISIBLE);
                checkBoxdeduction.setVisibility(view.VISIBLE);

                r1.setVisibility(view.GONE);
                r2.setVisibility(view.GONE);
                r3.setVisibility(view.GONE);
                r4.setVisibility(view.GONE);
                r5.setVisibility(view.GONE);
                r7.setVisibility(view.GONE);
                spinner_d1.setVisibility(view.GONE);
                spinner_d2.setVisibility(view.GONE);
                spinner_d3.setVisibility(view.GONE);
                spinner_d4.setVisibility(view.GONE);
                spinner_d5.setVisibility(view.GONE);
                r6.setVisibility(view.GONE);
                r8.setVisibility(view.GONE);
                c1_check.setVisibility(view.GONE);
                total.setVisibility(view.GONE);
                blnce.setVisibility(view.GONE);
                totaledit.setVisibility(view.GONE);
                blncedit.setVisibility(view.GONE);
                renameofdeduction.setVisibility(view.GONE);
            }
        });


        Calendar calendar    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat.format(today);
        date.setText(currentdate);

        String strt = date.getText().toString();
        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mdate = formats.parse(strt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        reversedate = simpleDate.format(mdate);


        db = new DatabaseHelper(getActivity().getApplicationContext());

        setupToolbar();

        farmerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDiologue();
            }
        });

        Listdeductionname();
        List4();

        savedededuction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (remarkdeduction.isShown()) {
                    remark = remarkdeduction.getText().toString();
                    Log.d("remark", remark);
                }

                amounts = amounttxt.getText().toString();
                farmerNumber = farmerNo.getText().toString();
                transactionid = "addnewid";
                deductionRepayment = "Add New Deduction";

                status = "Active";

                if (spinner_deduction5.getSelectedItem().toString().equals("Select")) {
                    if (spinner_deduction5.getChildAt(0) != null && spinner_deduction5.getChildAt(0) instanceof TextView) {
                        spinner_deduction5.getChildAt(0).setFocusable(true);
                        ((TextView) spinner_deduction5.getChildAt(0)).setError("error");
                        spinner_deduction5.getChildAt(0).requestFocus();
                    }
                }else if (spinner_deduction1.getSelectedItem().toString().equals("Select")) {
                    if (spinner_deduction1.getChildAt(0) != null && spinner_deduction1.getChildAt(0) instanceof TextView) {
                        spinner_deduction1.getChildAt(0).setFocusable(true);
                        ((TextView) spinner_deduction1.getChildAt(0)).setError("error");
                        spinner_deduction1.getChildAt(0).requestFocus();
                    }
                } else if (spinner_deduction2.getSelectedItem().toString().equals("Select")) {
                    if (spinner_deduction2.getChildAt(0) != null && spinner_deduction1.getChildAt(0) instanceof TextView) {
                        spinner_deduction2.getChildAt(0).setFocusable(true);
                        ((TextView) spinner_deduction2.getChildAt(0)).setError("error");
                        spinner_deduction2.getChildAt(0).requestFocus();
                    }
                } else if (spinner_deduction3.getSelectedItem().toString().equals("Select")) {
                    if (spinner_deduction3.getChildAt(0) != null && spinner_deduction3.getChildAt(0) instanceof TextView) {
                        spinner_deduction3.getChildAt(0).setFocusable(true);
                        ((TextView) spinner_deduction3.getChildAt(0)).setError("error");
                        spinner_deduction3.getChildAt(0).requestFocus();
                    }
                } else if (amounttxt.getText().toString().isEmpty()) {
                    amounttxt.setError("please enter amount");
                    amounttxt.requestFocus();
                } else if (spinner_deduction4.getSelectedItem().toString().equals("Select")) {
                    if (spinner_deduction4.getChildAt(0) != null && spinner_deduction4.getChildAt(0) instanceof TextView) {
                        spinner_deduction4.getChildAt(0).setFocusable(true);
                        ((TextView) spinner_deduction4.getChildAt(0)).setError("error");
                        spinner_deduction4.getChildAt(0).requestFocus();
                    }
                } else if(farmerNo.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity().getApplicationContext(), "Please select farmer number", Toast.LENGTH_LONG).show();

                } else {
                    if(deductionRepayment != null) {
                        db.insertDeductionRegi(null,nameofdeductions, typeofdeductions, deductioncycle, deductionmethod, amounts,
                                billingcycle, remark, farmerNumber,status);

                        ragi(idss,nameofdeductions,typeofdeductions, deductioncycle,deductionmethod,amounts,billingcycle, remark, farmerNumber);

                        Toast.makeText(getActivity().getApplicationContext(), "Add new deduction successfully", Toast.LENGTH_LONG).show();
                        ClearTextDeduction();
                    }
                }


                //REpayment data save code......//
                if (r8.isShown()) {
                    Reremark = r8.getText().toString();
                    Log.d("remark", Reremark);
                }

                Reamounts = totaledit.getText().toString();
                farmerNumber = farmerNo.getText().toString();
                String amounts = r6.getText().toString();

                Log.d("amount",amounts);

                if(blncedit.getText().toString().equals("0.0")){
                    statusdeactivate =  "Deactivate";
                }else{
                    statusdeactivate =  "Active";
                }

                if (totaledit.getText().toString().isEmpty()
                        && r8.getText().toString().isEmpty() ) {
                } else {
                    if(RedeductionRepayment != null) {
//                        db.insertRepayment(null, Retypeofdeductions, Redeductioncycle, Redeductionmethod, Reamounts,
//                                Rebillingcycle, Reremark, RefarmerNumber, Retransactionid, RedeductionRepayment);
                        db.updatedata(updateid,Renameofdeductions, Retypeofdeductions, Redeductioncycle, Redeductionmethod, amounts,
                                Rebillingcycle, Reremark, farmerNumber,statusdeactivate);
                        Toast.makeText(getActivity().getApplicationContext(), "Repayment add Successfully", Toast.LENGTH_LONG).show();
                        ClearTextDeduction2();
                    }
                }
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                spinner_deduction1.setSelection(0);
                spinner_deduction2.setSelection(0);
                amounttxt.setText("");
                spinner_deduction3.setSelection(1);
                spinner_deduction4.setSelection(0);
                spinner_deduction5.setSelection(0);
                remarkdeduction.setText("");
                remarkdeduction.setText("");

                spinner_d1.setSelection(1);
                spinner_d2.setSelection(1);
                r6.setText("");
                totaledit.setText("");
                spinner_d3.setSelection(1);
                spinner_d4.setSelection(1);
                r8.setText("");
                blncedit.setText("");

                farmerName.setText("");
                farmerNo.setText("");
            }
        });

        return view;
    }

    public void Listdeductionname(){
        List<String> list = db.getAllNameDeduction();
        list.add(0,"Select");

        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_deduction5.setAdapter(dataAdapter);
        spinner_deduction5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               nameofdeductions = parent.getItemAtPosition(position).toString();
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }else{
                    ((TextView) view).setTextColor(R.color.bordercolor);
                }

                SQLiteDatabase dataBase = db.getWritableDatabase();
                Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM deductionmaster where deductionName = ?", new String[]{
                        String.valueOf(nameofdeductions)}, null);
                if (mCursor2.moveToFirst()) {
                    do {
                        String typeofdeduction = mCursor2.getString(mCursor2.getColumnIndex("deductionType"));
                        String deductioncycles = mCursor2.getString(mCursor2.getColumnIndex("deductionCycle"));
                        String deductionmethode = mCursor2.getString(mCursor2.getColumnIndex("deductionMethod"));
                        idss = mCursor2.getString(mCursor2.getColumnIndex("id"));

                        if(typeofdeduction.equals("1")){
                            typeofdeduction = "Regular";
                        }else if(typeofdeduction.equals("2")){
                            typeofdeduction = "Fix";
                        }else if(typeofdeduction.equals("3")){
                            typeofdeduction = "Other Deduction";
                        }

                        if(deductioncycles.equals("1")){
                             deductioncycles = "bill-wise";
                        }else if(deductioncycles.equals("2")){
                            deductioncycles = "yearly";
                        }

                        if(deductionmethode.equals("1")){
                            deductionmethode = "liter";
                        }else if(deductionmethode.equals("2")){
                            deductionmethode = "fix-amount";
                        }

                            ArrayAdapter<String> lol1;
                            List<String>prodec = new ArrayList<>();
                            prodec.add(0,typeofdeduction);
                            prodec.add(1,"Select");
                            lol1 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec);
                            spinner_deduction1.setAdapter(lol1);
                            spinner_deduction1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    typeofdeductions = parent.getItemAtPosition(position).toString();
                                    if (position == 0) {
                                        if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                            ((TextView) parent.getChildAt(0)).setTextColor(R.color.black);
                                        }
                                    }
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                            ArrayAdapter<String> lol2;
                            List<String>prodec2 = new ArrayList<>();
                            prodec2.add(0,deductioncycles);
                            prodec2.add(1,"Select");
                            lol2 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec2);
                            spinner_deduction2.setAdapter(lol2);
                            spinner_deduction2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    deductioncycle = parent.getItemAtPosition(position).toString();
                                    if (position == 0) {
                                        if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                            ((TextView) parent.getChildAt(0)).setTextColor(R.color.black);
                                        }
                                    }
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                            ArrayAdapter<String> lol3;
                            List<String>prodec3= new ArrayList<>();
                            prodec3.add(0,deductionmethode);
                            prodec3.add(1,"Select");
                            lol3 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec3);
                            spinner_deduction3.setAdapter(lol3);
                            spinner_deduction3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    deductionmethod  = parent.getItemAtPosition(position).toString();
                                    if (position == 0) {
                                        if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                            ((TextView) parent.getChildAt(0)).setTextColor(R.color.black);
                                        }
                                    }
                                }
                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });

                    } while (mCursor2.moveToNext());
                    }

                else if(mCursor2.getCount() == 0){
                    ArrayAdapter<String> lol1;
                    List<String>prodec = new ArrayList<>();
                    prodec.add("Select");
                    lol1 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec);
                    spinner_deduction1.setAdapter(lol1);
                    spinner_deduction1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            typeofdeductions = parent.getItemAtPosition(position).toString();
                            if (position == 0) {
                                if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                                }
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    ArrayAdapter<String> lol2;
                    List<String>prodec2 = new ArrayList<>();
                    prodec2.add("Select");
                    lol2 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec2);
                    spinner_deduction2.setAdapter(lol2);
                    spinner_deduction2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String deductioncycle = parent.getItemAtPosition(position).toString();
                            if (position == 0) {
                                if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                                }
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                    ArrayAdapter<String> lol3;
                    List<String>prodec3= new ArrayList<>();
                    prodec3.add("Select");
                    lol3 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec3);
                    spinner_deduction3.setAdapter(lol3);
                    spinner_deduction3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @SuppressLint("ResourceAsColor")
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            String  deductionmethod  = parent.getItemAtPosition(position).toString();
                            if (position == 0) {
                                if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                    ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                                }
                            }
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void List4(){
        List<String> list4 = new ArrayList<String>();
        list4.add("Select");
        list4.add("1");
        list4.add("2");
        list4.add("3");
        list4.add("4");
        list4.add("5");
        list4.add("6");
        list4.add("7");
        list4.add("8");
        list4.add("9");
        list4.add("10");
        final ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list4);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_deduction4.setAdapter(dataAdapter4);
        spinner_deduction4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                billingcycle = parent.getItemAtPosition(position).toString();
                Log.d("Loggedspinner",billingcycle);
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }else{
                        ((TextView) view).setTextColor(R.color.bordercolor);
                    }

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private void ragi(final String idss,final String nameofdeductions,final String typeofdeductions, final String deductioncycle,final String deductionmethod,final String amounts,
                      final String billingcycle,final String remark,final String farmerNumber) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, deduction_save_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePost:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                VolleyLog.d("Error:" + error.toString());
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                if(typeofdeductions.equals("Regular")){
                    typeofdedu = "1";
                }else if(typeofdeductions.equals("Fix")){
                    typeofdedu = "2";
                }else if(typeofdeductions.equals("Other Deduction")){
                    typeofdedu = "3";
                }

                if(deductioncycle.equals("bill-wise")){
                    deducycle = "1";
                }else if(deductioncycle.equals("yearly")){
                    deducycle = "2";
                }

                if(deductionmethod.equals("liter")){
                    dedumenthod = "1";
                }else if(deductionmethod.equals("fix-amount")){
                    dedumenthod = "2" ;
                }

                map.put("deductioname",nameofdeductions);
                map.put("typeofdeduction",idss);
                map.put("deductioncycle", deducycle);
                map.put("deductionmethod", dedumenthod);
                map.put("amount",amounts);
                map.put("norepayment",billingcycle);
                map.put("remark",remark);
                map.put("farmernumber",farmerNocomplete.getText().toString());
                map.put("date",reversedate);
                map.put("farmername",farmerName.getText().toString());

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void openDiologue(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.layout_dialog,null);
        textViewtext1 = (EditText)view.findViewById(R.id.farmerno);
        textViewtext2 = (EditText) view.findViewById(R.id.farmername12);
        userList1 = (ListView)view.findViewById(R.id.listdialog);
        farmernocomplete = (EditText) view.findViewById(R.id.farmernocomplete);

        db = new DatabaseHelper(getActivity().getApplicationContext());
        db.getReadableDatabase();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.create();
        displayData();
        ListData();
        ClearText();
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(textViewtext2.getText().toString().length() >0) {
                    User u ;
                    u = new User();
                    farmerNo.setText(GetID);
                    farmerName.setText(textViewtext2.getText().toString());
                    farmerNocomplete.setText(farmernocomplete.getText().toString());

                }else{
                    ClearText();
                }
            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void openDiologue2(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.deduction_dialog,null);
        userList2 = (ListView)view.findViewById(R.id.listdeduction);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.create();
        ListDataDeduction();
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    public void ListDataDeduction() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.DEDU_REGI, null);

        ids.clear();
        nameofdeductionss.clear();
        typeofdeductionss.clear();
        deductionmethodess.clear();
        deductionamounts.clear();
        deductionbillingcycle.clear();
        deductionremark.clear();

        if (mCursor.moveToFirst()) {
            do {
                if(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMER_NUMBER)).equals(farmerNo.getText().toString()) &&
                        mCursor.getString(mCursor.getColumnIndex("status")).equals("Active"))
//                                mCursor.getColumnName(8).equals("NULL"))
                {
                    ids.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.IDs)));
                    nameofdeductionss.add(mCursor.getString(mCursor.getColumnIndex("nameofdeductions")));
                    typeofdeductionss.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DUDUCTION_TYPE)));
                    deductioncycless.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DEDUCTION_CYCLE)));
                    deductionmethodess.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DEDUCTION_Method)));
                    deductionbillingcycle.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DEDUCTION_BILLING_CYCLE)));
                    deductionamounts.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DEDUCTION_AMOUNT)));
                    deductionremark.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.DEDUCTION_REMARK)));

                }

            } while (mCursor.moveToNext());
        }

        DeductionAdapter disadpt = new DeductionAdapter(getActivity().getApplicationContext(), ids,nameofdeductionss, typeofdeductionss,
                deductioncycless,deductionmethodess,deductionamounts,deductionbillingcycle,deductionremark);
        userList2.setAdapter(disadpt);

        userList2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ViewGroup vg    =  (ViewGroup)view;

                TextView txt11   =  (TextView)vg.findViewById(R.id.dedu_name_1);
                String lols11    =  txt11.getText().toString();
                ArrayAdapter<String> lol11;
                List<String>prodec11 = new ArrayList<>();
                prodec11.add(lols11);
                prodec11.add("Select");
                lol11 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec11);
                spinner_d5.setAdapter(lol11);

                spinner_d5.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Renameofdeductions = parent.getItemAtPosition(position).toString();
                        Log.d("Loggedspinner",Renameofdeductions);
                        if (position == 0) {
                            if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                TextView txt1   =  (TextView)vg.findViewById(R.id.dedu_name);
                String lols1    =  txt1.getText().toString();
                ArrayAdapter<String> lol1;
                List<String>prodec = new ArrayList<>();
                prodec.add(lols1);
                prodec.add("Select");
                lol1 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec);
                spinner_d1.setAdapter(lol1);

                spinner_d1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Retypeofdeductions = parent.getItemAtPosition(position).toString();
                        Log.d("Loggedspinner",Retypeofdeductions);
                        if (position == 0) {
                            if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


                TextView txt2 = (TextView)vg.findViewById(R.id.dedu_cycle);
                String lols2 = txt2.getText().toString();

                ArrayAdapter<String> lol2;
                List<String>prodec2 = new ArrayList<>();
                prodec2.add(lols2);
                prodec2.add("Select");
                lol2 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec2);
                spinner_d2.setAdapter(lol2);

                spinner_d2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Redeductioncycle = parent.getItemAtPosition(position).toString();
                        Log.d("Loggedspinner",Redeductioncycle);
                        if (position == 0) {
                            if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


                TextView txt3 = (TextView)vg.findViewById(R.id.dedu_method);
                String lols3 = txt3.getText().toString();



                ArrayAdapter<String> lol3;
                List<String>prodec3 = new ArrayList<>();
                prodec3.add(lols3.toString());
                prodec3.add("Select");
                lol3 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec3);
                spinner_d3.setAdapter(lol3);

                spinner_d3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Redeductionmethod = parent.getItemAtPosition(position).toString();
                        Log.d("Loggedspinner",Redeductionmethod);
                        if (position == 0) {
                            if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


                TextView txt4 = (TextView)vg.findViewById(R.id.dedu_amount);
                totaledit.setText(txt4.getText().toString());

                TextView ids = (TextView)vg.findViewById(R.id.dedu_id);
                updateid = ids.getText().toString();

                TextView txt5 = (TextView)vg.findViewById(R.id.dedu_billing);
                String lols5 = txt5.getText().toString();

                ArrayAdapter<String> lol5;
                List<String>prodec5 = new ArrayList<>();
                prodec5.add(lols5.toString());
                prodec5.add("Select");
                lol5 = new ArrayAdapter<String>(getActivity().getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,prodec5);
                spinner_d4.setAdapter(lol5);

                spinner_d4.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Rebillingcycle = parent.getItemAtPosition(position).toString();
                        Log.d("Loggedspinner",Rebillingcycle);
                        if (position == 0) {
                            if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                                ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                            }
                        }
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                TextView txt6 = (TextView)vg.findViewById(R.id.dedu_remark);
                r8.setText(txt6.getText().toString());

                TextView txt7 = (TextView)vg.findViewById(R.id.dedu_id);
                Retransactionid = txt7.getText().toString();

                RedeductionRepayment = "Repayment Deduction";


                //....calculate blnce.....
                TextWatcher textWatcher2 = new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if(!totaledit.getText().toString().equals("") && !r6.getText().toString().equals("")) {
                            float total       =    Float.parseFloat(totaledit.getText().toString());
                            float amount      =    Float.parseFloat(r6.getText().toString());
                            double blnce    =    total - amount;
                            blncedit.setText(Double.toString(blnce));
                        }
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                    }
                };
                totaledit.addTextChangedListener(textWatcher2);
                r6.addTextChangedListener(textWatcher2);

            }
        });
        mCursor.close();
    }

    public void ClearTextDeduction() {
        spinner_deduction1.setSelection(1);
        spinner_deduction2.setSelection(1);
        amounttxt.setText("");
        spinner_deduction3.setSelection(1);
        spinner_deduction4.setSelection(0);
        spinner_deduction5.setSelection(0);
        remarkdeduction.setText("");
        remarkdeduction.setText("");
    }

    public void ClearTextDeduction2() {
        spinner_d1.setSelection(1);
        spinner_d2.setSelection(1);
        r6.setText("");
        totaledit.setText("");
        spinner_d3.setSelection(1);
        spinner_d4.setSelection(1);
        spinner_d5.setSelection(1);
        r8.setText("");
        blncedit.setText("");
    }

    private void displayData() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                GetID = textViewtext1.getText().toString();
                SQLiteDatabase dataBase = db.getWritableDatabase();
                User u;
                Cursor cursor = dataBase.rawQuery("select * from farmermaster where substr(farmer_number,-4) =?",
                        new String[]{String.valueOf(GetID)}, null);

                if (cursor !=null && cursor.getCount() > 0){
                    cursor.moveToFirst();

                    u = new User();
                    u.setCource(cursor.getString(cursor.getColumnIndex("farmername")));
                    String farmernocompletes = cursor.getString(cursor.getColumnIndex("farmer_number"));


                    textViewtext2.setText(u.getCource());
                    name= textViewtext2.getText().toString();
                    farmernocomplete.setText(farmernocompletes);


                    cursor.close();
                    db.close();

                }else {
//                    Toast.makeText(getActivity(), "farmer number invalid", Toast.LENGTH_SHORT).show();
                    cursor.close();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        textViewtext1.addTextChangedListener(textWatcher);

    }

    public void ClearText() {
        farmerNo.setText("");
        farmerName.setText("");

    }


    private void ListData() {
        SQLiteDatabase dataBase = db.getWritableDatabase();

        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);

        id.clear();
        firstname.clear();


        if (mCursor.moveToFirst()) {
            do {
                id.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNUMBER)));
                firstname.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNAME)));

            } while (mCursor.moveToNext());
        }


        FarmerSelectionAdapter disadpt = new FarmerSelectionAdapter(getActivity().getApplicationContext(), id, firstname);
        userList1.setAdapter(disadpt);
        userList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ViewGroup vg = (ViewGroup)view;
                TextView txt = (TextView)vg.findViewById(R.id.textviewdg1);

                String no = txt.getText().toString();
                String lastdigit = no.substring((no.length() -4));

                textViewtext1.setText(lastdigit);
                farmernocomplete.setText(no);
            }
        });
        mCursor.close();
    }




    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setupToolbar() {
        toolbar.setTitle("Deduction Transaction");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}



