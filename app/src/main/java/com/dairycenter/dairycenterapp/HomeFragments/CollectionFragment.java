package com.dairycenter.dairycenterapp.HomeFragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.Adapter.FarmerSelectionAdapter;
import com.dairycenter.dairycenterapp.BluetoothChatService;
import com.dairycenter.dairycenterapp.Bt;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.DeviceListActivity;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabBankDetailes3;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.User;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CollectionFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static TextView centerNo,date,mor_eve_shift,rate,amount,shift_wt_cow,
            shift_wt_buffalo,shift_fat_cow,shift_fat_buffalo,shift_snf_cow,shift_snf_buffalo,shift_sum_cow_buffalo,temp,farmerNo,farmreNoComplte;
    public static TextView weight1,fat1,lacto1,snf1,protien1,water1,weight_dev,fat_dev,lacto_dev,snf_dev,protien_dev,water_dev;
    public static TextView farmerName,total_wt_cow,total_wt_buffalo,aVg_cow_fat,avg_buffalo_fat,avg_cow_snf,avg_buffalo_snf;
    public static EditText textViewtext1,TextViewtext1Complete;
    public static EditText textViewtext2;
    DatabaseHelper db;

    String demo,yesterdaydemo,yesterdayshiftdate,shiftclose_dates,morningdemo;

    public static String GetID,name,yesterdayDate;
    public static ImageView ProfImg;
    public static Bitmap newbitmap,bitmap;
    public static byte[] datas;

    public static ArrayList<String> id = new ArrayList<String>();
    public static ArrayList<String> firstname= new ArrayList<String>();
    public static ArrayList<String> lastname= new ArrayList<String>();
    Button bluetoothsent;
    public static EditText weight,fat,lacto,snf,protien,water;
    public static ListView userList1;
    public static Integer res;
    public static String text,tra_id ,rate_id,rate_matrix_id,CLOSE_SHIFT,urlImage,urlImage2;

    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private EditText mOutEditText;
    public static Button mSendButton,connect,save_collection,close,print,close_popup,cancel;
    private String mConnectedDeviceName = null;
    private StringBuffer mOutStringBuffer;
    private BluetoothAdapter mBluetoothAdapter = null;
    private BluetoothChatService mChatService = null;
    public static RadioButton cow,buffalo;
    public static AlertDialog dialog1;
    public static String userCattle = "",dates,shift,farmer_no,weightss,fatsss,lactoo,snffss,protiens,waterss,ratesss,amountss,shift_small, Shistcenter_number;

    public static VideoView videoView;
    public static AlertDialog show;

    TextView shiftclose_date,centername;
    TextView shiftclose_time,cattletype;
    EditText vehicle_no,driver_name,contact_no;
    String range,charttype,cattlevalue;
    String shift_wtcow,shift_wtbuffalo,shift_fatcow,shift_fatbuffalo,shift_snfcow,shift_snfbuffalo,shift_date,shift_time,
            shift_vehicleno,shift_drivername,shift_contctno,shiftclose;
    RadioGroup radiogrp;
    Date mdate;
    String reversedate,litervalue,farmernumber,dateClose,dateshift,cattle,mergshiftcattle;

    public static final String COLLECTION_DATA = "http://mcsoft.whitegoldtech.com:8888/api/collectionform";
    public static final String CLS_SHIFT_API = "http://mcsoft.whitegoldtech.com:8888/api/shiftform";

    public static final String inputFormat = "HH:mm";

    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat,Locale.UK);

    public CollectionFragment() {
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_collection, container, false);
        toolbar        =     (Toolbar) view.findViewById(R.id.home_toolbar);
        farmerNo       =     (TextView) view.findViewById(R.id.farmerno);
        farmreNoComplte =     (TextView) view.findViewById(R.id.farmernocomplete);
        farmerName     =     (TextView) view.findViewById(R.id.farmername);
        ProfImg        =     (ImageView)view.findViewById(R.id.img_authorImage);
        date           =     (TextView)view.findViewById(R.id.date);
        mor_eve_shift  =     (TextView)view.findViewById(R.id.mor_eve);
        cow            =     (RadioButton) view.findViewById(R.id.cow);
        buffalo        =     (RadioButton) view.findViewById(R.id.buffalo);

        mSendButton    =     (Button) view.findViewById(R.id.button_send);
        weight         =     (EditText) view.findViewById(R.id.weight);
        fat            =     (EditText) view.findViewById(R.id.fat);
        lacto          =     (EditText) view.findViewById(R.id.lacto);
        snf            =     (EditText) view.findViewById(R.id.snf);
        protien        =     (EditText) view.findViewById(R.id.protein);
        water          =     (EditText) view.findViewById(R.id.water);
        rate           =     (TextView) view.findViewById(R.id.rate);
        amount         =     (TextView) view.findViewById(R.id.amount);
        save_collection=     (Button) view.findViewById(R.id.save);
        close          =     (Button) view.findViewById(R.id.cls);
        cancel         =     (Button) view.findViewById(R.id.cancel);

        weight1        =     (TextView) view.findViewById(R.id.weight1);
        fat1           =     (TextView) view.findViewById(R.id.fat1);
        lacto1         =     (TextView) view.findViewById(R.id.lacto1);
        snf1           =     (TextView) view.findViewById(R.id.snf1);
        protien1       =     (TextView) view.findViewById(R.id.protien1);
        water1         =     (TextView) view.findViewById(R.id.water1);

        weight_dev     =     (TextView) view.findViewById(R.id.weight_dev);
        fat_dev        =     (TextView)view.findViewById(R.id.fat_dev);
        lacto_dev      =     (TextView)view.findViewById(R.id.lacto_dev);
        snf_dev        =     (TextView)view.findViewById(R.id.snf_dev);
        protien_dev    =     (TextView)view.findViewById(R.id.protien_dev);
        water_dev      =     (TextView) view.findViewById(R.id.water_dev);

        total_wt_cow    =    (TextView) view.findViewById(R.id.total_wt_cow);
        total_wt_buffalo=    (TextView) view.findViewById(R.id.total_wt_buffalo);
        aVg_cow_fat     =    (TextView)view.findViewById(R.id.cow_fat);
        avg_buffalo_fat =    (TextView)view.findViewById(R.id.buffalo_fat);
        avg_buffalo_snf =    (TextView)view.findViewById(R.id.buffalo_snf);
        avg_cow_snf     =    (TextView)view.findViewById(R.id.cow_snf);
        temp            =    (TextView)view.findViewById(R.id.temp);
        cattletype      =    (TextView)view.findViewById(R.id.cattletype);
        centername      =    (TextView)view.findViewById(R.id.centername);

        radiogrp        =    (RadioGroup)view.findViewById(R.id.radiogrp);

        userCattle = "";
        yesterdayshiftdate = "";
        yesterdaydemo  = "";
        demo = "";
        morningdemo = "";
        cattlevalue = "";
        cattle = "";
        litervalue = "";
        tra_id = "";
        mergshiftcattle = "";

        weight.setFocusable(false);
        fat.setFocusable(false);
        lacto.setFocusable(false);
        snf.setFocusable(false);
        protien.setFocusable(false);
        water.setFocusable(false);

        //Set center name........//
        db = new DatabaseHelper(getActivity().getApplicationContext());
        SQLiteDatabase dataBase3 = db.getWritableDatabase();
        Cursor mCursor3 = dataBase3.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor3.moveToFirst()) {
            do {
               String center_name = mCursor3.getString(mCursor3.getColumnIndex(DatabaseHelper.cenetrname));
                Shistcenter_number = mCursor3.getString(mCursor3.getColumnIndex("center_number"));
               centername.setText(center_name);

            } while (mCursor3.moveToNext());
        }



//        Bt bt = new Bt();
//        bt.obj(getActivity());
//        bt.enable(getActivity());
//        bt.comm(getActivity());


//
//        final Handler handler = new Handler();
//        handler.post(new Runnable() {
//            @Override
//            public void run() {
//                handler.postDelayed(this, 1000);
//                String tmp;
//                tmp = bt.text;
//                weight.setText(tmp);
//                Log.d("dataaaaaaaaaaa ",tmp);
//            }
//        });

//        db = new DatabaseHelper(getActivity());
//        db.getWritableDatabase();
//        SQLiteDatabase dataBases = db.getWritableDatabase();
//        Cursor mCursors = dataBases.rawQuery("SELECT * FROM "
//                + DatabaseHelper.Bluetooth, null);
//        if (mCursors.moveToFirst()) {
//            do {
//                if(userstring.getText().toString().equals("Demo")) {
//
//                    String st = mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.text));
//
//                    Log.d("lll", String.valueOf(st));
//                }
//
//            } while (mCursors.moveToNext());
//        }


        //.....Calculate snf.....
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!lacto.getText().toString().equals("") && !fat.getText().toString().equals("")) {
                    float lac      =     Float.parseFloat(lacto.getText().toString());
                    float fats     =     Float.parseFloat(fat.getText().toString());

                    double snfCal  =  ((lac)/4 + ((0.21) * fats) +(0.36));
                    snf.setText(new DecimalFormat("##.#").format(snfCal));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        lacto.addTextChangedListener(textWatcher);
        fat.addTextChangedListener(textWatcher);


        //Calculate weight accourding littervalue.....//
        weight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if(litervalue.equals("1")) {
                        float weight1 = Float.parseFloat(weight.getText().toString());
                        Double weighntvalue = weight1 * (0.970);
                        String df = weighntvalue.toString();
                        weight.setText(df + " " + "ltr");
                    }
                    else if(litervalue.equals("0")) {
                        String df = weight.getText().toString();
                        weight.setText(df + " " + "kg");
                    }
                }
            }
        });


        //....calculate amount.....
        TextWatcher textWatcher2 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
               }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!rate.getText().toString().equals("") && !weight.getText().toString().equals("")) {
                    float weight1    =    Float.parseFloat(weight.getText().toString().replace("ltr","").replace("kg",""));
                    float rate1      =    Float.parseFloat(rate.getText().toString());
                    double amount1   =    weight1 * rate1;
                    amount.setText(new DecimalFormat("##.#").format(amount1));
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        rate.addTextChangedListener(textWatcher2);
        weight.addTextChangedListener(textWatcher2);

        connect   =   (Button) view.findViewById(R.id.scan);
        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent serverIntent = new Intent(getActivity().getApplicationContext(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
            }
        });


        mBluetoothAdapter   =   BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getActivity(), "Bluetooth is not available", Toast.LENGTH_LONG).show();
            appCompatActivity.finish();
        }


        Calendar calendar    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat.format(today);
        date.setText(currentdate);

        String strt = date.getText().toString();
        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mdate = formats.parse(strt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        reversedate = simpleDate.format(mdate);


        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",Locale.US);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        Log.d("hour", String.valueOf(hour));
        Date datess = null;
        try {
            datess = sdf.parse(hour+ ":" +minute);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("datess", String.valueOf(datess));


//set shift time....................
        db = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBased   =   db.getWritableDatabase();
        Cursor mCursord = dataBased.rawQuery("SELECT * FROM "
                + DatabaseHelper.shifttime, null);
        if (mCursord.moveToFirst()) {
            do {
                String  mor_starttime = mCursord.getString(mCursord.getColumnIndex("morningstarttime"));
                String  mor_endtime = mCursord.getString(mCursord.getColumnIndex("morningendtime"));
                String  eve_starttime = mCursord.getString(mCursord.getColumnIndex("eveningstarttime"));
                String  eve_endtime = mCursord.getString(mCursord.getColumnIndex("eveningendtime"));

                Date d1  = parseDate(mor_starttime);
                Date d2  = parseDate(mor_endtime);
                Date d3  = parseDate(eve_starttime);
                Date d4  = parseDate(eve_endtime);

                if(d1.before(datess) && d2.after(datess)){
                    mor_eve_shift.setText("Morning");
                    mor_eve_shift.setTextColor(getResources().getColor(R.color.subtitle));
                }
                if(d3.before(datess) && d4.after(datess)){
                    mor_eve_shift.setText("Evening");
                    mor_eve_shift.setTextColor(getResources().getColor(R.color.subtitle));
                }
            } while (mCursord.moveToNext());
        }
        //........................................


// set yesterday date........
        Calendar calendar1    =    Calendar.getInstance();
        calendar1.add(Calendar.DATE,-1);
        String yester   =    DateFormat.getDateInstance().format(calendar1.getTime());

        SimpleDateFormat formatss = new SimpleDateFormat("MMM dd,yyyy", Locale.getDefault());
        try {
            Date d2 = formatss.parse(yester);
            SimpleDateFormat simpleDatess = new SimpleDateFormat("dd/MM/yyyy");
            yesterdayDate = simpleDatess.format(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//.............................


        db    =   new DatabaseHelper(getActivity().getApplicationContext());
        setupToolbar();

        if(farmerNo.getText().toString().isEmpty()){
            cow.setClickable(false);
            buffalo.setClickable(false);
        }

        farmerNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mor_eve_shift.getText().toString().equals("Shift will be open soon") && total_wt_cow.getText().toString().equals("0")){
                    Toast.makeText(getActivity(), "Shift will be open soon", Toast.LENGTH_LONG).show();
                }else if(mor_eve_shift.getText().toString().equals("Shift will be open soon") && !total_wt_cow.getText().toString().equals("0") && !total_wt_cow.getText().toString().equals("0.00")) {
                    Toast.makeText(getActivity(), "Please close the previous shift", Toast.LENGTH_LONG).show();
                }else if(mor_eve_shift.getText().toString().equals("Shift will be open soon") && total_wt_cow.getText().toString().equals("0.00")){
                    Toast.makeText(getActivity(), "Shift will be open soon", Toast.LENGTH_LONG).show();
                }else if(mor_eve_shift.getText().toString().equals("Morning") && total_wt_cow.getText().toString().equals("0.00")){
                    Toast.makeText(getActivity(), "Shift is already closed", Toast.LENGTH_LONG).show();
                }else if(mor_eve_shift.getText().toString().equals("Evening") && total_wt_cow.getText().toString().equals("0.00")){
                    Toast.makeText(getActivity(), "Shift is already closed", Toast.LENGTH_LONG).show();
                }else if(yesterdaydemo.equals("yesterdaydemo")){
                    Toast.makeText(getActivity(), "Please close the yesterday evening shift", Toast.LENGTH_LONG).show();
                }else if(mor_eve_shift.getText().toString().equals("Evening") && morningdemo.equals("Morning")){
                    Toast.makeText(getActivity(), "Please close the previous morning shift", Toast.LENGTH_LONG).show();
                }
                else{
                    radiogrp.clearCheck();
                    openDiologue();

                    CalculatePreviousShift();
                    calculatePreviousBuffalo();
                }
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(total_wt_cow.getText().toString().equals("0") && total_wt_buffalo.getText().toString().equals("0")
                        && aVg_cow_fat.getText().toString().equals("0") &&
                        avg_buffalo_fat.getText().toString().equals("0") && avg_cow_snf.getText().toString().equals("0")
                        && avg_cow_snf.getText().toString().equals("0")) {
                    Toast.makeText(getActivity(), "Collection summary not updated", Toast.LENGTH_LONG).show();
                }else if(total_wt_cow.getText().toString().equals("000.0") && total_wt_buffalo.getText().toString().equals("000.0")
                        && aVg_cow_fat.getText().toString().equals("0.00") &&
                        avg_buffalo_fat.getText().toString().equals("0.00") && avg_cow_snf.getText().toString().equals("0.00")
                        && avg_cow_snf.getText().toString().equals("0.00")) {
                    Toast.makeText(getActivity(), "Collection summary not updated", Toast.LENGTH_LONG).show();
                }else if(total_wt_cow.getText().toString().equals("0.00") && total_wt_buffalo.getText().toString().equals("0.00")
                        && aVg_cow_fat.getText().toString().equals("0.00") &&
                        avg_buffalo_fat.getText().toString().equals("0.00") && avg_cow_snf.getText().toString().equals("0.00")
                        && avg_cow_snf.getText().toString().equals("0.00")){
                    Toast.makeText(getActivity(), "Shift is closed.", Toast.LENGTH_LONG).show();
                    Log.d("collection",total_wt_cow.getText().toString());
                } else {
                    openNotificationMessage();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weight.setText("");
                fat.setText("");
                lacto.setText("");
                snf.setText("");
                protien.setText("");
                water.setText("");
                rate.setText("0.00");
                amount.setText("0.00");
                weight_dev.setText("0.00");
                fat_dev.setText("0.00");
                lacto_dev.setText("0.00");
                snf_dev.setText("0.00");
                protien_dev.setText("0.00");
                water_dev.setText("0.00");

                weight1.setText("0.00");
                fat1.setText("0.00");
                lacto1.setText("0.00");
                snf1.setText("0.00");
                protien1.setText("0.00");
                water1.setText("0.00");

                farmerName.setText("");
                farmerNo.setText("");
                cow.setChecked(false);
                buffalo.setChecked(false);
                ProfImg.setImageResource(0);
            }
        });


        //.....Save collection Page details into database....
        weight.setOnEditorActionListener(mWriteListener);
        save_collection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dates       =    date.getText().toString();
                shift       =    mor_eve_shift.getText().toString();
                farmer_no   =    farmerNo.getText().toString();
                String   farmer_name   =    farmerName.getText().toString();
                weightss    =    weight.getText().toString().replace("ltr","").replace("kg","");
                fatsss      =    fat.getText().toString();
                lactoo      =    lacto.getText().toString();
                snffss      =    snf.getText().toString();
                protiens    =    protien.getText().toString();
                waterss     =    water.getText().toString();
                ratesss     =    rate.getText().toString();
                amountss    =    amount.getText().toString();
                reversedate.toString();


                if(total_wt_cow.getText().toString().equals("0.00") && total_wt_buffalo.getText().toString().equals("0.00")
                        && aVg_cow_fat.getText().toString().equals("0.00") &&
                        avg_buffalo_fat.getText().toString().equals("0.00") && avg_cow_snf.getText().toString().equals("0.00")
                        && avg_cow_snf.getText().toString().equals("0.00")){
                    Toast.makeText(getActivity(), "Shift is already closed.", Toast.LENGTH_LONG).show();
                }
                else {
                    if (weightss.trim().isEmpty()) {
                        weight.setError("");
                        weight.requestFocus();
                    } else if (fatsss.trim().isEmpty()) {
                        fat.setError("");
                        fat.requestFocus();
                    } else if (lactoo.trim().isEmpty()) {
                        lacto.setError("");
                        lacto.requestFocus();
                    } else if (snffss.trim().isEmpty()) {
                        snf.setError("");
                        snf.requestFocus();
                    } else if (protiens.trim().isEmpty()) {
                        protien.setError("");
                        protien.requestFocus();
                    } else if (waterss.trim().isEmpty()) {
                        water.setError("");
                        water.requestFocus();
                    }else if(farmer_no.trim().isEmpty()){
                        Toast.makeText(getActivity().getApplicationContext(), "Please select farmer", Toast.LENGTH_LONG).show();
                    }
                    else if(userCattle.equals("")){
                        Toast.makeText(getActivity().getApplicationContext(), "Please select cattle type", Toast.LENGTH_LONG).show();
                    }
                    else {
                        String localstring  = "Morning/Evening";
                        String cattlebothstring = "Both";

                        if(shift.equals("Morning")){
                            shift_small = "morning";
                        }else if(shift.equals("Evening")){
                            shift_small = "evening";
                        }

                        if(userCattle.equals("Buffalo")){
                            userCattle = "buffelo";
                        }

                        String date_shift = dates+shift+userCattle;

                        db.AddCollectionPageData(null, userCattle, dates, shift, farmer_no,farmer_name, weightss, fatsss, lactoo, snffss, protiens, waterss, ratesss, amountss,localstring,
                                reversedate,date_shift,cattlebothstring);

                        ragi(reversedate, userCattle,shift_small,farmer_no,farmer_name, weightss, fatsss, lactoo, snffss, protiens, waterss, ratesss, amountss);

                        String message = ("Date:"+dates+", "+"Shift:"+shift+", "+"Farmer No:"+farmer_no+", "+"Farmer Name:"+farmer_name+", "+"Cattle Type:"+userCattle+","+
                                "Weight:"+weightss+", "
                                +"Fat:"+fatsss+", "+"Lacto:"+lactoo+", "+"Snf:"+snffss+", "+"Protein"+protiens+", "+
                                "Water:"+waterss+", "+"Rate:"+ratesss+", "+"Amount:"+amountss+", "+"Rate:"+ratesss);

                        sendMessage(message);
                        Toast.makeText(getActivity().getApplicationContext(), "Save successfully", Toast.LENGTH_LONG).show();

                        Log.d("user", userCattle.toString());
                        calculateTotalCowWT();
                        calculateTotalBuffaloWT();
                        CalculateAvgFatCow();
                        CalculateAvgFatBuffalo();
                        CalculateAvgSnfCow();
                        CalculateAvgSnfBuffalo();
                        ClearCollectionData();
                        weight.requestFocus();
                    }
                }
            }
        });

        mChatService = new BluetoothChatService(getActivity(), mHandler);
        mOutStringBuffer = new StringBuffer("");



        if(mor_eve_shift.getText().toString().equals("Morning")){
            shift_small = "morning";
        }else if(mor_eve_shift.getText().toString().equals("Evening")){
            shift_small = "evening";
        }
        db   = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBaseClose  = db.getWritableDatabase();
        final Cursor mCursorclose   =  dataBaseClose.rawQuery("SELECT * FROM "
                + DatabaseHelper.closeShiftData, null);

        if (mCursorclose.moveToFirst()) {
            do {
                if(mCursorclose.getString(mCursorclose.getColumnIndex("dateshift")).equals(date.getText().toString() + shift_small)){
                    total_wt_cow.setText("0.00");
                    total_wt_buffalo.setText("0.00");
                    aVg_cow_fat.setText("0.00");
                    avg_buffalo_fat.setText("0.00");
                    avg_cow_snf.setText("0.00");
                    avg_buffalo_snf.setText("0.00");

                    Log.d("foooo","foooo");
                }
                else{
                    CalculateDeviation();
                    calculateTotalCowWT();
                    calculateTotalBuffaloWT();
                    CalculateAvgFatCow();
                    CalculateAvgFatBuffalo();
                    CalculateAvgSnfCow();
                    CalculateAvgSnfBuffalo();
                    Log.d("11","11");
                }
            } while (mCursorclose.moveToNext());
        }else{
            if(mCursorclose.getCount() == 0){
                CalculateDeviation();
                calculateTotalCowWT();
                calculateTotalBuffaloWT();
                CalculateAvgFatCow();
                CalculateAvgFatBuffalo();
                CalculateAvgSnfCow();
                CalculateAvgSnfBuffalo();
                Log.d("12","12");
            }
        }



        return view;
    }

    private Date parseDate(String Date){
        try{
            return inputParser.parse(Date);
        }catch(ParseException e ){
            return new Date(0);
        }
    }

    public void fatbasertatecalculate() {
        //rate calculated by fat_base.........
        TextWatcher textWatcherw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }@Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                db = new DatabaseHelper(getActivity().getApplicationContext());
                final SQLiteDatabase dataBase1 = db.getWritableDatabase();

                range = fat.getText().toString();
                Cursor mCursorc = dataBase1.rawQuery("select * from ratechartmatrix where fat_range = ?",
                        new String[]{String.valueOf(range)}, null);

                rate.setText("0.00");
                if (mCursorc.moveToFirst()) {
                    do {
                        String total_wt = mCursorc.getString(mCursorc.getColumnIndex("rate_value"));
                        Log.d("mmm",total_wt);
                        int str = fat.getText().toString().length();
                        if (str == 3 || str == 1) {
                            rate.setText(total_wt);
                        }
                    } while (mCursorc.moveToNext());
                } else {
                    if (mCursorc.getCount() == 0) {
                        int str = fat.getText().toString().length();
                        if (str == 4) {
                            rate.setText("0.00");
                            Toast.makeText(getActivity(), "Rate not available", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        fat.addTextChangedListener(textWatcherw);
    }

    public void fatssnfbasertatecalculate(){
        //rate calculated by fat_snf_base.........
        TextWatcher textWatcherw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                db = new DatabaseHelper(getActivity().getApplicationContext());
                final SQLiteDatabase dataBase1 = db.getWritableDatabase();

                range = fat.getText().toString();
                Cursor mCursorc = dataBase1.rawQuery("select * from ratechartmatrix where fat_range = ? AND  snf_lacto_range = ?",
                        new String[]{String.valueOf(range), snf.getText().toString()} , null);

                rate.setText("0.00");
                if (mCursorc.moveToFirst()) {
                    do {
                        String total_wt = mCursorc.getString(mCursorc.getColumnIndex("rate_value"));
                        int str = fat.getText().toString().length();
                        int str2 = snf.getText().toString().length();
                        if (str == 3 && str2 == 3) {
                            rate.setText(total_wt);
                        }

                    } while (mCursorc.moveToNext());
                } else {
                    if (mCursorc.getCount() == 0) {
                        int str = fat.getText().toString().length();
                        int str2 = snf.getText().toString().length();
                        if (str == 4 && str2 == 3) {
                            rate.setText("0.00");
                            Toast.makeText(getActivity(), "Rate not available", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        fat.addTextChangedListener(textWatcherw);
        snf.addTextChangedListener(textWatcherw);
    }

    public void ClearCollectionData(){
        weight.setText("");
        fat.setText("");
        lacto.setText("");
        snf.setText("");
        protien.setText("");
        water.setText("");
        rate.setText("0.00");
        amount.setText("0.00");

        weight_dev.setText("0.00");
        fat_dev.setText("0.00");
        lacto_dev.setText("0.00");
        snf_dev.setText("0.00");
        protien_dev.setText("0.00");
        water_dev.setText("0.00");

        weight1.setText("0.00");
        fat1.setText("0.00");
        lacto1.setText("0.00");
        snf1.setText("0.00");
        protien1.setText("0.00");
        water1.setText("0.00");


        farmerName.setText("");
        farmerNo.setText("");
        cow.setChecked(false);
        buffalo.setChecked(false);
        ProfImg.setImageResource(0);

        weight.setFocusable(false);
        fat.setFocusable(false);
        lacto.setFocusable(false);
        snf.setFocusable(false);
        protien.setFocusable(false);
        water.setFocusable(false);
    }

    public void CalculatePreviousShift() {
        cow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    userCattle = "cow";

                    if(!farmerNo.getText().toString().equals("")) {
                        weight.setFocusableInTouchMode(true);
                        fat.setFocusableInTouchMode(true);
                        lacto.setFocusableInTouchMode(true);
                        snf.setFocusableInTouchMode(true);
                        protien.setFocusableInTouchMode(true);
                        water.setFocusableInTouchMode(true);
                    }

                    db = new DatabaseHelper(getActivity().getApplicationContext());
                    final SQLiteDatabase dataBaseClose = db.getWritableDatabase();
                    final Cursor mCursorclose = dataBaseClose.rawQuery("SELECT * FROM "
                            + DatabaseHelper.COLLECTIONMODE, null);
                    if (mCursorclose.getCount() == 0) {
                        do {
                            openVideoview();
                            flashmsg();

//                   send buetooth data......
//
//                    Bt bt = new Bt();
//                    String weight = "~BUTN";
//                    bt.out(weight);
//                    Log.d("Logged",bt.toString());
                        } while (mCursorclose.moveToNext());
                    }else{
                        if (mCursorclose.moveToFirst()) {
                            do {
                                if (mCursorclose.getString(mCursorclose.getColumnIndex("st1")).equals("Auto") &&
                                        mCursorclose.getString(mCursorclose.getColumnIndex("st2")).equals("Auto")) {
                                    openVideoview();
                                    flashmsg();
                                }
                            } while (mCursorclose.moveToNext());
                        }
                    }

                    db         = new DatabaseHelper(getActivity().getApplicationContext());
                    final SQLiteDatabase dataBase1 = db.getWritableDatabase();
                    Cursor mCursors = dataBase1.rawQuery("SELECT * FROM addcollectionpagedata " +
                            "where userCattle = ? AND dates = ? AND  shift = ? AND farmer_no = ? " , new String[]{userCattle,
                            yesterdayDate,mor_eve_shift.getText().toString(),farmerNo.getText().toString()}, null);
                    if (mCursors.moveToFirst()) {
                        do {
                            Log.d("qqqqqqq",cow.getText().toString());
                                String wei    =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.weight));
                                String fat    =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.fat));
                                String lact   =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.lacto));
                                String snf    =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.snf));
                                String pro    =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.protien));
                                String wat    =   mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.water));

                                weight1.setText(wei);
                                fat1.setText(fat);
                                lacto1.setText(lact);
                                snf1.setText(snf);
                                protien1.setText(pro);
                                water1.setText(wat);

                        } while (mCursors.moveToNext());
                    }else if(mCursors.getCount() == 0){
                        weight1.setText("0.00");
                        fat1.setText("0.00");
                        lacto1.setText("0.00");
                        snf1.setText("0.00");
                        protien1.setText("0.00");
                        water1.setText("0.00");
                    }
                }
            }
        });
    }

    public void calculatePreviousBuffalo() {
        buffalo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    userCattle   = "buffelo";

                    if(!farmerNo.getText().toString().equals("")) {
                        weight.setFocusableInTouchMode(true);
                        fat.setFocusableInTouchMode(true);
                        lacto.setFocusableInTouchMode(true);
                        snf.setFocusableInTouchMode(true);
                        protien.setFocusableInTouchMode(true);
                        water.setFocusableInTouchMode(true);
                    }

                    db = new DatabaseHelper(getActivity().getApplicationContext());
                    final SQLiteDatabase dataBaseClose = db.getWritableDatabase();
                    final Cursor mCursorclose = dataBaseClose.rawQuery("SELECT * FROM "
                            + DatabaseHelper.COLLECTIONMODE, null);
                    if (mCursorclose.getCount() == 0) {
                        do {
                            openVideoview();
                            flashmsg();
                        } while (mCursorclose.moveToNext());
                    }else{
                        if (mCursorclose.moveToFirst()) {
                            do {
                                if (mCursorclose.getString(mCursorclose.getColumnIndex("st1")).equals("Auto") &&
                                        mCursorclose.getString(mCursorclose.getColumnIndex("st2")).equals("Auto")) {
                                    openVideoview();
                                    flashmsg();
                                }
                            } while (mCursorclose.moveToNext());
                        }
                    }

                    db           = new DatabaseHelper(getActivity().getApplicationContext());
                    final SQLiteDatabase dataBase1 = db.getWritableDatabase();
                    Cursor mCursors = dataBase1.rawQuery("SELECT * FROM addcollectionpagedata " +
                            "where userCattle = ? AND dates = ? AND  shift = ? AND farmer_no = ? " , new String[]{userCattle,
                            yesterdayDate,mor_eve_shift.getText().toString(),farmerNo.getText().toString()}, null);
                    if (mCursors.moveToFirst()) {
                        do {
                            Log.d("mmmmmmm",buffalo.getText().toString());
                                String wei1    =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.weight));
                                String fat11   =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.fat));
                                String lact1   =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.lacto));
                                String snf11   =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.snf));
                                String pro1    =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.protien));
                                String wat1    =    mCursors.getString(mCursors.getColumnIndex(DatabaseHelper.water));

                                weight1.setText(wei1);
                                fat1.setText(fat11);
                                lacto1.setText(lact1);
                                snf1.setText(snf11);
                                protien1.setText(pro1);
                                water1.setText(wat1);
                        } while (mCursors.moveToNext());
                    }else if(mCursors.getCount() == 0){
                        weight1.setText("0.00");
                        fat1.setText("0.00");
                        lacto1.setText("0.00");
                        snf1.setText("0.00");
                        protien1.setText("0.00");
                        water1.setText("0.00");
                    }
                }
            }
        });
    }



    private void ragi(final String reversedate, final String userCattle,final String shift_small,final String farmer_no,
                      final String farmer_name,final String weightss,final String fatsss,final String lactoo,
                      final String snffss,final String protiens,final String waterss,final String ratesss,
                      final String amountss) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, COLLECTION_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePost:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                VolleyLog.d("Error:" + error.toString());
                String  farmreNoCompltes = farmreNoComplte.getText().toString();
                db.addsychronizedata(null,reversedate, userCattle, shift_small,farmer_name, weightss, fatsss, lactoo, snffss,
                        protiens, waterss, ratesss, amountss,farmreNoCompltes);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("date",reversedate );
                map.put("cattletype", userCattle);
                map.put("shift", shift_small);
                map.put("farmernumber",farmreNoComplte.getText().toString());
                map.put("farmername",farmer_name);
                map.put("weight",weightss);
                map.put("fat",fatsss);
                map.put("lacto",lactoo);
                map.put("snf",snffss);
                map.put("protein",protiens);
                map.put("water",waterss);
                map.put("rate",ratesss);
                map.put("amount",amountss);
                map.put("center_number",Shistcenter_number);

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }


    public void CalculateDeviation() {
        TextWatcher textWatcher4 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!weight.getText().toString().equals("") && !weight1.getText().toString().equals("")) {
                    float wei      =    Float.parseFloat(weight.getText().toString().replace("ltr","").replace("kg",""));
                    float wei1     =    Float.parseFloat(weight1.getText().toString());
                    double wei_dev =    wei - wei1;

                    weight_dev.setText(new DecimalFormat("##.#").format(wei_dev));
                    if(wei_dev < 0){
                        weight_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(wei_dev == 0){
                        weight_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(wei_dev > 0){
                        weight_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }
                if (!fat.getText().toString().equals("") && !fat1.getText().toString().equals("")) {
                    float fa      =    Float.parseFloat(fat.getText().toString());
                    float fa1     =    Float.parseFloat(fat1.getText().toString());
                    double fa_dev = fa - fa1;

                    fat_dev.setText(new DecimalFormat("##.#").format(fa_dev));
                    if(fa_dev < 0){
                        fat_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(fa_dev == 0){
                        fat_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(fa_dev > 0){
                        fat_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }
                if (!lacto.getText().toString().equals("") && !lacto1.getText().toString().equals("")) {
                    float lac      = Float.parseFloat(lacto.getText().toString());
                    float lac1     = Float.parseFloat(lacto1.getText().toString());
                    double lac_dev = lac - lac1;

                    lacto_dev.setText(new DecimalFormat("##.#").format(lac_dev));
                    if(lac_dev < 0){
                        lacto_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(lac_dev == 0){
                        lacto_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(lac_dev > 0){
                        lacto_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }
                if (!snf.getText().toString().equals("") && !snf1.getText().toString().equals("")) {
                    float sn       =    Float.parseFloat(snf.getText().toString());
                    float sn1      =    Float.parseFloat(snf1.getText().toString());
                    double sn_dev  =    sn - sn1;

                    snf_dev.setText(new DecimalFormat("##.#").format(sn_dev));
                    if(sn_dev < 0){
                        snf_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(sn_dev == 0){
                        snf_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(sn_dev > 0){
                        snf_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }
                if (!protien.getText().toString().equals("") && !protien1.getText().toString().equals("")) {
                    float pro      =   Float.parseFloat(protien.getText().toString());
                    float pro1     =   Float.parseFloat(protien1.getText().toString());
                    double pro_dev = pro - pro1;

                    protien_dev.setText(new DecimalFormat("##.#").format(pro_dev));
                    if(pro_dev < 0){
                        protien_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(pro_dev == 0){
                        protien_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(pro_dev > 0){
                        protien_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }

                if (!water.getText().toString().equals("") && !water1.getText().toString().equals("")) {
                    float wat      =    Float.parseFloat(water.getText().toString());
                    float wat1     =    Float.parseFloat(water1.getText().toString());
                    double wat_dev = wat - wat1;

                    water_dev.setText(new DecimalFormat("##.#").format(wat_dev));
                    if(wat_dev < 0){
                        water_dev.setTextColor(getResources().getColor(R.color.red));
                    }else if(wat_dev == 0){
                        water_dev.setTextColor(getResources().getColor(R.color.colorPrimary1));
                    }else if(wat_dev > 0){
                        water_dev.setTextColor(getResources().getColor(R.color.deviation));
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        weight.addTextChangedListener(textWatcher4);
        weight1.addTextChangedListener(textWatcher4);
        fat.addTextChangedListener(textWatcher4);
        fat1.addTextChangedListener(textWatcher4);
        lacto.addTextChangedListener(textWatcher4);
        lacto1.addTextChangedListener(textWatcher4);
        snf.addTextChangedListener(textWatcher4);
        snf1.addTextChangedListener(textWatcher4);
        protien.addTextChangedListener(textWatcher4);
        protien1.addTextChangedListener(textWatcher4);
        water.addTextChangedListener(textWatcher4);
        water1.addTextChangedListener(textWatcher4);
    }

    public void calculateTotalCowWT() {
        db = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase1 = db.getWritableDatabase();

        Cursor mCursor = dataBase1.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase1.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt == 0) {

                    } else {
                        total_wt_cow.setText(Integer.toString(total_wt));
                        Log.d("dip","dip");
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase1.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase1.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase1.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt == 0) {

                    } else {
                        total_wt_cow.setText(Integer.toString(total_wt));
                        Log.d("gay","gay");
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase1.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    Log.d("demo",demo);
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase1.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase1.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt == 0) {

                    } else {
                        total_wt_cow.setText(Integer.toString(total_wt));
                        Log.d("yak","yak");
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase1.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }
    }

    public void calculateTotalBuffaloWT(){
        db   = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase2   = db.getWritableDatabase();

        Cursor mCursor = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt1 = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt1 == 0) {

                    } else {
                        total_wt_buffalo.setText(Integer.toString(total_wt1));
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt1 = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt1 == 0) {

                    } else {
                        total_wt_buffalo.setText(Integer.toString(total_wt1));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT sum(weightss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    int total_wt1 = mCursorc.getInt(mCursorc.getColumnIndex("Total"));
                    if (total_wt1 == 0) {

                    } else {
                        total_wt_buffalo.setText(Integer.toString(total_wt1));

                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }

    }

    public void CalculateAvgFatCow(){
        db   =   new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase2 = db.getWritableDatabase();

        Cursor mCursor = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata  " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatCow = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatCow == 0) {

                    } else {
                        aVg_cow_fat.setText(new DecimalFormat("##.#").format(avg_fatCow));
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatCow = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatCow == 0) {

                    } else {
                        aVg_cow_fat.setText(new DecimalFormat("##.#").format(avg_fatCow));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatCow  = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatCow == 0) {

                    } else {
                        aVg_cow_fat.setText(new DecimalFormat("##.#").format(avg_fatCow));

                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }
    }

    public void CalculateAvgFatBuffalo(){
        db   =   new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase2 = db.getWritableDatabase();

        Cursor mCursor = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata  " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatBuffalo = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatBuffalo == 0) {

                    } else {
                        avg_buffalo_fat.setText(new DecimalFormat("##.#").format(avg_fatBuffalo));
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatBuffalo = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatBuffalo == 0) {

                    } else {
                        avg_buffalo_fat.setText(new DecimalFormat("##.#").format(avg_fatBuffalo));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(fatsss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_fatBuffalo  = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_fatBuffalo == 0) {

                    } else {
                        avg_buffalo_fat.setText(new DecimalFormat("##.#").format(avg_fatBuffalo));

                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }
    }

    public void CalculateAvgSnfCow(){
        db = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase2 = db.getWritableDatabase();

        Cursor mCursor = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata  " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfCow = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfCow == 0) {

                    } else {
                        avg_cow_snf.setText(new DecimalFormat("##.#").format(avg_snfCow));
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfCow = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfCow == 0) {

                    } else {
                        avg_cow_snf.setText(new DecimalFormat("##.#").format(avg_snfCow));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfCow  = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfCow == 0) {

                    } else {
                        avg_cow_snf.setText(new DecimalFormat("##.#").format(avg_snfCow));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "cow"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }
    }

    public void CalculateAvgSnfBuffalo(){
        db = new DatabaseHelper(getActivity().getApplicationContext());
        final SQLiteDatabase dataBase2 = db.getWritableDatabase();

        Cursor mCursor = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{yesterdayDate.toString(),
                "evening"}, null);
        if (mCursor.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata  " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfBuffalo = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfBuffalo == 0) {

                    } else {
                        avg_buffalo_snf.setText(new DecimalFormat("##.#").format(avg_snfBuffalo));
                        yesterdaydemo = "yesterdaydemo";
                    }
                } while (mCursorc.moveToNext());
            }

            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    yesterdayDate + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    yesterdayshiftdate = yesterdayDate.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "morning"}, null);
        if (mCursor2.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfBuffalo = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfBuffalo == 0) {

                    } else {
                        avg_buffalo_snf.setText(new DecimalFormat("##.#").format(avg_snfBuffalo));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Morning" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                    morningdemo = demo.toString();
                } while (mCursorr.moveToNext());
            }
        }

        Cursor mCursor3 = dataBase2.rawQuery("SELECT * FROM closeshift " +
                "where dateClose = ? AND  shiftclose = ?", new String[]{date.getText().toString(),
                "evening"}, null);
        if (mCursor3.getCount() == 0) {
            Cursor mCursorc = dataBase2.rawQuery("SELECT avg(snffss) As Total FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorc.moveToFirst()) {
                do {
                    Float avg_snfBuffalo  = mCursorc.getFloat(mCursorc.getColumnIndex("Total"));
                    if (avg_snfBuffalo == 0) {

                    } else {
                        avg_buffalo_snf.setText(new DecimalFormat("##.#").format(avg_snfBuffalo));
                    }
                } while (mCursorc.moveToNext());
            }
            Cursor mCursorr = dataBase2.rawQuery("SELECT * FROM addcollectionpagedata " +
                    "where date_shift = ?", new String[]{
                    date.getText().toString() + "Evening" + "buffelo"}, null);
            if (mCursorr.moveToFirst()) {
                do {
                    String shift = mCursorr.getString(mCursorr.getColumnIndex("shift"));
                    demo = shift.toString();
                } while (mCursorr.moveToNext());
            }
        }
    }

    private void openNotificationMessage(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("If this is the shift close...");
        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                openSecondpopUp();

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    @SuppressLint("SetTextI18n")
    public void openSecondpopUp(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.notification_window,null);

        db = new DatabaseHelper(getActivity().getApplicationContext());
        db.getReadableDatabase();

        close_popup           =   (Button)view.findViewById(R.id.close_popup);
        print                 =   (Button)view.findViewById(R.id.print);
        shift_wt_cow          =   (TextView)view.findViewById(R.id.shift_wt_cow);
        shift_wt_buffalo      =   (TextView)view.findViewById(R.id.shift_wt_buffalo);
        shift_fat_cow         =   (TextView)view.findViewById(R.id.shift_cow_fat);
        shift_fat_buffalo     =   (TextView)view.findViewById(R.id.shift_buffalo_fat);
        shift_snf_cow         =   (TextView)view.findViewById(R.id.shift_cow_snf);
        shift_snf_buffalo     =   (TextView)view.findViewById(R.id.shift_buffalo_snf);
        shift_sum_cow_buffalo =   (TextView)view.findViewById(R.id.cow_buffalo_sum);
        shiftclose_date       =   (TextView)view.findViewById(R.id.date);
        shiftclose_time       =   (TextView)view.findViewById(R.id.time);
        vehicle_no            =   (EditText) view.findViewById(R.id.vehi_no);
        driver_name           =   (EditText) view.findViewById(R.id.driver_name);
        contact_no            =   (EditText) view.findViewById(R.id.driver_contno);


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false);
        builder.setView(view);
        dialog1 = builder.create();
        displayShiftReport();

        String edi          =     total_wt_cow.getText().toString();
        String edi2         =     total_wt_buffalo.getText().toString();
        res                 =     Integer.parseInt(edi) + Integer.parseInt(edi2);
        shift_sum_cow_buffalo.setText(res.toString());


        Calendar calendar    =    Calendar.getInstance();
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat1.format(today);
        shiftclose_date.setText(currentdate);

        String strt = date.getText().toString();
        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mdate = formats.parse(strt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        reversedate = simpleDate.format(mdate);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        shiftclose_time.setText(""+simpleDateFormat.format(calendar.getTime()));


        db = new DatabaseHelper(getActivity().getApplicationContext());
        SQLiteDatabase dataBase3 = db.getWritableDatabase();
        Cursor mCursor3 = dataBase3.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor3.moveToFirst()) {
            do {
                Shistcenter_number = mCursor3.getString(mCursor3.getColumnIndex("center_number"));
            } while (mCursor3.moveToNext());
        }


        close_popup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(vehicle_no.getText().toString().trim().isEmpty()) {
                        vehicle_no.setError("Please enter vehicle no");
                        vehicle_no.requestFocus();
                    }else if (driver_name.getText().toString().trim().isEmpty()) {
                        driver_name.setError("Please enter driver name");
                        driver_name.requestFocus();
                    }else if(contact_no.getText().toString().trim().isEmpty()){
                        contact_no.setError("Please enter contact no");
                        contact_no.requestFocus();
                    }else if (contact_no.getText().toString().trim().length() < 10) {
                        contact_no.setError("Enter 10 digit contact number");
                        contact_no.requestFocus();
                    }else if (contact_no.getText().toString().trim().length() > 10) {
                        contact_no.setError("Enter 10 digit contact number");
                        contact_no.requestFocus();
                    }else {
                    dialog1.dismiss();
                    CLOSE_SHIFT = "closeshift";

                    total_wt_cow.setText("0.00");
                    total_wt_buffalo.setText("0.00");
                    aVg_cow_fat.setText("0.00");
                    avg_buffalo_fat.setText("0.00");
                    avg_cow_snf.setText("0.00");
                    avg_buffalo_snf.setText("0.00");

                    if(demo.equals("Morning")){
                       shiftclose = "morning";
                    }else if(demo.equals("Evening")){
                       shiftclose = "evening";
                   }else if(demo.equals("")){
                       if(mor_eve_shift.getText().toString().equals("Morning")){
                       shiftclose = "morning";
                        }else if(mor_eve_shift.getText().toString().equals("Evening")){
                       shiftclose = "evening";
                      }
                   }

                   Shistcenter_number.toString();


                    String shift_wt_cows = shift_wt_cow.getText().toString();
                    String shift_wt_buffalos = shift_wt_buffalo.getText().toString();
                    String shift_fat_cows = shift_fat_cow.getText().toString();
                    String shift_fat_buffalos = shift_fat_buffalo.getText().toString();
                    String shift_snf_cows = shift_snf_cow.getText().toString();
                    String shift_snf_buffalos = shift_snf_buffalo.getText().toString();
                    String shift_sum_cow_buffalos = shift_sum_cow_buffalo.getText().toString();
                    String shiftclose_times = shiftclose_time.getText().toString();
                    String vehicle_nos = vehicle_no.getText().toString();
                    String driver_names = driver_name.getText().toString();
                    String contact_nos = contact_no.getText().toString();


                    if(yesterdayshiftdate.toString().equals("")){
                        shiftclose_dates = reversedate;
                        dateClose = date.getText().toString();
                        dateshift =  dateClose+shiftclose.toString();
                    }else{
                        Calendar calendar    =    Calendar.getInstance();
                        String strt =yesterdayshiftdate.toString();;
                        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
                        try {
                            mdate = formats.parse(strt);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
                        shiftclose_dates = simpleDate.format(mdate);
                        dateClose =  yesterdayshiftdate.toString();
                        dateshift =  dateClose+shiftclose.toString();
                    }


                    db.insertCloseShift(CLOSE_SHIFT, dateClose, shiftclose,dateshift);

                    db.inserttrucksheetreport(vehicle_nos, driver_names, contact_nos, shiftclose_dates, shiftclose_times, shift_wt_cows,
                                shift_wt_buffalos, shift_fat_cows, shift_fat_buffalos, shift_snf_cows, shift_snf_buffalos,
                                shift_sum_cow_buffalos, shiftclose);

                    ragi2(vehicle_nos, driver_names, contact_nos, shiftclose_dates, shiftclose_times, shift_wt_cows,
                                shift_wt_buffalos, shift_fat_cows, shift_fat_buffalos, shift_snf_cows, shift_snf_buffalos,
                                shift_sum_cow_buffalos, shiftclose,Shistcenter_number);

                        Toast.makeText(getActivity(), "Shift closed", Toast.LENGTH_LONG).show();
                    }
                }
            });

        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();

                shift_wtcow       =      shift_wt_cow.getText().toString();
                shift_wtbuffalo   =      shift_wt_buffalo.getText().toString();
                shift_fatcow      =      shift_fat_cow.getText().toString();
                shift_fatbuffalo  =      shift_fat_buffalo.getText().toString();
                shift_snfcow      =      shift_snf_cow.getText().toString();
                shift_snfbuffalo  =      shift_snf_buffalo.getText().toString();
                shift_date        =      shiftclose_date.getText().toString();
                shift_time        =      shiftclose_time.getText().toString();
                shift_vehicleno   =      vehicle_no.getText().toString();
                shift_drivername  =      driver_name.getText().toString();
                shift_contctno    =      contact_no.getText().toString();


                String message = ("Total weight cow:"+shift_wtcow+", "+"Total weight Buffalo:"+shift_wtbuffalo+", "
                        +"Avg fat cow:"+shift_fatcow+", "+"Avg fat buffalo:"+shift_fatbuffalo+", "+"Avg snf cow:"+shift_snfcow+","+
                        "Avg snf buffalo :"+shift_snfbuffalo+", "
                        +"Date:"+shift_date+", "+"Time:"+shift_time+", "+"Vehicle No:"+shift_vehicleno+", "+"Driver Name:"+shift_drivername+", "+
                        "Contact No:"+shift_contctno);

                sendMessage(message);
            }
        });
        dialog1.show();
    }



    public void displayShiftReport() {
        String shift_wt_cows        =   total_wt_cow.getText().toString();
        shift_wt_cow.setText(shift_wt_cows);
        String shift_wt_buffalos    =   total_wt_buffalo.getText().toString();
        shift_wt_buffalo.setText(shift_wt_buffalos);
        String shift_fat_cows       =   aVg_cow_fat.getText().toString();
        shift_fat_cow.setText(shift_fat_cows);
        String shift_fat_buffalos   =  avg_buffalo_fat.getText().toString();
        shift_fat_buffalo.setText(shift_fat_buffalos);
        String shift_snf_cows       =   avg_cow_snf.getText().toString();
        shift_snf_cow.setText(shift_snf_cows);
        String shift_snf_buffalos   =  avg_buffalo_snf.getText().toString();
        shift_snf_buffalo.setText(shift_snf_buffalos);
    }

    private void ragi2(final String vehicle_nos, final String driver_names, final String contact_nos, final String shiftclose_dates,
                       final String shiftclose_times, final String shift_wt_cows, final String shift_wt_buffalos,
                       final String shift_fat_cows, final String shift_fat_buffalos, final String shift_snf_cows,
                       final String shift_snf_buffalos, final String shift_sum_cow_buffalos, final String shiftclose,final String Shistcenter_number) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, CLS_SHIFT_API, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePost:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error:" + error.toString());
                db.addcloseShiftSynchronizedata(null,vehicle_nos,driver_names,contact_nos,shiftclose_dates,shiftclose_times,
                        shift_wt_cows,shift_wt_buffalos,shift_fat_cows,shift_fat_buffalos,shift_snf_cows,shift_snf_buffalos,
                        shift_sum_cow_buffalos,shiftclose,Shistcenter_number);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("vehicle_number",vehicle_nos );
                map.put("driver_name", driver_names);
                map.put("driver_contact", contact_nos);
                map.put("date",shiftclose_dates);
                map.put("time",shiftclose_times);
                map.put("total_weight_cow",shift_wt_cows);
                map.put("total_weight_buffalo",shift_wt_buffalos);
                map.put("avgfat_cow",shift_fat_cows);
                map.put("avgfat_buffalo",shift_fat_buffalos);
                map.put("avgsnf_cow",shift_snf_cows);
                map.put("avgsnf_buffalo",shift_snf_buffalos);
                map.put("total",shift_sum_cow_buffalos);
                map.put("shift",shiftclose);
                map.put("center_number",Shistcenter_number);


                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            Log.d("aaaa","aaaa");
        } else {
            if (mChatService == null) setupChat();
            Log.d("bbbb","bbbb");
        }
    }
    @Override
    public synchronized void onResume() {
        super.onResume();
        if (mBluetoothAdapter.isEnabled()) {
            if (mChatService != null) {
                if (mChatService.getState() == BluetoothChatService.STATE_NONE) {
                    mChatService.start();
                    Log.d("cccc", "cccc");
                }
            }
        }
    }

    private void setupChat() {
        weight.setOnEditorActionListener(mWriteListener);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView view1    =   (TextView)getActivity().findViewById(R.id.weight);
                TextView view2    =   (TextView)getActivity().findViewById(R.id.fat);
                TextView view3    =   (TextView)getActivity().findViewById(R.id.lacto);
                TextView view5    =   (TextView)getActivity().findViewById(R.id.protein);
                TextView view6    =   (TextView)getActivity().findViewById(R.id.water);

                String messa1   = view1.getText().toString();
                String message2 = view2.getText().toString();
                String message3 = view3.getText().toString();
                String message5 = view5.getText().toString();
                String message6 = view6.getText().toString();

                String message = (messa1+"\\n"+message2+"\\n"+message3+"\\n"+message5+"\\n"+message6+"\\n");
                Log.d("message:",message);
//                sendMessage(message);
            }
        });
        mChatService = new BluetoothChatService(getActivity(), mHandler);
        mOutStringBuffer = new StringBuffer("");
    }
    @Override
    public synchronized void onPause() {
        super.onPause();
    }
    @Override
    public void onStop() {
        super.onStop();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) mChatService.stop();
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    public  void sendMessage(String message) {
        if (mChatService.getState() != BluetoothChatService.STATE_CONNECTED) {
            Toast.makeText(getActivity(), "not connected", Toast.LENGTH_SHORT).show();
            return;
        }
        if (message.length() > 0) {
            byte[] send = message.getBytes();
            mChatService.write(send);
            mOutStringBuffer.setLength(0);
            weight.setText(mOutStringBuffer);
            fat.setText(mOutStringBuffer);
            lacto.setText(mOutStringBuffer);
            snf.setText(mOutStringBuffer);
            protien.setText(mOutStringBuffer);
            water.setText(mOutStringBuffer);
        }
    }

    private TextView.OnEditorActionListener mWriteListener =
            new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_UP) {
                        String message = view.getText().toString();
                        sendMessage(message);
                    }
                    return true;
                }
            };

    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    break;
                case MESSAGE_READ:
                    db.deleteFarmerRegRecord2();
                    dismissDialog();
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    Log.d("mrunali",readMessage);
                    String[] splits = readMessage.split("\\\\n");

                    int pos  = 1;
                    for(int i =0;i< splits.length;i++){
                        db.insertblue(pos,splits[i]);
                        pos++;
                    }
                    getbluutoothdata();
                    //                    weight.setText(readMessage);
                    break;
                case MESSAGE_DEVICE_NAME:
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getActivity().getApplicationContext(), "Connected to "
                            + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getActivity().getApplicationContext(), msg.getData().getString(TOAST),
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                if (resultCode == Activity.RESULT_OK) {
                    String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
                    mChatService.connect(device);
                }
                break;
            case REQUEST_ENABLE_BT:
                if (resultCode == Activity.RESULT_OK) {
                    setupChat();
                } else {
                    Toast.makeText(getActivity(), "Bluetooth was not enabled", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void openDiologue(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.layout_dialog,null);
        textViewtext1 = (EditText)view.findViewById(R.id.farmerno);
        textViewtext2 =(EditText) view.findViewById(R.id.farmername12);
        TextViewtext1Complete = (EditText) view.findViewById(R.id.farmernocomplete);
        userList1 = (ListView)view.findViewById(R.id.listdialog);


        db = new DatabaseHelper(getActivity().getApplicationContext());
        db.getReadableDatabase();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.create();
        displayData();
        ListData();
        ClearText();
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (CollectionFragment.setProfilePic() != null ) {
                    byte[] img = CollectionFragment.setProfilePic();
                    Log.e("byte", img.toString());

                    Bitmap bitmap = BitmapFactory.decodeByteArray(img, 0, img.length);
                    Log.d("bit", String.valueOf(bitmap));

                    int currentwidth    =   bitmap.getWidth();
                    int currentheight   =   bitmap.getHeight();
                    int ivwidth         =   ProfImg.getWidth();
                    int ivheight        =   ProfImg.getHeight();
                    int newwidth        =   ivwidth;

                    int newHeight       = (int) Math.floor((double) currentheight * ((double) newwidth / (double) currentwidth));
                    newbitmap      = Bitmap.createScaledBitmap(bitmap, newwidth, newHeight, true);
                }

                if(textViewtext2.getText().toString().length() >0) {
                    User u ;
                    u = new User();
                    farmerNo.setText(GetID);
                    farmerName.setText(textViewtext2.getText().toString());
                    farmreNoComplte.setText(TextViewtext1Complete.getText().toString());
                    cattletype.setText(cattlevalue.toString());
//                    new GetImageFromUrl(ProfImg).execute(urlImage2);
//                                      ProfImg.setImageBitmap(newbitmap);

                    if(urlImage.equals("http://mcsoft.whitegoldtech.com:8888/images/no-image.jpg")) {
                    }else {
                        new GetImageFromUrl(ProfImg).execute(urlImage);

                    }
                    if(urlImage2.equals("http://mcsoft.whitegoldtech.com:8888/images/no-image.jpg")) {
                    }else {
                        new GetImageFromUrl(ProfImg).execute(urlImage2);
                    }

                    if(cattlevalue.equals("cow")){
                        cow.setChecked(true);
                        Log.d("cow","cow");
                        buffalo.setClickable(false);

                    }

                    if(cattlevalue.equals("buffelo")){
                        buffalo.setChecked(true);
                        Log.d("buffalo","buffalo");
                        cow.setClickable(false);
                    }

                    if(cattlevalue.equals("both")){
                        cow.setChecked(true);
                        buffalo.setClickable(true);
                        radiogrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(RadioGroup group, int checkedId) {
                                if (checkedId == R.id.buffalo) {
                                    userCattle = "buffelo";
                                    buffalo.setClickable(true);
                                }
                            }
                        });
                    }

                    //.....Calculate rate value according rate chart table.......
                    db = new DatabaseHelper(getActivity().getApplicationContext());
                    final SQLiteDatabase dataBase   =   db.getWritableDatabase();
                    Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                            + DatabaseHelper.ratechartmatrix, null);
                    if (mCursor2.moveToFirst()) {
                        do {
                            rate_matrix_id = mCursor2.getString(mCursor2.getColumnIndex("ratechart_id"));
                        } while (mCursor2.moveToNext());
                    }

                    final Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                            + DatabaseHelper.downloadsettingdata, null);
                    if (mCursor.moveToFirst()) {
                        do {
                            mergshiftcattle = userCattle+ mor_eve_shift.getText().toString();
                            Log.d("mergshiftcattle",mergshiftcattle);

                            if(mergshiftcattle.equals("cowMorning")){
                                tra_id = mCursor.getString(mCursor.getColumnIndex("cowmorning"));
                                litervalue = mCursor.getString(mCursor.getColumnIndex("littervalue"));
                            }else if(mergshiftcattle.equals("buffeloMorning")){
                                tra_id = mCursor.getString(mCursor.getColumnIndex("buffelomorning"));
                                litervalue = mCursor.getString(mCursor.getColumnIndex("littervalue"));
                            }else if(mergshiftcattle.equals("cowEvening")){
                                tra_id = mCursor.getString(mCursor.getColumnIndex("cowevening"));
                                litervalue = mCursor.getString(mCursor.getColumnIndex("littervalue"));
                            }else if(mergshiftcattle.equals("buffeloEvening")){
                                tra_id = mCursor.getString(mCursor.getColumnIndex("buffeloevening"));
                                litervalue = mCursor.getString(mCursor.getColumnIndex("littervalue"));
                            }
                        } while (mCursor.moveToNext());
                    }

                    Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                            + DatabaseHelper.listRatechart, null);
                    if (mCursor1.moveToFirst()) {
                        do {
                            rate_id = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                            String RateMatrixID = tra_id;
                            if(mCursor.getCount()== 0){
                                Log.d("traid","tra_id");
                            }else
                            if (tra_id.equals(rate_id)) {
                                charttype = mCursor1.getString(mCursor1.getColumnIndex("charttype"));
                                if(RateMatrixID.equals(rate_id)) {
                                    if (charttype.equals("fat-chart")) {
                                        fatbasertatecalculate();
                                    } else if (charttype.equals("fatsnf-chart")) {
                                        fatssnfbasertatecalculate();
                                    }
                                }
                            }
                        } while (mCursor1.moveToNext());
                    }

                }else{
                    ClearText();
                }
            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.show();
    }

    private void displayData() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                GetID = textViewtext1.getText().toString();
                SQLiteDatabase dataBase = db.getWritableDatabase();
                User u;
                Cursor cursor = dataBase.rawQuery("select * from farmermaster where substr(farmer_number,-4) = ?",
                        new String[]{String.valueOf(GetID)}, null);

                if (cursor !=null && cursor.getCount() > 0){
                    cursor.moveToFirst();

                    u = new User();

                    u.setCource(cursor.getString(cursor.getColumnIndex("farmername")));
                    urlImage = cursor.getString(cursor.getColumnIndex("app_image"));
                    urlImage2 = cursor.getString(cursor.getColumnIndex("image"));
                    cattlevalue = cursor.getString(cursor.getColumnIndex("cattlevalue"));
                    String farmernocompletes = cursor.getString(cursor.getColumnIndex("farmer_number"));


                    textViewtext2.setText(u.getCource());
                    name = textViewtext2.getText().toString();
                    TextViewtext1Complete.setText(farmernocompletes);
                    cursor.close();
                    db.close();

                }else {
//                    Toast.makeText(getActivity(), "farmer number invalid", Toast.LENGTH_SHORT).show();
                    cursor.close();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        textViewtext1.addTextChangedListener(textWatcher);

    }

    public static byte[] setProfilePic() {
        return datas;
    }

    public static class GetImageFromUrl extends AsyncTask<String, Void, Bitmap> {
        ImageView imageView;
        public GetImageFromUrl(ImageView img){
            this.imageView = img;
        }
        @Override
        protected Bitmap doInBackground(String... url) {
            String stringUrl = url[0];
            bitmap = null;
            InputStream inputStream;
            try {
                inputStream = new java.net.URL(stringUrl).openStream();
                bitmap = BitmapFactory.decodeStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return bitmap;
        }
        @Override
        protected void onPostExecute(Bitmap bitmap){
            super.onPostExecute(bitmap);
            imageView.setImageBitmap(bitmap);
        }
    }

    public void ClearText() {
        farmerNo.setText("");
        farmerName.setText("");
        ProfImg.setImageBitmap(null);
        cow.setChecked(false);
        buffalo.setChecked(false);
    }

    private void ListData() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.TABLE_NAME, null);
        id.clear();
        firstname.clear();

        if (mCursor.moveToFirst()) {
            do {
                id.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNUMBER)));
                firstname.add(mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.FARMERNAME)));

            } while (mCursor.moveToNext());

        }

        FarmerSelectionAdapter disadpt = new FarmerSelectionAdapter(getActivity().getApplicationContext(), id, firstname);
        userList1.setAdapter(disadpt);
        userList1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ViewGroup vg   = (ViewGroup)view;
                TextView txt   = (TextView)vg.findViewById(R.id.textviewdg1);

                String no = txt.getText().toString();
                String lastdigit = no.substring((no.length() -4));

                textViewtext1.setText(lastdigit);
                TextViewtext1Complete.setText(no);
            }
        });
        mCursor.close();
    }


    private void openVideoview(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.video_popup,null);
        videoView = (VideoView) view.findViewById(R.id.video_view);

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.create();

        Uri uri = Uri.parse("android.resource://"+ getActivity().getApplicationContext().getPackageName() + "/"+ R.raw.vide);
        MediaController mediaController = new MediaController(getActivity());
        mediaController.setAnchorView(videoView);
        mediaController.setMediaPlayer(videoView);
        videoView.setMediaController(mediaController);
        videoView.requestFocus();
        videoView.setVideoURI(uri);
        videoView.start();
        show = builder.show();


        if(cattletype.getText().toString().equals("cow")){
            cow.setChecked(true);
            buffalo.setClickable(false);
        }

        if(cattletype.getText().toString().equals("buffelo")) {
            buffalo.setChecked(true);
            cow.setClickable(false);
        }

        if(cattletype.getText().toString().equals("both")) {
            cow.setChecked(true);
        }
    }

    public void dismissDialog(){
        show.cancel();
    }

    private void flashmsg(){
        final Toast toast = Toast.makeText(getActivity().getApplicationContext(),
                "Pour the milk and put the sample on analyser. wait for analyser cycle to finish.",Toast.LENGTH_LONG);
        toast.setGravity(Gravity.FILL_HORIZONTAL|Gravity.BOTTOM,0,0);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },50000);
        toast.show();
    }


    public void getbluutoothdata(){
        SQLiteDatabase dataBase1 = db.getWritableDatabase();
        Cursor cursor = dataBase1.rawQuery("select * from bluetooth where pos = 1", null);
        if (cursor !=null && cursor.getCount() > 0){
            cursor.moveToFirst();
            String id1 = cursor.getString(cursor.getColumnIndex("result"));
            weight.setText(id1);
            cursor.close();
            db.close();
        }

        SQLiteDatabase dataBase2 = db.getWritableDatabase();
        Cursor cursor1 = dataBase2.rawQuery("select * from bluetooth where pos = 2",null);
        if (cursor1 !=null && cursor1.getCount() > 0){
            cursor1.moveToFirst();
            String id1 = cursor1.getString(cursor1.getColumnIndex("result"));
            fat.setText(id1);
            cursor1.close();
            db.close();
        }

        SQLiteDatabase dataBase3 = db.getWritableDatabase();
        Cursor cursor2 = dataBase3.rawQuery("select * from bluetooth where pos = 3", null);
        if (cursor2 !=null && cursor2.getCount() > 0){
            cursor2.moveToFirst();
            String id1 = cursor2.getString(cursor2.getColumnIndex("result"));
            lacto.setText(id1);
            cursor2.close();
            db.close();
        }

        SQLiteDatabase dataBase5 = db.getWritableDatabase();
        Cursor cursor4 = dataBase5.rawQuery("select * from bluetooth where pos = 4", null);
        if (cursor4 !=null && cursor4.getCount() > 0){
            cursor4.moveToFirst();
            String id1 = cursor4.getString(cursor4.getColumnIndex("result"));
            protien.setText(id1);
            cursor4.close();
            db.close();
        }

        SQLiteDatabase dataBase6 = db.getWritableDatabase();
        Cursor cursor6 = dataBase6.rawQuery("select * from bluetooth where pos = 5", null);
        if (cursor6 !=null && cursor6.getCount() > 0){
            cursor6.moveToFirst();
            String id1 = cursor6.getString(cursor6.getColumnIndex("result"));
            water.setText(id1);
            cursor6.close();
            db.close();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mymenu,menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        switch (item.getItemId()){
            case R.id.openblu:
                Intent serverIntent = new Intent(getActivity().getApplicationContext(), DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                return true;
            default:
                break;
        }
        return false;
    }

    private void setupToolbar() {
        toolbar.setTitle("Collection");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}

