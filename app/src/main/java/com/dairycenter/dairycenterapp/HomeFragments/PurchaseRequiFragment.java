package com.dairycenter.dairycenterapp.HomeFragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.Adapter.MyAdapter;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.ItemClickListener;
import com.dairycenter.dairycenterapp.ProductListActivity;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.UserData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.dairycenter.dairycenterapp.HomeFragments.CollectionFragment.inputFormat;


public class PurchaseRequiFragment extends Fragment {

    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static TextView purchase_date, purchase_rmcuname, purchase_centername, productno, rought, product_unit,time;
    public static Spinner product_name;
    public static EditText quantity;
    DatabaseHelper db;
    public static String purchase_rmcunames, purchase_centernames, product, quan, purchase_rmcuid, purchase_centerid;
    public static Button Add, Save, Views_product,close,cancel;
    public static ListView listview;
    public static final String PURCHASE_API = "http://mcsoft.whitegoldtech.com:8888/api/purchaseform";
    public static final String PURCHASE_API2 = "http://mcsoft.whitegoldtech.com:8888/api/purchaseformnew";

    String proname;
    RecyclerView recyclerView;
    MyAdapter adapter;
    List<UserData> list = new ArrayList<>();
    String Demo;
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat,Locale.UK);
    Date mdate;
    String reversedate;

    Switch switches;

    TextView english,gujrati,purchasename,txtdate,txttime,rmcu_names,centernames,s1,s2,s3,s4,pro,pro2,prounit,pro3,pro5,delete;


    String gujaratilang;


    public PurchaseRequiFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_purchase_requi, container, false);
        toolbar                = (Toolbar) view.findViewById(R.id.home_toolbar);
        purchase_date          = (TextView) view.findViewById(R.id.purchase_date);
        time                   = (TextView) view.findViewById(R.id.purchase_time);
        purchase_rmcuname      = (TextView) view.findViewById(R.id.rmcu_name);
        purchase_centername    = (TextView) view.findViewById(R.id.center_name);
        productno              = (TextView) view.findViewById(R.id.product_number);
        rought                 = (TextView) view.findViewById(R.id.rough);
        product_unit           = (TextView) view.findViewById(R.id.unit);
        quantity               = (EditText) view.findViewById(R.id.Quantity);
        product_name           = (Spinner) view.findViewById(R.id.purchase_spinner1);
        Add                    = (Button) view.findViewById(R.id.purchase_add);
        Save                   = (Button) view.findViewById(R.id.save);
        close                  = (Button) view.findViewById(R.id.cls);
        cancel                 = (Button) view.findViewById(R.id.cancel);
        Views_product          = (Button) view.findViewById(R.id.views);
        recyclerView           = (RecyclerView) view.findViewById(R.id.recycler_view);
        switches               = (Switch) view.findViewById(R.id.switchs);
        english                = (TextView) view.findViewById(R.id.english);
        gujrati                = (TextView) view.findViewById(R.id.gujrati);


        purchasename           = (TextView) view.findViewById(R.id.purchasename);     txtdate       = (TextView) view.findViewById(R.id.txtdate);
        txttime                = (TextView) view.findViewById(R.id.txttime);          rmcu_names    = (TextView) view.findViewById(R.id.rmcu_names);
        centernames            = (TextView) view.findViewById(R.id.centernames);      s1            = (TextView) view.findViewById(R.id.s1);
        s2                     = (TextView) view.findViewById(R.id.s2);               s3            = (TextView) view.findViewById(R.id.s3);
        s4                     = (TextView) view.findViewById(R.id.s4);               pro           = (TextView) view.findViewById(R.id.pro);
        pro2                   = (TextView) view.findViewById(R.id.pro2);             prounit       = (TextView) view.findViewById(R.id.prounit);
        pro3                   = (TextView) view.findViewById(R.id.pro3);             pro5          = (TextView) view.findViewById(R.id.pro5);
        delete                 = (TextView) view.findViewById(R.id.delete);

        db = new DatabaseHelper(getActivity());
        db.getWritableDatabase();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));

        adapter = new MyAdapter(getActivity(), list);
        recyclerView.setAdapter(adapter);

        setupToolbar();

        db = new DatabaseHelper(getActivity().getApplicationContext());
        setRMCUName();
        setCenterName();
        setProduct_name();
        ProductNumber();

//        Calendar calendar = Calendar.getInstance();
//        String currentdate = DateFormat.getDateInstance().format(calendar.getTime());
//        purchase_date.setText(currentdate);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat.format(today);
        purchase_date.setText(currentdate);

        String strt = purchase_date.getText().toString();
        SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yyyy");
        try {
            mdate = formats.parse(strt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
        reversedate = simpleDate.format(mdate);

        Calendar now = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm", Locale.US);
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        Log.d("hour", String.valueOf(hour));
        Date datess = null;
        try {
            datess = sdf.parse(hour+ ":" +minute);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Log.d("datess", String.valueOf(datess));

        time.setText(hour+ ":" +minute);

        gujaratilang = "";

        switches.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(switches.isChecked()){
                    Log.d("On","On");
                    Resources res = getResources();
                    purchasename.setText(res.getString(R.string.PurchaseRequisition));
                    txtdate.setText(res.getString(R.string.date));
                    txttime.setText(res.getString(R.string.time));
                    rmcu_names.setText(res.getString(R.string.rmcu_name));
                    centernames.setText(res.getString(R.string.center_name));
                    s1.setText(res.getString(R.string.product_name));
                    s2.setText(res.getString(R.string.product_unit));
                    s3.setText(res.getString(R.string.product_number));
                    s4.setText(res.getString(R.string.product_quantity));
                    pro.setText(res.getString(R.string.product_number));
                    pro2.setText(res.getString(R.string.product_name));
                    prounit.setText(res.getString(R.string.product_unit));
                    pro3.setText(res.getString(R.string.requi_history));
                    pro5.setText(res.getString(R.string.edit));
                    delete.setText(res.getString(R.string.delete));

                    Add.setText(res.getString(R.string.add));
                    Save.setText(res.getString(R.string.save));
                    cancel.setText(res.getString(R.string.cancel));
                    close.setText(res.getString(R.string.close));
                    Views_product.setText(res.getString(R.string.requi_history));


                }else{
                    Log.d("Off","Off");
                    Resources res = getResources();
                    purchasename.setText("Purchase Requisition");
                    txtdate.setText("Date:");
                    txttime.setText("Time:");
                    rmcu_names.setText("Society Name:");
                    centernames.setText("Center Name:");
                    s1.setText("Product Name:");
                    s2.setText("Product Unit:");
                    s3.setText("Product Number:");
                    s4.setText("Quantity:");
                    pro.setText("Product Number");
                    pro2.setText("Product Name");
                    prounit.setText("Product Unit");
                    pro3.setText("Requested Quantity");
                    pro5.setText("Edit");
                    delete.setText("Delete");

                    Add.setText("Add");
                    Save.setText("Save");
                    cancel.setText("Cancel");
                    close.setText("Close");
                    Views_product.setText("Requisition history");
                }
            }
        });






        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product_name.getSelectedItem().toString().equals("Select")) {
                    if (product_name.getChildAt(0) != null && product_name.getChildAt(0) instanceof TextView) {
                        product_name.getChildAt(0).setFocusable(true);
                        ((TextView) product_name.getChildAt(0)).setError("error");
                        product_name.getChildAt(0).requestFocus();
                    }
                } else if (product_unit.getText().toString().isEmpty()) {
                    product_unit.setError("please enter product unit");
                    product_unit.requestFocus();
                } else if (productno.getText().toString().isEmpty()) {
                    productno.setError("please enter product no");
                    productno.requestFocus();
                } else if (quantity.getText().toString().isEmpty()) {
                    quantity.setError("please enter quantity");
                    quantity.requestFocus();
                } else {
                    proname = product_name.getSelectedItem().toString();
                    String prounit = product_unit.getText().toString();
                    quan = quantity.getText().toString();
                    String prono = productno.getText().toString();

                    Demo = "Data";
                    for (int i = 0; i < list.size(); i++) {
                        if(list.get(i).getEmail().contains(proname)){
                            Toast.makeText(getActivity(), "Product already added", Toast.LENGTH_SHORT).show();
                            clear();
                            Demo = "Demo";
                        }
                    }

                    if(!Demo.toString().equals("Demo")){
                        UserData userData = new com.dairycenter.dairycenterapp.UserData();
                        userData.setName(prono);
                        userData.setEmail(proname);
                        userData.setUnit(prounit);
                        userData.setEtc(quan);

                        list.add(userData);
                        adapter.notifyDataSetChanged();
                        Toast.makeText(getActivity(), "Add", Toast.LENGTH_SHORT).show();
                        clear();
                    }
                }
            }
        });


        adapter.setOnItemClickListener(new ItemClickListener() {
            @Override
            public void OnItemClick(int position, UserData userData,String produnit,String prodname,String prodno,String edit) {
                UserData userData1 = new UserData();
                userData1.setEmail(prodname);
                userData1.setName(prodno);
                userData1.setUnit(produnit);
                userData1.setEtc(edit);
                list.remove(position);
                list.add(position,userData1);
                adapter.notifyDataSetChanged();
                Toast.makeText(getActivity(), "Edited", Toast.LENGTH_SHORT).show();
            }
        });


        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.deletepurchasedata();
                if (list.isEmpty()) {
                    Toast.makeText(getActivity(), "Please add product", Toast.LENGTH_SHORT).show();
                } else {
                    openConfirmationdialogbox();
                }
            }
        });

        Views_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), ProductListActivity.class);
                startActivity(i);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product_name.setSelection(0);
                quantity.setText("");
                productno.setText("");
                product_unit.setText("");
                list.clear();
                adapter.notifyDataSetChanged();
            }
        });

        return view;
    }

    public void clear() {
        product_name.setSelection(0);
        quantity.setText("");
        productno.setText("");
        product_unit.setText("");

    }

    private Date parseDate(String Date){
        try{
            return inputParser.parse(Date);
        }catch(ParseException e ){
            return new Date(0);
        }
    }

    private void setAppLocaleString(String localcode){
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(localcode.toLowerCase()));
        res.updateConfiguration(conf,dm);

        purchasename.setText(res.getString(R.string.PurchaseRequisition));
    }




    public void setRMCUName() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.rmcumaster, null);
        if (mCursor.moveToFirst()) {
            do {
                purchase_rmcunames = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.rmcuname));
                purchase_rmcuid = mCursor.getString(mCursor.getColumnIndex("id"));
            } while (mCursor.moveToNext());

        }
        purchase_rmcuname.setText(purchase_rmcunames);
    }


    public void setCenterName() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor2.moveToFirst()) {
            do {
                purchase_centernames = mCursor2.getString(mCursor2.getColumnIndex(DatabaseHelper.cenetrname));
                purchase_centerid = mCursor2.getString(mCursor2.getColumnIndex("id"));
            } while (mCursor2.moveToNext());
        }
        purchase_centername.setText(purchase_centernames);
    }


    public void setProduct_name() {
        List<String> list = db.getAllProductName();
        list.add(0, "Select");

        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        product_name.setAdapter(dataAdapter);
        product_name.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String typeofdeductions = parent.getItemAtPosition(position).toString();
                rought.setText(typeofdeductions);
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void ProductNumber() {
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                product = rought.getText().toString();
                SQLiteDatabase dataBase = db.getWritableDatabase();

                Cursor cursor = dataBase.rawQuery("select * from productmaster where product_name = ?",
                        new String[]{String.valueOf(product)}, null);
                if (cursor != null && cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    String productNO = cursor.getString(cursor.getColumnIndex("id"));
                    productno.setText(productNO);
                    String productUnit = cursor.getString(cursor.getColumnIndex("unit_scale"));

                    SQLiteDatabase dataBase2 = db.getWritableDatabase();
                    Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM "
                            + DatabaseHelper.unitmaster, null);
                    if (mCursor2.moveToFirst()) {
                        do {
                            if(productUnit.toString().equals(mCursor2.getString(mCursor2.getColumnIndex("id")))){
                                String unitname = mCursor2.getString(mCursor2.getColumnIndex("unitname"));
                                product_unit.setText(unitname);
                            }
                        } while (mCursor2.moveToNext());
                    }


                    cursor.close();
                    db.close();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        rought.addTextChangedListener(textWatcher);
    }


    private void openConfirmationdialogbox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure to requisite product");

        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                purchase_rmcuid.toString();
                purchase_centerid.toString();
                ragi();

                int listsize = list.size();

                db.insertsecondpurchase(null,purchase_centerid,purchase_rmcuid,purchase_centernames,purchase_rmcunames,listsize);

                for (int i = 0; i < list.size(); i++) {
                    db.insertsave(null, list.get(i).getName(), list.get(i).getEmail(), list.get(i).getUnit(), list.get(i).getEtc());
                }
                Toast.makeText(getActivity(), "Save successfully", Toast.LENGTH_SHORT).show();
                list.clear();
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }


    private void ragi() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, PURCHASE_API,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("Responce1:" + response.toString());
//                    Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity().getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                VolleyLog.d("error",error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();

                JSONArray array = new JSONArray();
                JSONObject object  = new JSONObject();
                try {
                    object.put("centername",purchase_centernames);
                    object.put("rmcuname",purchase_rmcunames);
                    object.put("centerid",purchase_centerid);
                    object.put("rmcuid",purchase_rmcuid);
                    object.put("date", reversedate);
                    object.put("flag","active" );
                    object.put("listsize",list.size());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < list.size(); i++) {
                    JSONObject   object1 = new JSONObject();
                    try {
                        object1.put("ProductName", list.get(i).getEmail());
                        object1.put("Unit", list.get(i).getUnit());
                        object1.put("quantity", list.get(i).getEtc());
                        object1.put("productnumber", list.get(i).getName());
                        object1.put("centername",purchase_centernames);
                        object1.put("rmcuname",purchase_rmcunames);
                        object1.put("centerid",purchase_centerid);
                        object1.put("rmcuid",purchase_rmcuid);
                        object1.put("flag","active" );
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    array.put(object1);
                }

                map.put("objectdata",object.toString());
                map.put("data", array.toString());

                Log.d("data",array.toString());

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void setupToolbar() {
        toolbar.setTitle("Purchase Requisition");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}





