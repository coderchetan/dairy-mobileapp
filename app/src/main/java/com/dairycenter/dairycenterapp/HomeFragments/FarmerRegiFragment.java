package com.dairycenter.dairycenterapp.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.FarmerListActivity;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabBankDetailes3;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabCattleDetailes4;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabContactDetailes2;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabFarmerName1;
import com.dairycenter.dairycenterapp.FarmerRegistrationTab.TabFarmerTempDisable5;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.R;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FarmerRegiFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    public static ViewPager viewPager;
    TabLayout tabLayout;
    Button backbutton;

    public static Button save, nextbutton, save1,cancel,close;

    public static String firstname, middlename, lastname, firstReligionName, middleReligionName, lastReligionName, address,
            contactNo, city, state, pincode, adhar_no, bankname, branchname, accountno, ifsccode, noofcow, noofbuffelo,
            noofcattle, user_image,centernos,rmcunos,demo,rfid;;

    public static final String FARMER_LIST_REGI = "http://mcsoft.whitegoldtech.com:8888/api/farmernewform";

    DatabaseHelper db;
    public static byte[] datas;
    public static String imgString;
    int pos;

    String cattletype;


    public FarmerRegiFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_farmer_regi, container, false);

        toolbar      =   (Toolbar) view.findViewById(R.id.home_toolbar);
        viewPager    =   (ViewPager) view.findViewById(R.id.pager);
        tabLayout    =   (TabLayout) view.findViewById(R.id.tab_layout);
        backbutton   =   (Button) view.findViewById(R.id.backbutton);
        nextbutton   =   (Button) view.findViewById(R.id.nextbutton);
        save1        =   (Button) view.findViewById(R.id.save1);
        close        =   (Button) view.findViewById(R.id.cls);
        cancel       =   (Button) view.findViewById(R.id.cancel);


        save1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), FarmerListActivity.class);
                startActivity(i);
            }
        });


        db   = new DatabaseHelper(getActivity().getApplicationContext());
        db.getWritableDatabase();
        setRMCUNumber();
        setCenterNumber();

        save = (Button) view.findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TabFarmerName1.getProfilePic() != null) {
                    datas = TabFarmerName1.getProfilePic();
                }

                if (TabFarmerName1.getProfilePic2() != null) {
                    imgString = TabFarmerName1.getProfilePic2();

                }

                firstname          =     TabFarmerName1.firstname1.getText().toString();
                middlename         =     TabFarmerName1.middlename1.getText().toString();
                lastname           =     TabFarmerName1.lastname1.getText().toString();
                firstReligionName  =     TabFarmerName1.fisrtRelegionName.getText().toString();
                middleReligionName =     TabFarmerName1.MiddleReligionName.getText().toString();
                lastReligionName   =     TabFarmerName1.LastReligionname.getText().toString();

                address            =     TabContactDetailes2.Address.getText().toString();
                contactNo          =     TabContactDetailes2.contact_no.getText().toString();
                adhar_no           =     TabContactDetailes2.adhar_no.getText().toString();
                city               =     TabContactDetailes2.city.getText().toString();
                state              =     TabContactDetailes2.state.getText().toString();
                pincode            =     TabContactDetailes2.pin_code.getText().toString();

                bankname           =     TabBankDetailes3.name_bank.getSelectedItem().toString();
                branchname         =     TabBankDetailes3.branch_bank.getText().toString();
                accountno          =     TabBankDetailes3.no_account.getText().toString();
                ifsccode           =     TabBankDetailes3.code_ifsc.getText().toString();

                noofcow            =     TabCattleDetailes4.Noofcow.getText().toString();
                noofbuffelo        =     TabCattleDetailes4.Noofbuffalo.getText().toString();
                noofcattle         =     TabCattleDetailes4.totalNoOfCattle.getText().toString();
                rfid               =     TabCattleDetailes4.rfid.getText().toString();

                String disable = TabFarmerTempDisable5.value.toString();
                Log.d("Values",disable);

                cattletype = TabCattleDetailes4.usercentercattle.toString();
                Log.d("cattletype",cattletype);


                if (firstname.trim().isEmpty()) {
                    TabFarmerName1.firstname1.setError("farmer name not entered");
                    TabFarmerName1.firstname1.requestFocus();
                } else if (lastname.trim().isEmpty()) {
                    TabFarmerName1.lastname1.setError("farmer last name not entered");
                    TabFarmerName1.lastname1.requestFocus();
                } else if (firstReligionName.trim().isEmpty()) {
                    TabFarmerName1.fisrtRelegionName.setError("farmer rigional name not entered");
                    TabFarmerName1.fisrtRelegionName.requestFocus();
                } else if (lastReligionName.trim().isEmpty()) {
                    TabFarmerName1.LastReligionname.setError("farmer rigional last name not entered");
                    TabFarmerName1.LastReligionname.requestFocus();
                } else if (address.trim().isEmpty()) {
                    TabContactDetailes2.Address.setError("Address not entered");
                    TabContactDetailes2.Address.requestFocus();
                } else if (contactNo.trim() == null || contactNo.trim().length() < 10) {
                    TabContactDetailes2.contact_no.setError("Enter 10 digit mobile number");
                    TabContactDetailes2.contact_no.requestFocus();
                } else if (adhar_no.trim() == null || adhar_no.trim().length() < 12) {
                    TabContactDetailes2.adhar_no.setError("Enter a twelve digit adhar number");
                    TabContactDetailes2.adhar_no.requestFocus();
                } else if (pincode.trim() == null || pincode.trim().length() < 6) {
                    TabContactDetailes2.pin_code.setError("Enter a six digit pin code");
                    TabContactDetailes2.pin_code.requestFocus();
                } else if (TabBankDetailes3.name_bank.getSelectedItem().toString().equals("Select")) {
                    if (TabBankDetailes3.name_bank.getChildAt(0) != null && TabBankDetailes3.name_bank.getChildAt(0) instanceof TextView) {
                        TabBankDetailes3.name_bank.getChildAt(0).setFocusable(true);
                        ((TextView) TabBankDetailes3.name_bank.getChildAt(0)).setError("error");
                        TabBankDetailes3.name_bank.getChildAt(0).requestFocus();
                    }
                } else if (branchname.trim().isEmpty()) {
                    TabBankDetailes3.branch_bank.setError("branch name not entered");
                    TabBankDetailes3.branch_bank.requestFocus();
                } else if (accountno.trim() == null || accountno.length() < 12) {
                    TabBankDetailes3.no_account.setError("Enter a right account number");
                    TabBankDetailes3.no_account.requestFocus();
                } else if (ifsccode.trim() == null || ifsccode.trim().length() < 11) {
                    TabBankDetailes3.code_ifsc.setError("Enter a eleven digit IFSC code");
                    TabBankDetailes3.code_ifsc.requestFocus();
                } else if (noofcattle.trim().isEmpty()) {
                    TabCattleDetailes4.totalNoOfCattle.setError("Enter a number of cattle");
                    TabCattleDetailes4.totalNoOfCattle.requestFocus();
                }else if (rfid.trim().isEmpty()) {
                    TabCattleDetailes4.rfid.setError("Enter a RFID  number");
                    TabCattleDetailes4.rfid.requestFocus();
                } else {
                    db.insertFarmerRegiData(null, firstname, middlename, lastname, firstReligionName, middleReligionName,
                            lastReligionName, address, contactNo, adhar_no, city, state, pincode,
                            bankname, branchname, accountno, ifsccode, noofcow, noofbuffelo, noofcattle, datas,disable);

                    ragi(firstname, lastname,address,contactNo,adhar_no,city,state,pincode,bankname,branchname,
                            accountno,ifsccode,noofcow,noofbuffelo,noofcattle,rmcunos,centernos,imgString,cattletype,rfid);

                    Toast.makeText(getActivity().getApplicationContext(), "Registered Successfully", Toast.LENGTH_LONG).show();
                    TabFarmerName1 tabFarmerName1 = new TabFarmerName1();
                    tabFarmerName1.ClearText();
//                    TabFarmerName1.user_image.setImageResource(0);

                    TabContactDetailes2 tabContactDetailes2 = new TabContactDetailes2();
                    tabContactDetailes2.ClearText2();
                    TabBankDetailes3 tabBankDetailes3 = new TabBankDetailes3();
                    tabBankDetailes3.ClearText3();
                    TabCattleDetailes4 tabCattleDetailes4 = new TabCattleDetailes4();
                    tabCattleDetailes4.ClearText5();
                }
            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TabFarmerName1 tabFarmerName1 = new TabFarmerName1();
                tabFarmerName1.ClearText();
                TabContactDetailes2 tabContactDetailes2 = new TabContactDetailes2();
                tabContactDetailes2.ClearText2();
                TabBankDetailes3 tabBankDetailes3 = new TabBankDetailes3();
                tabBankDetailes3.ClearText3();
                TabCattleDetailes4 tabCattleDetailes4 = new TabCattleDetailes4();
                tabCattleDetailes4.ClearText5();
            }
        });




        final int position = 0;
        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstname          =     TabFarmerName1.firstname1.getText().toString();
                lastname           =     TabFarmerName1.lastname1.getText().toString();

                if (firstname.trim().isEmpty()) {
                    TabFarmerName1.firstname1.setError("farmer name not entered");
                    TabFarmerName1.firstname1.requestFocus();
                } else if (lastname.trim().isEmpty()) {
                    TabFarmerName1.lastname1.setError("farmer last name not entered");
                    TabFarmerName1.lastname1.requestFocus();
                }else{
                    viewPager.setCurrentItem(viewPager.getCurrentItem()+1, true);
//                    if(demo.equals("Valid1")) {
//
//                    }
//                    if(pos == 2){
//                        viewPager.setCurrentItem(3, true);
//                    }
//                    Log.d("mru", String.valueOf(viewPager.getCurrentItem()+1));
                }


//                if(TabContactDetailes2.gets() != null) {
//                    demo = TabContactDetailes2.gets();
//                    Log.d("demo0", demo);
//                    if (demo.equals("Demo")) {
//                        viewPager.setCurrentItem(position +1, true);
//                        Log.d("mru0", "mru0");
//                    }
//                }

                if(TabBankDetailes3.getstring() != null) {
                    demo = TabBankDetailes3.getstring();
                    Log.d("demo1", demo);
                    if (demo.equals("Demo")) {
//                        pos = 123;
                        viewPager.setCurrentItem(position +1, true);
                        Log.d("mru1", String.valueOf(position+1));
                    }
                }

                if(TabCattleDetailes4.getstr2() != null) {
                    demo = TabCattleDetailes4.getstr2();
                    Log.d("demo2", demo);
                    if (demo.equals("Demo")) {
                        pos = position+2;
                        viewPager.setCurrentItem(position +2, true);
                        Log.d("mru2", String.valueOf(position+2));
                    }
                }

                if(TabFarmerTempDisable5.getstr3() != null) {
                    demo = TabFarmerTempDisable5.getstr3();
                    Log.d("demo3", demo);
                    if (demo.equals("Demo")) {
                        viewPager.setCurrentItem(position +3, true);
                    }
                }
            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
            }
        });

        View root = tabLayout.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(getResources().getColor(R.color.date));
            drawable.setSize(2, 2);
            ((LinearLayout) root).setDividerPadding(1);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }

        setupToolbar();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

//        setupTabIcons();

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void ragi(final String firstname, final String lastname,final String address,final String contactNo,
                      final String adhar_no,final String city,final String state,final String pincode,
                      final String bankname,final String branchname,final String accountno,final String ifsccode,
                      final String noofcow,final String noofbuffelo,final String noofcattle,final String centernos,
                      final String rmcunos,final String imgString,final String cattletype,final String rfid) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, FARMER_LIST_REGI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePost:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                Toast.makeText(getContext(), error.toString(), Toast.LENGTH_LONG).show();
                VolleyLog.d("Errormru:" + error.toString());

                db.addfarmerregisynch(null,firstname,lastname,address,contactNo,adhar_no,city,state,pincode,bankname,branchname,
                        accountno,ifsccode,noofcow,noofbuffelo,noofcattle,centernos,rmcunos,imgString,cattletype,rfid);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("farmername", (firstname + " " + lastname));
                map.put("address", address);
                map.put("rfid", rfid);
                map.put("mobilenumber", contactNo);
                map.put("aadharcardnumber",adhar_no);
                map.put("district",city);
                map.put("state",state);
                map.put("pincode",pincode);
                map.put("bankname",bankname);
                map.put("branch",branchname);
                map.put("accountnumber",accountno);
                map.put("ifscvalue",ifsccode);
                map.put("totalcows",noofcow);
                map.put("totalbuffaloes",noofbuffelo);
                map.put("cattleshedvalue",noofcattle);
                map.put("cattlevalue",cattletype);
                map.put("centernumber",rmcunos);  //centernumber //i did change key...
                map.put("rmcunumber",centernos);  //rmcunumber // i did change key...
                map.put("imagefile",imgString.toString());
                map.put("isdeleted","active");
                map.put("creationvalue","centercreation");

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void setRMCUNumber(){
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.rmcumaster, null);
        if (mCursor.moveToFirst()) {
            do {
                rmcunos = mCursor.getString(mCursor.getColumnIndex("society_number"));
                Log.d("rmcunos",rmcunos);
            } while (mCursor.moveToNext());
        }
    }

    public void setCenterNumber(){
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor2.moveToFirst()) {
            do {
                centernos =  mCursor2.getString(mCursor2.getColumnIndex("center_number"));
                Log.d("centernos",centernos);
            } while (mCursor2.moveToNext());
        }
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());

        viewPagerAdapter.addFragment(new TabFarmerName1(), "Farmer Details");
        viewPagerAdapter.addFragment(new TabContactDetailes2(), "Contact Details");
        viewPagerAdapter.addFragment(new TabBankDetailes3(), "Bank Details");
        viewPagerAdapter.addFragment(new TabCattleDetailes4(), "Cattle Details");
        viewPagerAdapter.addFragment(new TabFarmerTempDisable5(), "Farmer Temporary Disable");
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(viewPagerAdapter);

        viewPager.beginFakeDrag();

    }

    private static class ViewPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        List<String> fragmentTitles = new ArrayList<>();

        ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            if (object instanceof ValidatedFragment) {
                ((ValidatedFragment) object).validate();
            }
            return super.getItemPosition(object); }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        void addFragment(Fragment fragment, String name) {
            fragmentList.add(fragment);
            fragmentTitles.add(name);
        }
    }
    private void setupToolbar() {
        toolbar.setTitle("Farmer Registration");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}

