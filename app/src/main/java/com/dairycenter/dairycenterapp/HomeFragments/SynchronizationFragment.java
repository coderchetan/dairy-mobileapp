package com.dairycenter.dairycenterapp.HomeFragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.dairycenter.dairycenterapp.CollectionHistoryListActivity;
import com.dairycenter.dairycenterapp.CollectionReportListActivity;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.FarmerReportListActivity;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.MainActivity;
import com.dairycenter.dairycenterapp.NoOfDaysMilkReportActivity;
import com.dairycenter.dairycenterapp.R;
import com.dairycenter.dairycenterapp.RateChartReportListActivity;
import com.dairycenter.dairycenterapp.TruckSheetReportActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class SynchronizationFragment extends Fragment {

    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    Button close,imports,export;
    DatabaseHelper db;
    String id1,id2,id3,id4,rate_chartid,centeridss;
    private ProgressBar loading;
    private ProgressDialog progressDialog;

    public static final String COLLECTION_DATA = "http://mcsoft.whitegoldtech.com:8888/api/collectionform";
    public static final String CLS_SHIFT_API = "http://mcsoft.whitegoldtech.com:8888/api/shiftform";
    public static final String FARMER_LIST_REGI = "http://mcsoft.whitegoldtech.com:8888/api/farmernewform";

    public static final String CENTER_MASTER = "http://mcsoft.whitegoldtech.com:8888/api/listcenterspecific/";
    public static final String FARMER_LIST = "http://mcsoft.whitegoldtech.com:8888/api/listfarmersspecific/";
    public static final String UNIT_MASTER = "http://mcsoft.whitegoldtech.com:8888/api/listunit";
    public static final String RMCU_MASTER = "http://mcsoft.whitegoldtech.com:8888/api/listsocietyspecific/";
    public static final String DEDUCTION_MASTER = "http://mcsoft.whitegoldtech.com:8888/api/listdeduction";
    public static final String TRANSACTION_SETTING = "http://mcsoft.whitegoldtech.com:8888/api/listsettingsspecific/"; //21
    public static final String LISTRATECHART = "http://mcsoft.whitegoldtech.com:8888/api/listratechart";
    public static final String ProductMaster = "http://mcsoft.whitegoldtech.com:8888/api/listproducts";
    public static final String LISTPURCHASE = "http://mcsoft.whitegoldtech.com:8888/api/listpurchasenewspecific/";
    public static final String BANKLIST = "http://mcsoft.whitegoldtech.com:8888/api/listbanks";
    public static final String shifttym = "http://mcsoft.whitegoldtech.com:8888/api/superadminlist";

    public static final String RateChartMatrix1 = "http://mcsoft.whitegoldtech.com:8888/api/listratechartmatrix1/";
    public static final String RateChartMatrix2 = "http://mcsoft.whitegoldtech.com:8888/api/listratechartmatrix1/";
    public static final String RateChartMatrix3 = "http://mcsoft.whitegoldtech.com:8888/api/listratechartmatrix1/";
    public static final String RateChartMatrix4 = "http://mcsoft.whitegoldtech.com:8888/api/listratechartmatrix1/";

    public static final String LISTRATECHART1 = " http://mcsoft.whitegoldtech.com:8888/api/listratechartidspecific/";
    public static final String LISTRATECHART2 = " http://mcsoft.whitegoldtech.com:8888/api/listratechartidspecific/";
    public static final String LISTRATECHART3 = " http://mcsoft.whitegoldtech.com:8888/api/listratechartidspecific/";
    public static final String LISTRATECHART4 = " http://mcsoft.whitegoldtech.com:8888/api/listratechartidspecific/";

    String st1,st2;

    VolleyError error;
    String response;

    String network;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_synchronization, container, false);

        toolbar     =     (Toolbar)view.findViewById(R.id.home_toolbar);
        close       =     (Button)view.findViewById(R.id.cls);
        imports     =     (Button)view.findViewById(R.id.imports);
        export      =     (Button)view.findViewById(R.id.export);
        loading     =     (ProgressBar)view.findViewById(R.id.loading);

        db = new DatabaseHelper(getActivity());
        db.getWritableDatabase();


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        loading.setVisibility(View.GONE);

        network = "";

        imports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db    =   new DatabaseHelper(getActivity().getApplicationContext());
                db.deleteSynchronizedata();

                SQLiteDatabase dataBase = db.getWritableDatabase();
                Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                            + DatabaseHelper.centeruser, null);
                    if (mCursor1.moveToFirst()) {
                        do {
                            centeridss = mCursor1.getString(mCursor1.getColumnIndex("centerid"));
                            SimpleDateFormat simpleDateFormats = new SimpleDateFormat("dd/MM/yyyy");
                            Date todays = new Date();
                            String currentdate = simpleDateFormats.format(todays);

                            Calendar calendarr    =    Calendar.getInstance();
                            SimpleDateFormat simpleDateFormatr = new SimpleDateFormat("HH:mm:ss");
                            String currenttime = simpleDateFormatr.format(calendarr.getTime());

                            db.adddownload_datadate(currentdate,currenttime);

                            getCenterMaster();
                            progressDialog.show();
                        } while (mCursor1.moveToNext());
                    }
                }
        });


        export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase dataBase = db.getWritableDatabase();
                Cursor mCursor = dataBase.rawQuery("SELECT * FROM collectionsynch", null);
                if (mCursor.moveToFirst()) {
                    do {
                        String   ids1               =   mCursor.getString(mCursor.getColumnIndex("id"));
                      String   reversedate          =   mCursor.getString(mCursor.getColumnIndex("reversedate"));
                      String   userCattle           =   mCursor.getString(mCursor.getColumnIndex("userCattles"));
                      String   shift_small          =   mCursor.getString(mCursor.getColumnIndex("shift_small"));
                      String   farmreNoCompltes     =   mCursor.getString(mCursor.getColumnIndex("farmreNoCompltes"));
                      String   farmer_name          =   mCursor.getString(mCursor.getColumnIndex("farmer_name"));
                      String   weightss             =   mCursor.getString(mCursor.getColumnIndex("weightss"));
                      String   fatsss               =   mCursor.getString(mCursor.getColumnIndex("fatsss"));
                      String   lactoo               =   mCursor.getString(mCursor.getColumnIndex("lactoo"));
                      String   snffss               =   mCursor.getString(mCursor.getColumnIndex("snffss"));
                      String   protiens             =   mCursor.getString(mCursor.getColumnIndex("protiens"));
                      String   waterss              =   mCursor.getString(mCursor.getColumnIndex("waterss"));
                      String   ratesss              =   mCursor.getString(mCursor.getColumnIndex("ratesss"));
                      String   amountss             =   mCursor.getString(mCursor.getColumnIndex("amountss"));

                      ragicollection(ids1,reversedate,userCattle,shift_small,farmreNoCompltes,farmer_name,weightss,fatsss,lactoo,snffss,
                                protiens,waterss,ratesss,amountss);

                    } while (mCursor.moveToNext());
                }

                Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM closeshiftsynch", null);
                if (mCursor2.moveToFirst()) {
                    do {
                        String   ids2                    =   mCursor2.getString(mCursor2.getColumnIndex("id"));
                        String   vehicle_nos            =   mCursor2.getString(mCursor2.getColumnIndex("vehicle_nos"));
                        String   driver_names           =   mCursor2.getString(mCursor2.getColumnIndex("driver_names"));
                        String   contact_nos            =   mCursor2.getString(mCursor2.getColumnIndex("contact_nos"));
                        String   shiftclose_dates       =   mCursor2.getString(mCursor2.getColumnIndex("shiftclose_dates"));
                        String   shiftclose_times       =   mCursor2.getString(mCursor2.getColumnIndex("shiftclose_times"));
                        String   shift_wt_cows          =   mCursor2.getString(mCursor2.getColumnIndex("shift_wt_cows"));
                        String   shift_wt_buffalos      =   mCursor2.getString(mCursor2.getColumnIndex("shift_wt_buffalos"));
                        String   shift_fat_cows         =   mCursor2.getString(mCursor2.getColumnIndex("shift_fat_cows"));
                        String   shift_fat_buffalos     =   mCursor2.getString(mCursor2.getColumnIndex("shift_fat_buffalos"));
                        String   shift_snf_cows         =   mCursor2.getString(mCursor2.getColumnIndex("shift_snf_cows"));
                        String   shift_snf_buffalos     =   mCursor2.getString(mCursor2.getColumnIndex("shift_snf_buffalos"));
                        String   shift_sum_cow_buffalos =   mCursor2.getString(mCursor2.getColumnIndex("shift_sum_cow_buffalos"));
                        String   shiftclose             =   mCursor2.getString(mCursor2.getColumnIndex("shiftclose"));
                        String   Shistcenter_number     =   mCursor2.getString(mCursor2.getColumnIndex("Shistcenter_number"));

                        ragiclose(ids2,vehicle_nos,driver_names,contact_nos,shiftclose_dates,shiftclose_times,shift_wt_cows,shift_wt_buffalos,
                                shift_fat_cows,shift_fat_buffalos,shift_snf_cows,shift_snf_buffalos,shift_sum_cow_buffalos,shiftclose,Shistcenter_number) ;

                    } while (mCursor2.moveToNext());
                }

                Cursor mCursor3 = dataBase.rawQuery("SELECT * FROM farmerregisynch", null);
                if (mCursor3.moveToFirst()) {
                    do {
                        String   ids3              =   mCursor3.getString(mCursor3.getColumnIndex("id"));
                        String   firstname        =   mCursor3.getString(mCursor3.getColumnIndex("firstname"));
                        String   lastname         =   mCursor3.getString(mCursor3.getColumnIndex("lastname"));
                        String   address          =   mCursor3.getString(mCursor3.getColumnIndex("address"));
                        String   contactNo        =   mCursor3.getString(mCursor3.getColumnIndex("contactNo"));
                        String   adhar_no         =   mCursor3.getString(mCursor3.getColumnIndex("adhar_no"));
                        String   city             =   mCursor3.getString(mCursor3.getColumnIndex("city"));
                        String   state            =   mCursor3.getString(mCursor3.getColumnIndex("state"));
                        String   pincode          =   mCursor3.getString(mCursor3.getColumnIndex("pincode"));
                        String   bankname         =   mCursor3.getString(mCursor3.getColumnIndex("bankname"));
                        String   branchname       =   mCursor3.getString(mCursor3.getColumnIndex("branchname"));
                        String   accountno        =   mCursor3.getString(mCursor3.getColumnIndex("accountno"));
                        String   ifsccode         =   mCursor3.getString(mCursor3.getColumnIndex("ifsccode"));
                        String   noofcow          =   mCursor3.getString(mCursor3.getColumnIndex("noofcow"));
                        String   noofbuffelo      =   mCursor3.getString(mCursor3.getColumnIndex("noofbuffelo"));
                        String   noofcattle       =   mCursor3.getString(mCursor3.getColumnIndex("noofcattle"));
                        String   centernos        =   mCursor3.getString(mCursor3.getColumnIndex("centernos"));
                        String   rmcunos          =   mCursor3.getString(mCursor3.getColumnIndex("rmcunos"));
                        String   imgString        =   mCursor3.getString(mCursor3.getColumnIndex("imgString"));
                        String   cattletype       =   mCursor3.getString(mCursor3.getColumnIndex("cattletype"));
                        String   rfid             =   mCursor3.getString(mCursor3.getColumnIndex("rfid"));

                        ragifarmer(ids3,firstname,lastname,address,contactNo,adhar_no,city,state,pincode,bankname,
                                   branchname,accountno,ifsccode,noofcow,noofbuffelo,noofcattle,centernos,rmcunos,
                                   imgString,cattletype,rfid);

                    } while (mCursor3.moveToNext());
                }

                ConnectivityManager connectivityManager = (ConnectivityManager) appCompatActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();



               if( mCursor2.getCount() == 0 && mCursor.getCount() == 0){
                   Toast.makeText(getActivity(),"Data not available",Toast.LENGTH_LONG).show();
               }else if(networkInfo == null && networkInfo.isConnected() == false){
                   Toast.makeText(getActivity(), "Network not available", Toast.LENGTH_LONG).show();
               } else {
                   progressDialog.show();
                   Runnable runnable = new Runnable() {
                       @Override
                       public void run() {
                           progressDialog.cancel();
                           Toast.makeText(getActivity(), "Export completed", Toast.LENGTH_LONG).show();
                       }
                   };
                   Handler handler = new Handler();
                   handler.postDelayed(runnable, 3000);
               }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });

        setupToolbar();
        return view;
    }

    private void getCenterMaster(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,CENTER_MASTER+"0"+centeridss, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                VolleyLog.d("ResponseCenter:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){
                        JSONObject data = jsonArray.getJSONObject(i);

                        String id                   =    data.getString("id");
                        String center_name          =    data.getString("center_name");
                        String center_name1         =    data.getString("center_name1");
                        String center_operator      =    data.getString("center_operator");
                        String center_operator1     =    data.getString("center_operator1");
                        String center_pin           =    data.getString("center_pin");
                        String bank_details         =    data.getString("bank_details");
                        String rate_chart           =    data.getString("rate_chart");
                        String status               =    data.getString("status");
                        String created_at           =    data.getString("created_at");
                        String updated_at           =    data.getString("updated_at");
                        String user_id              =    data.getString("user_id");
                        String bank_address         =    data.getString("bank_address");
                        String branch_name          =    data.getString("branch_name");
                        String ifsc_code            =    data.getString("ifsc_code");
                        String account_number       =    data.getString("account_number");
                        String center_number        =    data.getString("center_number");
                        String image_url            =    data.getString("image_url");


                        db.insertCenterMaster(id, center_name, center_name1, center_operator, center_operator1,
                                center_pin, bank_details, rate_chart, status, created_at, updated_at, user_id, bank_address,
                                branch_name, ifsc_code, account_number, center_number,image_url);
                            rate_chartid = rate_chart.toString();

                    }
                    farmermasterGet();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error:" + error.toString());

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest);
    }

    private void farmermasterGet(){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, FARMER_LIST+"0"+centeridss, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                 VolleyLog.d("ResponseFarmer:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id                =    data.getString("id");
                        String farmername        =    data.getString("farmername");
                        String farmer_number     =    data.getString("farmer_number");
                        String image             =    data.getString("image");
                        String app_image         =    data.getString("app_image");
                        String address           =    data.getString("address");
                        String mobilenumber      =    data.getString("mobilenumber");
                        String cattlevalue       =    data.getString("cattlevalue");
                        String dscno             =    data.getString("dscno");
                        String centernumber      =    data.getString("centernumber");
                        String rmcunumber        =    data.getString("rmcunumber");
                        String bankname          =    data.getString("bankname");
                        String branch            =    data.getString("branch");
                        String ifscvalue         =    data.getString("ifscvalue");
                        String accountnumber     =    data.getString("accountnumber");
                        String state             =    data.getString("state");
                        String pincode           =    data.getString("pincode");
                        String country           =    data.getString("country");
                        String aadharcardnumber  =    data.getString("aadharcardnumber");
                        String rfid              =    data.getString("rfid");


                        db.insertDataFarmer(id,farmername,farmer_number,image,app_image,address,mobilenumber,
                                cattlevalue,dscno,centernumber,rmcunumber,bankname,branch,ifscvalue,accountnumber,state,pincode,
                                country, aadharcardnumber,rfid);

                    }
                    getUnitMaster();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error1:" + error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest);
    }

    private void getUnitMaster(){
        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.GET, UNIT_MASTER, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id          =   data.getString("id");
                        String unitname    =   data.getString("unit_name");
                        String unitname1   =   data.getString("unit_name1");
                        String scalefactor =   data.getString("scale_factor");
                        String factorunit  =   data.getString("factor_unit");
                        String createdunit =   data.getString("created_at");

                        db.insertunitmaster(id,unitname,unitname1,scalefactor,factorunit,createdunit);
                    }
                    getBanklist();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error2:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest2);
    }

    private void getBanklist(){
        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.GET, BANKLIST, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){
                        JSONObject data = jsonArray.getJSONObject(i);

                        String bank_name    =   data.getString("bank_name");

//                        db.insertbanklist(bank_name);
                    }
                    getShifttime();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error3:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest2);
    }

    private void getShifttime(){
        JsonObjectRequest jsonObjectRequest2 = new JsonObjectRequest(Request.Method.GET, shifttym, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){
                        JSONObject data = jsonArray.getJSONObject(i);
                        String id                 =   data.getString("id");
                        String morningstarttime    =   data.getString("morningstarttime");
                        String morningendtime       =   data.getString("morningendtime");
                        String eveningstarttime    =   data.getString("eveningstarttime");
                        String eveningendtime    =   data.getString("eveningendtime");

                        db.insertshifttime(id,morningstarttime,morningendtime,eveningstarttime,eveningendtime);
                    }
                    getRmcumaster();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error3:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest2);
    }

    private void getRmcumaster(){
        JsonObjectRequest jsonObjectRequest3= new JsonObjectRequest(Request.Method.GET, RMCU_MASTER +"0"+rate_chartid, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseRmcu:" + response.toString());

                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =   data.getString("id");
                        String societyName     =   data.getString("society_name");
                        String societyName1    =   data.getString("society_name1");
                        String societyAddress  =   data.getString("society_address");
                        String pinCode         =   data.getString("pin_code");
                        String unionName       =   data.getString("union_name");
                        String mobileNumber    =   data.getString("mobile_number");
                        String bankDetails     =   data.getString("bank_details");
                        String status          =   data.getString("status");
                        String createdAt       =   data.getString("created_at");
                        String updatedAt       =   data.getString("updated_at");
                        String centername      =    data.getString("center_name");
                        String society_number  =    data.getString("society_number");


                            db.insertRMCUmaster(id,societyName,societyName1,societyAddress,pinCode,unionName,
                                    mobileNumber,bankDetails,status,createdAt,updatedAt,centername,society_number);

                    }
                    getTransactionSettings();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error4:" +error.toString());

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest3);
    }

    private void getTransactionSettings(){
        JsonObjectRequest jsonObjectRequest4= new JsonObjectRequest(Request.Method.GET, TRANSACTION_SETTING + centeridss, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseTransaction:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String rmcuname       = data.getString("rmcuname");
                        String centername     = data.getString("centername");
                        String scale          = data.getString("scale");
                        String analyzer       = data.getString("analyzer");
                        String cowmorning     = data.getString("cowmorning");
                        String buffelomorning = data.getString("buffelomorning");
                        String cowevening     = data.getString("cowevening");
                        String buffeloevening = data.getString("buffeloevening");
                        String printingvalue  = data.getString("printingvalue");
                        String status         = data.getString("status");
                        String settingvalue   = data.getString("settingvalue");
                        String headermessage  = data.getString("headermessage");
                        String footermessage  = data.getString("footermessage");
                        String snfformula     = data.getString("snfformula");
                        String littervalue    = data.getString("littervalue");
                        String idss           = data.getString("id");

                        db.insertTransactionSetting(rmcuname, centername, scale, analyzer, cowmorning, buffelomorning, cowevening,
                                buffeloevening, printingvalue, status, settingvalue, headermessage, footermessage, snfformula,
                                littervalue,idss);
                    }

                    getSelectedSettingData();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error5:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest4);
    }

    public void getSelectedSettingData() {
        db = new DatabaseHelper(getActivity());
        SQLiteDatabase dataBase3 = db.getWritableDatabase();
        Cursor mCursor3 = dataBase3.rawQuery("SELECT * FROM "
                + DatabaseHelper.centeruser, null);
        if (mCursor3.moveToFirst()) {
            do {
                String centerid = mCursor3.getString(mCursor3.getColumnIndex("centerid"));

                SQLiteDatabase dataBase = db.getWritableDatabase();
                Cursor mCursor = dataBase.rawQuery("select * from listTransactionSetting where centername = ?",
                        new String[]{String.valueOf(centerid)}, null);
                if (mCursor.moveToFirst()) {
                    do {
                        String id = mCursor.getString(mCursor.getColumnIndex("idss"));
                        String weighting_scale = mCursor.getString(mCursor.getColumnIndex("scale"));
                        String analyzer_scale = mCursor.getString(mCursor.getColumnIndex("analyzer"));
                        String cowmorning = mCursor.getString(mCursor.getColumnIndex("cowmorning"));
                        String buffelomorning = mCursor.getString(mCursor.getColumnIndex("buffelomorning"));
                        String cowevening = mCursor.getString(mCursor.getColumnIndex("cowevening"));
                        String buffeloevening = mCursor.getString(mCursor.getColumnIndex("buffeloevening"));
                        String littervalue = mCursor.getString(mCursor.getColumnIndex("littervalue"));

                        id1 = cowmorning;
                        id2 = buffelomorning;
                        id3 = cowevening;
                        id4 = buffeloevening;

                        Log.d("1", "1");
                        db.insertdownloadsettingdata(id,weighting_scale,analyzer_scale,cowmorning,buffelomorning,cowevening,buffeloevening,littervalue);
                        if(weighting_scale.equals("auto")){
                            st1 = "Auto";
                        }
                        if(analyzer_scale.equals("auto")){
                            st2 = "Auto";
                        }
                        if(weighting_scale.equals("manual")){
                            st1 = "Manual";
                        }
                        if(analyzer_scale.equals("manual")){
                            st2 = "Manual";
                        }

                        db.insetcollectionmode(st1, st2);

                    } while (mCursor.moveToNext());
                } else if (mCursor.getCount() == 0) {
                    SQLiteDatabase dataBase2 = db.getWritableDatabase();
                    Cursor mCursor2 = dataBase2.rawQuery("SELECT * FROM "
                            + DatabaseHelper.listTransactionSetting, null);
                    if (mCursor2.moveToFirst()) {
                        do {
                            if (mCursor2.getString(mCursor2.getColumnIndex("centername")).equals("all")) {
                                String id = mCursor2.getString(mCursor2.getColumnIndex("idss"));
                                String weighting_scale = mCursor2.getString(mCursor2.getColumnIndex("scale"));
                                String analyzer_scale = mCursor2.getString(mCursor2.getColumnIndex("analyzer"));
                                String cowmorning = mCursor2.getString(mCursor2.getColumnIndex("cowmorning"));
                                String buffelomorning = mCursor2.getString(mCursor2.getColumnIndex("buffelomorning"));
                                String cowevening = mCursor2.getString(mCursor2.getColumnIndex("cowevening"));
                                String buffeloevening = mCursor2.getString(mCursor2.getColumnIndex("buffeloevening"));
                                String littervalue = mCursor2.getString(mCursor2.getColumnIndex("littervalue"));

                                db.insertdownloadsettingdata(id,weighting_scale,analyzer_scale,cowmorning,buffelomorning,cowevening,buffeloevening,littervalue);
                                Log.d("2", "2");

                                if(weighting_scale.equals("auto")){
                                    st1 = "Auto";
                                }
                                if(analyzer_scale.equals("auto")){
                                    st2 = "Auto";
                                }
                                if(weighting_scale.equals("manual")){
                                    st1 = "Manual";
                                }
                                if(analyzer_scale.equals("manual")){
                                    st2 = "Manual";
                                }

                            }
                        } while (mCursor2.moveToNext());
                    }
                }
            } while (mCursor3.moveToNext());
        }

        getListRatechart1();
    }

    private void getListRatechart1(){
        JsonObjectRequest jsonObjectRequest5= new JsonObjectRequest(Request.Method.GET, LISTRATECHART1+id1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseListccccccccccccccc:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id_R             =    data.getString("id");
                        String chartName        =    data.getString("chartname");
                        String shift            =    data.getString("shift");
                        String ratechart        =    data.getString("ratechart");
                        String cattletype       =    data.getString("cattletype");
                        String charttype        =    data.getString("charttype");
                        String range            =    data.getString("range");
                        String fixedrate        =    data.getString("fixedrate");
                        String ratedate         =    data.getString("ratedate");
                        String status           =    data.getString("status");
                        String created_at       =    data.getString("created_at");
                        String updated_at       =    data.getString("updated_at");
                        String fatrange_from    =    data.getString("fatrange_from");
                        String fatrange_to      =    data.getString("fatrange_to");
                        String fatsnfrange_from =    data.getString("fatsnfrange_from");
                        String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                        String effective_date   =    data.getString("effective_date");
                        String expiration_date  =    data.getString("expiration_date");
                        String fat_value        =    data.getString("fat_value");
                        String snf_value        =    data.getString("snf_value");


                        db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                effective_date,expiration_date,fat_value,snf_value);

                    }

                    getListRatechart2();
                }catch ( JSONException e){
                    e.printStackTrace();
//                    dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error6:" +error.toString());
//                dismissDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest5);
    }

    private void getListRatechart2(){
        JsonObjectRequest jsonObjectRequest5= new JsonObjectRequest(Request.Method.GET, LISTRATECHART2+id2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseListccccccccccccccc:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id_R             =    data.getString("id");
                        String chartName        =    data.getString("chartname");
                        String shift            =    data.getString("shift");
                        String ratechart        =    data.getString("ratechart");
                        String cattletype       =    data.getString("cattletype");
                        String charttype        =    data.getString("charttype");
                        String range            =    data.getString("range");
                        String fixedrate        =    data.getString("fixedrate");
                        String ratedate         =    data.getString("ratedate");
                        String status           =    data.getString("status");
                        String created_at       =    data.getString("created_at");
                        String updated_at       =    data.getString("updated_at");
                        String fatrange_from    =    data.getString("fatrange_from");
                        String fatrange_to      =    data.getString("fatrange_to");
                        String fatsnfrange_from =    data.getString("fatsnfrange_from");
                        String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                        String effective_date   =    data.getString("effective_date");
                        String expiration_date  =    data.getString("expiration_date");
                        String fat_value        =    data.getString("fat_value");
                        String snf_value        =    data.getString("snf_value");


                        db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                effective_date,expiration_date,fat_value,snf_value);

                    }

                    getListRatechart3();
                }catch ( JSONException e){
                    e.printStackTrace();
//                    dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error6:" +error.toString());
//                dismissDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest5);
    }

    private void getListRatechart3(){
        JsonObjectRequest jsonObjectRequest5= new JsonObjectRequest(Request.Method.GET, LISTRATECHART3+id3, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseListccccccccccccccc:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id_R             =    data.getString("id");
                        String chartName        =    data.getString("chartname");
                        String shift            =    data.getString("shift");
                        String ratechart        =    data.getString("ratechart");
                        String cattletype       =    data.getString("cattletype");
                        String charttype        =    data.getString("charttype");
                        String range            =    data.getString("range");
                        String fixedrate        =    data.getString("fixedrate");
                        String ratedate         =    data.getString("ratedate");
                        String status           =    data.getString("status");
                        String created_at       =    data.getString("created_at");
                        String updated_at       =    data.getString("updated_at");
                        String fatrange_from    =    data.getString("fatrange_from");
                        String fatrange_to      =    data.getString("fatrange_to");
                        String fatsnfrange_from =    data.getString("fatsnfrange_from");
                        String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                        String effective_date   =    data.getString("effective_date");
                        String expiration_date  =    data.getString("expiration_date");
                        String fat_value        =    data.getString("fat_value");
                        String snf_value        =    data.getString("snf_value");


                        db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                effective_date,expiration_date,fat_value,snf_value);

                    }
                    getListRatechart4();
                }catch ( JSONException e){
                    e.printStackTrace();
//                    dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error6:" +error.toString());
//                dismissDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest5);
    }
    private void getListRatechart4(){
        JsonObjectRequest jsonObjectRequest5= new JsonObjectRequest(Request.Method.GET, LISTRATECHART4+id4, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponseListccccccccccccccc:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id_R             =    data.getString("id");
                        String chartName        =    data.getString("chartname");
                        String shift            =    data.getString("shift");
                        String ratechart        =    data.getString("ratechart");
                        String cattletype       =    data.getString("cattletype");
                        String charttype        =    data.getString("charttype");
                        String range            =    data.getString("range");
                        String fixedrate        =    data.getString("fixedrate");
                        String ratedate         =    data.getString("ratedate");
                        String status           =    data.getString("status");
                        String created_at       =    data.getString("created_at");
                        String updated_at       =    data.getString("updated_at");
                        String fatrange_from    =    data.getString("fatrange_from");
                        String fatrange_to      =    data.getString("fatrange_to");
                        String fatsnfrange_from =    data.getString("fatsnfrange_from");
                        String fatsnfrange_to   =    data.getString("fatsnfrange_to");
                        String effective_date   =    data.getString("effective_date");
                        String expiration_date  =    data.getString("expiration_date");
                        String fat_value        =    data.getString("fat_value");
                        String snf_value        =    data.getString("snf_value");


                        db.insertListRatechart(id_R,chartName,shift,ratechart,cattletype,charttype,range,fixedrate,ratedate,
                                status,created_at,updated_at,fatrange_from,fatrange_to,fatsnfrange_from,fatsnfrange_to,
                                effective_date,expiration_date,fat_value,snf_value);

                    }

                    geRatechartMatrix1();
                }catch ( JSONException e){
                    e.printStackTrace();
//                    dismissDialog();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error6:" +error.toString());
//                dismissDialog();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest5);
    }

    private void geRatechartMatrix1(){
        JsonObjectRequest jsonObjectRequestid1= new JsonObjectRequest(Request.Method.GET, RateChartMatrix1 +id1, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponsechartMatrix:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =    data.getString("id");
                        String ratechart_id    =    data.getString("ratechart_id");
                        String fat_range       =    data.getString("fat_range");
                        String snf_lacto_range =    data.getString("snf_lacto_range");
                        String rate_value      =    data.getString("rate_value");
                        String status          =    data.getString("status");
                        String created_at      =    data.getString("created_at");
                        String updated_at      =    data.getString("updated_at");
                        String fatnewrange     =    data.getString("fatnewrange");


                        db.insertRatechartMatrix(id, ratechart_id, fat_range, snf_lacto_range, rate_value, status, created_at,
                                updated_at, fatnewrange);

                    }
                    geRatechartMatrix2();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Errorid1:" +error.toString());
            }
        });

        jsonObjectRequestid1.setShouldCache(false);
        jsonObjectRequestid1.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequestid1);
    }

    private void geRatechartMatrix2(){
        JsonObjectRequest jsonObjectRequestid2= new JsonObjectRequest(Request.Method.GET, RateChartMatrix2+ id2, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponsechartMatrix:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =    data.getString("id");
                        String ratechart_id    =    data.getString("ratechart_id");
                        String fat_range       =    data.getString("fat_range");
                        String snf_lacto_range =    data.getString("snf_lacto_range");
                        String rate_value      =    data.getString("rate_value");
                        String status          =    data.getString("status");
                        String created_at      =    data.getString("created_at");
                        String updated_at      =    data.getString("updated_at");
                        String fatnewrange     =    data.getString("fatnewrange");

                        db.insertRatechartMatrix(id, ratechart_id, fat_range, snf_lacto_range, rate_value, status, created_at, updated_at, fatnewrange);

                    }
                    geRatechartMatrix3();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Errorid2:" +error.toString());
            }
        });

        jsonObjectRequestid2.setShouldCache(false);
        jsonObjectRequestid2.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequestid2);
    }

    private void geRatechartMatrix3(){
        JsonObjectRequest jsonObjectRequestid3= new JsonObjectRequest(Request.Method.GET, RateChartMatrix3+id3, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponsechartMatrix:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =    data.getString("id");
                        String ratechart_id    =    data.getString("ratechart_id");
                        String fat_range       =    data.getString("fat_range");
                        String snf_lacto_range =    data.getString("snf_lacto_range");
                        String rate_value      =    data.getString("rate_value");
                        String status          =    data.getString("status");
                        String created_at      =    data.getString("created_at");
                        String updated_at      =    data.getString("updated_at");
                        String fatnewrange     =    data.getString("fatnewrange");

                        db.insertRatechartMatrix(id, ratechart_id, fat_range, snf_lacto_range, rate_value, status, created_at,
                                updated_at, fatnewrange);

                    }
                    geRatechartMatrix4();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Errorid3:" +error.toString());
            }
        });

        jsonObjectRequestid3.setShouldCache(false);
        jsonObjectRequestid3.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequestid3);
    }

    private void geRatechartMatrix4(){
        JsonObjectRequest jsonObjectRequestid4= new JsonObjectRequest(Request.Method.GET, RateChartMatrix4 +id4, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("ResponsechartMatrix:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =    data.getString("id");
                        String ratechart_id    =    data.getString("ratechart_id");
                        String fat_range       =    data.getString("fat_range");
                        String snf_lacto_range =    data.getString("snf_lacto_range");
                        String rate_value      =    data.getString("rate_value");
                        String status          =    data.getString("status");
                        String created_at      =    data.getString("created_at");
                        String updated_at      =    data.getString("updated_at");
                        String fatnewrange     =    data.getString("fatnewrange");


                        db.insertRatechartMatrix(id, ratechart_id, fat_range, snf_lacto_range, rate_value, status, created_at,
                                updated_at, fatnewrange);

                    }
                    getProductName();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Errorid4:" +error.toString());
            }
        });

        jsonObjectRequestid4.setShouldCache(false);
        jsonObjectRequestid4.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequestid4);
    }

    private void getProductName(){
        JsonObjectRequest jsonObjectRequest6= new JsonObjectRequest(Request.Method.GET, ProductMaster, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id                  =    data.getString("id");
                        String product_name        =    data.getString("product_name");
                        String product_name1       =    data.getString("product_name1");
                        String record_list         =    data.getString("record_list");
                        String manufacture_details =    data.getString("manufacture_details");
                        String unit_scale          =    data.getString("unit_scale");
                        String subsidy_percentage  =    data.getString("subsidy_percentage");
                        String created_at          =    data.getString("created_at");
                        String updated_at          =    data.getString("updated_at");
                        String status              =    data.getString("status");

                        db.insertProductMaster(id,product_name,product_name1,record_list,manufacture_details,
                                unit_scale,subsidy_percentage,created_at,updated_at,status);
                    }
                    getListPurchase();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error8:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest6);
    }

    private void getListPurchase(){
        JsonObjectRequest jsonObjectRequest6= new JsonObjectRequest(Request.Method.GET, LISTPURCHASE+centeridss, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                VolleyLog.d("Responselistpurchase:" + response.toString());
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id               =    data.getString("id");
                        String purchaseid       =    data.getString("purchaseid");
                        String productname      =    data.getString("productname");
                        String productunit      =    data.getString("productunit");
                        String productquantity  =    data.getString("productquantity");
                        String productnumber    =    data.getString("productnumber");
                        String flag             =    data.getString("flag");
                        String allocatedstock   =    data.getString("allocatedstock");
                        String sale_rate        =    data.getString("salerate");
                        String center_id        =    data.getString("center_id");
                        String created_at       =    data.getString("created_at");


                            db.insertPurchaseList(id, purchaseid, productname, productunit, productquantity,
                                    productnumber, flag, allocatedstock, sale_rate,created_at);

                    }
                    getDeductionMaster();
                }catch ( JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error9:" +error.toString());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest6);
    }

    private void getDeductionMaster(){
        JsonObjectRequest jsonObjectRequest7= new JsonObjectRequest(Request.Method.GET, DEDUCTION_MASTER, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    JSONArray jsonArray= response.getJSONArray("data");
                    for(int i= 0; i< jsonArray.length();i++){

                        JSONObject data = jsonArray.getJSONObject(i);

                        String id              =   data.getString("id");
                        String deductionName   =   data.getString("deduction_name");
                        String deductionName1  =   data.getString("deduction_name1");
                        String deductionType   =   data.getString("deduction_type");
                        String deductionCycle  =   data.getString("deduction_cycle");
                        String activationPeriod=   data.getString("activation_period");
                        String deductionMethod =   data.getString("deduction_method");
                        String deductionActive =   data.getString("deduction_active");
                        String createdAt       =   data.getString("created_at");
                        String updatedAt       =   data.getString("updated_at");

                        db.insertdeductionmaster(id,deductionName,deductionName1,deductionType,deductionCycle,activationPeriod,deductionMethod,deductionActive,createdAt,updatedAt);

                    }
//                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                    progressDialog.dismiss();
//                    finish();
//                    intent.putExtra("demo","Alert");
//                    startActivity(intent);
                    Toast.makeText(getActivity(),"Import completed",Toast.LENGTH_LONG).show();

                }catch ( JSONException e){
                    e.printStackTrace();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error10:" +error.toString());

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(jsonObjectRequest7);

    }

    private void ragicollection(final String ids1,final String reversedate,final String userCattle,final String shift_small,final String farmreNoCompltes,
                               final String farmer_name,final String weightss,final String fatsss,final String lactoo,final String snffss,
                       final String protiens,final String waterss,final String ratesss,final String amountss) {

        StringRequest stringRequest1 = new StringRequest(Request.Method.POST, COLLECTION_DATA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePostcollection:" + response.toString());
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
                        db.deletefarmerid1(ids1);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error1:" + error.toString());
//                Toast.makeText(getActivity(), "Network connection Failed, please try again.", Toast.LENGTH_LONG).show();
                network = "network";
            }
        })
        {@Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("date",reversedate );
                map.put("cattletype", userCattle);
                map.put("shift", shift_small);
                map.put("farmernumber",farmreNoCompltes);
                map.put("farmername",farmer_name);
                map.put("weight",weightss);
                map.put("fat",fatsss);
                map.put("lacto",lactoo);
                map.put("snf",snffss);
                map.put("protein",protiens);
                map.put("water",waterss);
                map.put("rate",ratesss);
                map.put("amount",amountss);
                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest1);
    }



    private void ragiclose(final String ids2,final String vehicle_nos, final String driver_names, final String contact_nos, final String shiftclose_dates,
                       final String shiftclose_times, final String shift_wt_cows, final String shift_wt_buffalos,
                       final String shift_fat_cows, final String shift_fat_buffalos, final String shift_snf_cows,
                       final String shift_snf_buffalos, final String shift_sum_cow_buffalos, final String shiftclose,final String Shistcenter_number) {

        StringRequest stringRequest2 = new StringRequest(Request.Method.POST, CLS_SHIFT_API, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                VolleyLog.d("ResponcePostclose:" + response.toString());
                db.deletefarmerid2(ids2);
//                        Toast.makeText(getActivity().getApplicationContext(), response, Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error2:" + error.toString());
//                Toast.makeText(getActivity(), "Network connection Failed, please try again.", Toast.LENGTH_LONG).show();
                network = "network";
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("vehicle_number",vehicle_nos );
                map.put("driver_name", driver_names);
                map.put("driver_contact", contact_nos);
                map.put("date",shiftclose_dates);
                map.put("time",shiftclose_times);
                map.put("total_weight_cow",shift_wt_cows);
                map.put("total_weight_buffalo",shift_wt_buffalos);
                map.put("avgfat_cow",shift_fat_cows);
                map.put("avgfat_buffalo",shift_fat_buffalos);
                map.put("avgsnf_cow",shift_snf_cows);
                map.put("avgsnf_buffalo",shift_snf_buffalos);
                map.put("total",shift_sum_cow_buffalos);
                map.put("shift",shiftclose);
                map.put("center_number",Shistcenter_number);

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest2);
    }


    private void ragifarmer(final String ids3,final String firstname, final String lastname,final String address,final String contactNo,
                      final String adhar_no,final String city,final String state,final String pincode,
                      final String bankname,final String branchname,final String accountno,final String ifsccode,
                      final String noofcow,final String noofbuffelo,final String noofcattle,final String centernos,
                      final String rmcunos,final String imgString,final String cattletype,final String rfid) {

        StringRequest stringRequest3 = new StringRequest(Request.Method.POST, FARMER_LIST_REGI,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        VolleyLog.d("ResponcePostFarmer:" + response.toString());
                        db.deletefarmerid(ids3);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Error3:" + error.toString());
//                network = "network";
            }
        })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> map = new HashMap<>();
                map.put("farmername", (firstname + " " + lastname));
                map.put("address", address);
                map.put("rfid", rfid);
                map.put("mobilenumber", contactNo);
                map.put("aadharcardnumber",adhar_no);
                map.put("district",city);
                map.put("state",state);
                map.put("pincode",pincode);
                map.put("bankname",bankname);
                map.put("branch",branchname);
                map.put("accountnumber",accountno);
                map.put("ifscvalue",ifsccode);
                map.put("totalcows",noofcow);
                map.put("totalbuffaloes",noofbuffelo);
                map.put("cattleshedvalue",noofcattle);
                map.put("cattlevalue",cattletype);
                map.put("centernumber",rmcunos);  //centernumber
                map.put("rmcunumber",centernos);  //rmcunumber
                map.put("imagefile",imgString.toString());
                map.put("isdeleted","active");
                map.put("creationvalue","centercreation");

                return map;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getContext().getApplicationContext());
        requestQueue.add(stringRequest3);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    private void setupToolbar() {
        toolbar.setTitle("Synchronization");
        appCompatActivity.setSupportActionBar(toolbar);
    }
}
