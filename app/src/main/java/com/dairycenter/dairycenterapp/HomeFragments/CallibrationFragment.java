package com.dairycenter.dairycenterapp.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeActivity;
import com.dairycenter.dairycenterapp.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CallibrationFragment extends Fragment {
    private HomeActivity appCompatActivity;
    private Toolbar toolbar;
    TextView time,date;
    DatabaseHelper db;
    TextView rmcuname,centername;
    String rmcu_name,center_name,rmcu_id,center_id;
    Button save,cancel,close;
    EditText caliremark,personname;


    public CallibrationFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        appCompatActivity = (HomeActivity) context;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appCompatActivity.setupNavigationDrawer(toolbar);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_callibration, container, false);
        toolbar          = (Toolbar) view.findViewById(R.id.home_toolbar);

        time                = (TextView) view.findViewById(R.id.tym);
        date                = (TextView) view.findViewById(R.id.date);
        centername          = (TextView) view.findViewById(R.id.center_name);
        rmcuname            = (TextView) view.findViewById(R.id.rmcu_name);
        save                = (Button) view.findViewById(R.id.save);
        close               = (Button) view.findViewById(R.id.cls);
        cancel              = (Button) view.findViewById(R.id.cancel);
        caliremark          = (EditText) view.findViewById(R.id.cali_analy);
        personname          = (EditText) view.findViewById(R.id.person_name);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        time.setText(""+simpleDateFormat.format(calendar.getTime()));

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        String currentdate = simpleDateFormat2.format(today);
        date.setText(currentdate);

        db = new DatabaseHelper(getActivity().getApplicationContext());


        setCenterName();
        setRMCUName();
        setupToolbar();


        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  times         = time.getText().toString();
                String  dates        = date.getText().toString();
                String  centernames  = centername.getText().toString();
                String  rmcunames    = rmcuname.getText().toString();
                String  caliremarks  = caliremark.getText().toString();
                String  person_names =  personname.getText().toString();

                if (caliremarks.trim().isEmpty()) {
                    caliremark.setError("Please select calibration remark");
                    caliremark.requestFocus();
                } else if (person_names.trim().isEmpty()) {
                    personname.setError("Please select person name");
                    personname.requestFocus();
                }else {
                    db.insertcallibrationreport(null,times, dates, centernames, rmcunames, caliremarks, person_names);
                    Toast.makeText(getActivity().getApplicationContext(),"Save successfully",Toast.LENGTH_LONG).show();
                    cleardata();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personname.setText("");
                caliremark.setText("");
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity().getApplicationContext(), HomeActivity.class);
                startActivity(i);
            }
        });



        return view;
    }

    public void cleardata(){
        personname.setText("");
        caliremark.setText("");
    }

    public void setRMCUName() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.rmcumaster, null);
        if (mCursor.moveToFirst()) {
            do {
                rmcu_name = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.rmcuname));
                rmcu_id = mCursor.getString(mCursor.getColumnIndex("id"));
            } while (mCursor.moveToNext());

        }
        rmcuname.setText(rmcu_name);
    }

    public void setCenterName() {
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.CENTER_MASTER, null);
        if (mCursor2.moveToFirst()) {
            do {
                center_name = mCursor2.getString(mCursor2.getColumnIndex(DatabaseHelper.cenetrname));
                center_id = mCursor2.getString(mCursor2.getColumnIndex("id"));
            } while (mCursor2.moveToNext());
        }
        centername.setText(center_name);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void setupToolbar() {
        toolbar.setTitle("Calibration Report");
        appCompatActivity.setSupportActionBar(toolbar);
    }

}