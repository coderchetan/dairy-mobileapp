package com.dairycenter.dairycenterapp;

public class TruckSheetReport {
    String vehiNo,Drivername,drivercon_no,date,time,shift,totalwt_cow,totalwt_buffalo,avgfat_cow,avgfat_buffalo,avgsnf_cow,avgsnf_buffalo,totalwt;


    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getVehiNo() {
        return vehiNo;
    }

    public void setVehiNo(String vehiNo) {
        this.vehiNo = vehiNo;
    }

    public String getDrivername() {
        return Drivername;
    }

    public void setDrivername(String drivername) {
        Drivername = drivername;
    }

    public String getDrivercon_no() {
        return drivercon_no;
    }

    public void setDrivercon_no(String drivercon_no) {
        this.drivercon_no = drivercon_no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTotalwt_cow() {
        return totalwt_cow;
    }

    public void setTotalwt_cow(String totalwt_cow) {
        this.totalwt_cow = totalwt_cow;
    }

    public String getTotalwt_buffalo() {
        return totalwt_buffalo;
    }

    public void setTotalwt_buffalo(String totalwt_buffalo) {
        this.totalwt_buffalo = totalwt_buffalo;
    }

    public String getAvgfat_cow() {
        return avgfat_cow;
    }

    public void setAvgfat_cow(String avgfat_cow) {
        this.avgfat_cow = avgfat_cow;
    }

    public String getAvgfat_buffalo() {
        return avgfat_buffalo;
    }

    public void setAvgfat_buffalo(String avgfat_buffalo) {
        this.avgfat_buffalo = avgfat_buffalo;
    }

    public String getAvgsnf_cow() {
        return avgsnf_cow;
    }

    public void setAvgsnf_cow(String avgsnf_cow) {
        this.avgsnf_cow = avgsnf_cow;
    }

    public String getAvgsnf_buffalo() {
        return avgsnf_buffalo;
    }

    public void setAvgsnf_buffalo(String avgsnf_buffalo) {
        this.avgsnf_buffalo = avgsnf_buffalo;
    }

    public String getTotalwt() {
        return totalwt;
    }

    public void setTotalwt(String totalwt) {
        this.totalwt = totalwt;
    }
}
