package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.dairycenter.dairycenterapp.Adapter.FarmerListAdapter;
import com.dairycenter.dairycenterapp.Adapter.ProductListAdapter;

import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProductListAdapter adapter;
    DatabaseHelper db;
    List<ProductList> list = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        getSupportActionBar().setTitle("Product List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = new DatabaseHelper(ProductListActivity.this);
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.ViewPurchase, null);
        if (mCursor.moveToFirst()) {
            do {

              String  Productname =  mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.ViewProno));
              String  Productnumber = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.ViewProname));
              String  Productunit = mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.Viewproduct));
              String  Quantity =     mCursor.getString(mCursor.getColumnIndex(DatabaseHelper.Viewquan));


                ProductList productList = new ProductList();

                productList.setProductname(Productname);
                productList.setProdct_number(Productnumber);
                productList.setProdct_unit(Productunit);
                productList.setQuantity(Quantity);


                list.add(productList);
                adapter = new ProductListAdapter(ProductListActivity.this, list);
                recyclerView.setAdapter(adapter);

            } while (mCursor.moveToNext());

        }
        mCursor.close();
    }

    @Override
     public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
        case android.R.id.home:
        onBackPressed();
        return true;
        }
        return true;
        }
}









