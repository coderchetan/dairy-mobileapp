package com.dairycenter.dairycenterapp.FarmerRegistrationTab;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.HomeFragments.ValidatedFragment;
import com.dairycenter.dairycenterapp.R;

import java.util.List;

public class TabBankDetailes3 extends Fragment implements ValidatedFragment {

    public static EditText branch_bank, no_account, code_ifsc;
    public static String demo;
    public static Spinner name_bank;
    DatabaseHelper db;

    public TabBankDetailes3() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_bank_detailes3, container, false);
        Log.e("tab3", "tab3");
        name_bank       =   (Spinner) view.findViewById(R.id.abc);
        branch_bank     =   (EditText) view.findViewById(R.id.bran);
        no_account      =   (EditText) view.findViewById(R.id.account);
        code_ifsc       =   (EditText) view.findViewById(R.id.ifsc);

        db = new DatabaseHelper(getActivity());
        db.getWritableDatabase();

        List<String> list = db.getAllbanklist();
        list.add(0, "Select");

        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        name_bank.setAdapter(dataAdapter);
        name_bank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String typeofdeductions = parent.getItemAtPosition(position).toString();
//                    rought.setText(typeofdeductions);
                if (position == 0) {
                    if (parent.getChildAt(0) != null && parent.getChildAt(0) instanceof TextView) {
                        ((TextView) parent.getChildAt(0)).setTextColor(R.color.bordercolor);
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ClearText3();
        return view;
    }

    public void ClearText3() {
        name_bank.setSelection(0);;
        branch_bank.setText("");
        no_account.setText("");
        code_ifsc.setText("");
    }

    @Override
    public void validate() {
        Log.d("logged","hello");

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            String  address            =     TabContactDetailes2.Address.getText().toString();
            String  contactNo          =     TabContactDetailes2.contact_no.getText().toString();
            String  adhar_no           =     TabContactDetailes2.adhar_no.getText().toString();
            String  pincode            =     TabContactDetailes2.pin_code.getText().toString();

            if (address.trim().isEmpty()) {
                TabContactDetailes2.Address.setError("address not entered");
                TabContactDetailes2.Address.requestFocus();
                demo = "Demo";
            } else if (contactNo.trim().isEmpty()) {
                TabContactDetailes2.contact_no.setError("Please enter mobile number");
                TabContactDetailes2.contact_no.requestFocus();
                demo = "Demo";
            }else if (contactNo.trim().length() < 10) {
                TabContactDetailes2.contact_no.setError("Enter 10 digit mobile number");
                TabContactDetailes2.contact_no.requestFocus();
                demo = "Demo";
            }else if (contactNo.trim().length() > 10) {
                TabContactDetailes2.contact_no.setError("Enter 10 digit mobile number");
                TabContactDetailes2.contact_no.requestFocus();
                demo = "Demo";
            }else if (adhar_no.trim().isEmpty()) {
                TabContactDetailes2.adhar_no.setError("Please enter adhar number");
                TabContactDetailes2.adhar_no.requestFocus();
                demo = "Demo";
            }else if (adhar_no.trim().length() < 12) {
                TabContactDetailes2.adhar_no.setError("Enter a twelve digit adhar number");
                TabContactDetailes2.adhar_no.requestFocus();
                demo = "Demo";
            }else if (adhar_no.trim().length() > 12) {
                TabContactDetailes2.adhar_no.setError("Enter a twelve digit adhar number");
                TabContactDetailes2.adhar_no.requestFocus();
                demo = "Demo";
            }else if (pincode.trim().isEmpty()) {
                TabContactDetailes2.pin_code.setError("Please enter pin code");
                TabContactDetailes2.pin_code.requestFocus();
                demo = "Demo";
            }else if(pincode.trim().length() < 6) {
                TabContactDetailes2.pin_code.setError("Enter a six digit pin code");
                TabContactDetailes2.pin_code.requestFocus();
                demo = "Demo";
            } else if(pincode.trim().length() > 6) {
                TabContactDetailes2.pin_code.setError("Enter a six digit pin code");
                TabContactDetailes2.pin_code.requestFocus();
                demo = "Demo";
            } else {
                demo = "Valid1";
            }
        }
    }

    public static String getstring(){
        return demo;
    }
}















