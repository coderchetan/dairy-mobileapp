package com.dairycenter.dairycenterapp.FarmerRegistrationTab;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.dairycenter.dairycenterapp.HomeFragments.ValidatedFragment;
import com.dairycenter.dairycenterapp.R;


public class TabContactDetailes2 extends Fragment implements ValidatedFragment {

    public static EditText Address,contact_no,adhar_no,city,state,pin_code;
    public static String address,contactNo,demo;

    @Override
    public void onAttachFragment(@NonNull Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_contact_detailes2, container, false);
        Log.e("tab2", "tab2");

        Address       =    (EditText) view.findViewById(R.id.address);
        contact_no    =    (EditText) view.findViewById(R.id.contactno);
        adhar_no      =    (EditText) view.findViewById(R.id.adharno);
        city          =    (EditText) view.findViewById(R.id.city);
        state         =    (EditText) view.findViewById(R.id.state);
        pin_code      =    (EditText) view.findViewById(R.id.pincode);

        address            =     TabContactDetailes2.Address.getText().toString();
        contactNo          =     TabContactDetailes2.contact_no.getText().toString();

        ClearText2();
        return view;
    }

    public void ClearText2() {
        Address.setText("");
        contact_no.setText("");
        adhar_no.setText("");
        city.setText("");
        state.setText("");
        pin_code.setText("");
    }

    @Override
    public void validate() {
        Log.d("logged","hello");

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            String  firstname          =     TabFarmerName1.firstname1.getText().toString();
            String  lastname           =     TabFarmerName1.lastname1.getText().toString();

            if (firstname.trim().trim().isEmpty()) {
                TabFarmerName1.firstname1.setError("farmer name not entered");
                TabFarmerName1.firstname1.requestFocus();
                demo = "Demo";
            } else if (lastname.trim().isEmpty()) {
                TabFarmerName1.lastname1.setError("farmer last name not entered");
                TabFarmerName1.lastname1.requestFocus();
                demo = "Demo";
            }
        }else {
            demo = "Valid";
        }
    }


    public static String gets(){
        return demo;
    }
}
