package com.dairycenter.dairycenterapp.FarmerRegistrationTab;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.dairycenter.dairycenterapp.HomeFragments.ValidatedFragment;
import com.dairycenter.dairycenterapp.R;


public class TabCattleDetailes4 extends Fragment implements ValidatedFragment {
    public static EditText Noofcow,Noofbuffalo,totalNoOfCattle,rfid;
    public static String demo;
    LinearLayout l1,l2,l3;
    public static  String usercentercattle;
    RadioButton cow,buffflo,both;


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_cattle_detailes4, container, false);
        Log.e("tab5", "tab5");
        Noofcow             =    (EditText) view.findViewById(R.id.NoofCow);
        Noofbuffalo         =    (EditText) view.findViewById(R.id.NoofBuffalo);
        totalNoOfCattle     =    (EditText) view.findViewById(R.id.NoofCattle);
        rfid                =    (EditText) view.findViewById(R.id.rfid);
        l1                  =    (LinearLayout)view.findViewById(R.id.l1);
        l2                  =    (LinearLayout)view.findViewById(R.id.l2);
        l3                  =    (LinearLayout)view.findViewById(R.id.l3);
        cow                 =    (RadioButton) view.findViewById(R.id.cow);
        buffflo             =    (RadioButton) view.findViewById(R.id.buffalo);
        both                =    (RadioButton) view.findViewById(R.id.both);



        l1.setVisibility(View.GONE);
        l2.setVisibility(View.GONE);
        l3.setVisibility(View.GONE);

        usercentercattle = "";


        cow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    usercentercattle = "cow";
                    l1.setVisibility(View.VISIBLE);
                    l2.setVisibility(View.GONE);
                    l3.setVisibility(View.VISIBLE);

                    totalNoOfCattle.setText("");
                    Noofcow.setText("");

                    TextWatcher textWatcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(!Noofcow.getText().toString().equals("")){
                                int temp2    =    Integer.parseInt(Noofcow.getText().toString());
                                totalNoOfCattle.setText(String.valueOf(temp2));
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    };
                    Noofcow.addTextChangedListener(textWatcher);
                }
            }
        });

        buffflo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    usercentercattle = "buffelo";
                    l2.setVisibility(View.VISIBLE);
                    l1.setVisibility(View.GONE);
                    l3.setVisibility(View.VISIBLE);
                    totalNoOfCattle.setText("");
                    Noofbuffalo.setText("");


                    TextWatcher textWatcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(!Noofbuffalo.getText().toString().equals("")){
                                int temp2    =    Integer.parseInt(Noofbuffalo.getText().toString());
                                totalNoOfCattle.setText(String.valueOf(temp2));
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    };
                    Noofbuffalo.addTextChangedListener(textWatcher);
                }
            }
        });


        both.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    usercentercattle = "both";
                    l1.setVisibility(View.VISIBLE);
                    l2.setVisibility(View.VISIBLE);
                    l3.setVisibility(View.VISIBLE);

                    Noofcow.setText("");
                    Noofbuffalo.setText("");
                    totalNoOfCattle.setText("");


                    TextWatcher textWatcher = new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            if(!Noofcow.getText().toString().equals("") && !Noofbuffalo.getText().toString().equals("")){

                                int temp1    =    Integer.parseInt(Noofcow.getText().toString());
                                int temp2    =    Integer.parseInt(Noofbuffalo.getText().toString());

                                totalNoOfCattle.setText(String.valueOf(temp1 + temp2));
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    };
                    Noofcow.addTextChangedListener(textWatcher);
                    Noofbuffalo.addTextChangedListener(textWatcher);
                }
            }
        });


        ClearText5();
        return view;
    }

    public void ClearText5() {
        Noofcow.setText("");
        Noofbuffalo.setText("");
        totalNoOfCattle.setText("");
        rfid.setText("");
    }

    @Override
    public void validate() {
        Log.d("logged","hello");

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            String   bankname           =     TabBankDetailes3.name_bank.getSelectedItem().toString();
            String   branchname         =     TabBankDetailes3.branch_bank.getText().toString();
            String   accountno          =     TabBankDetailes3.no_account.getText().toString();
            String   ifsccode           =     TabBankDetailes3.code_ifsc.getText().toString();

            if (TabBankDetailes3.name_bank.getSelectedItem().toString().equals("Select")) {
                if (TabBankDetailes3.name_bank.getChildAt(0) != null && TabBankDetailes3.name_bank.getChildAt(0) instanceof TextView) {
                    TabBankDetailes3.name_bank.getChildAt(0).setFocusable(true);
                    ((TextView) TabBankDetailes3.name_bank.getChildAt(0)).setError("error");
                    TabBankDetailes3.name_bank.getChildAt(0).requestFocus();
                }
                demo = "Demo";
            } else if (branchname.trim().isEmpty()) {
                TabBankDetailes3.branch_bank.setError("branch name not entered");
                TabBankDetailes3.branch_bank.requestFocus();
                demo = "Demo";
            } else if (accountno.trim().isEmpty()) {
                TabBankDetailes3.no_account.setError("Please enter account number");
                TabBankDetailes3.no_account.requestFocus();
                demo = "Demo";
            } else if(accountno.length() < 9) {
                TabBankDetailes3.no_account.setError("Enter a right account number");
                TabBankDetailes3.no_account.requestFocus();
                demo = "Demo";
            }else if(accountno.length() < 18) {
                TabBankDetailes3.no_account.setError("Enter a right account number");
                TabBankDetailes3.no_account.requestFocus();
                demo = "Demo";
            }else if (ifsccode.trim().isEmpty()) {
                TabBankDetailes3.code_ifsc.setError("Please enter IFSC code");
                TabBankDetailes3.code_ifsc.requestFocus();
                demo = "Demo";
            }else if (ifsccode.trim().length() < 11) {
                TabBankDetailes3.code_ifsc.setError("Enter a eleven digit IFSC code");
                TabBankDetailes3.code_ifsc.requestFocus();
                demo = "Demo";
            }else if (ifsccode.trim().length() > 11) {
                TabBankDetailes3.code_ifsc.setError("Enter a eleven digit IFSC code");
                TabBankDetailes3.code_ifsc.requestFocus();
                demo = "Demo";
            }
            else {
                demo = "Valid";
            }
            Log.d("DEmoooooo",demo);
        }
    }

    public static String getstr2(){
        return demo;
    }

}


