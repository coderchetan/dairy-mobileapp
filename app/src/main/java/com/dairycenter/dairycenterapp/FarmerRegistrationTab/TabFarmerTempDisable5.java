package com.dairycenter.dairycenterapp.FarmerRegistrationTab;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.HomeFragments.ValidatedFragment;
import com.dairycenter.dairycenterapp.R;


public class TabFarmerTempDisable5 extends Fragment implements ValidatedFragment {

    CheckBox checkBox;
    public static String value,demo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_farmer_temp_disable, container, false);
        Log.e("tab4", "tab4");
        checkBox = (CheckBox) view.findViewById(R.id.c1);
        value = "True";

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    value = "False";
                } else {
                    value = "True";
                }
            }
        });
        return view;
    }

    public void validate() {
        Log.d("logged","hello");
    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            String noofcow       = TabCattleDetailes4.Noofcow.getText().toString();
            String noofbuffelo   = TabCattleDetailes4.Noofbuffalo.getText().toString();
            String noofcattle    = TabCattleDetailes4.totalNoOfCattle.getText().toString();
            String  rfids        = TabCattleDetailes4.rfid.getText().toString();

            if (TabCattleDetailes4.usercentercattle.equals("")) {
                Toast.makeText(getActivity(), "Plaese select cattle type", Toast.LENGTH_LONG).show();
                demo = "Demo";
            } else if (TabCattleDetailes4.usercentercattle.toString().equals("cow") && noofcow.trim().trim().isEmpty()) {
                TabCattleDetailes4.Noofcow.setError("Enter a number of cow");
                TabCattleDetailes4.Noofcow.requestFocus();
                demo = "Demo";
            } else if (TabCattleDetailes4.usercentercattle.toString().equals("buffelo") && noofbuffelo.trim().isEmpty()) {
                TabCattleDetailes4.Noofbuffalo.setError("Enter a number of buffalo");
                TabCattleDetailes4.Noofbuffalo.requestFocus();
                demo = "Demo";
            } else if (TabCattleDetailes4.usercentercattle.toString().equals("both") && noofcow.trim().trim().isEmpty()
                    && noofbuffelo.trim().isEmpty()) {
                TabCattleDetailes4.Noofcow.setError("Enter a number of cow");
                TabCattleDetailes4.Noofcow.requestFocus();
                TabCattleDetailes4.Noofbuffalo.setError("Enter a number of buffalo");
                TabCattleDetailes4.Noofbuffalo.requestFocus();
                demo = "Demo";
            }else if (rfids.trim().isEmpty()) {
                TabCattleDetailes4.rfid.setError("Enter a RFID number");
                TabCattleDetailes4.rfid.requestFocus();
                demo = "Demo";
            }else if (rfids.trim().length() < 8) {
                TabCattleDetailes4.rfid.setError("Enter a eight digit RFID number");
                TabCattleDetailes4.rfid.requestFocus();
                demo = "Demo";
            }else if (rfids.trim().length() > 8) {
                TabCattleDetailes4.rfid.setError("Enter a eight digit RFID number");
                TabCattleDetailes4.rfid.requestFocus();
                demo = "Demo";
            }else {
                demo = "Valid";
            }
            Log.d("DEmoooooo", demo);
        }

    }
    public static String getstr3(){
        return demo;
    }
}







