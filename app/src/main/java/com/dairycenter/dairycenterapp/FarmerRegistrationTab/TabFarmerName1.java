package com.dairycenter.dairycenterapp.FarmerRegistrationTab;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.dairycenter.dairycenterapp.DatabaseHelper;
import com.dairycenter.dairycenterapp.R;
import java.io.ByteArrayOutputStream;
import static android.app.Activity.RESULT_OK;

public class TabFarmerName1 extends Fragment {

    public static EditText firstname1, middlename1, lastname1, fisrtRelegionName, MiddleReligionName, LastReligionname;
    private ProgressDialog progressDialog;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();
    DatabaseHelper db;
    private static final int GALLERY_INTENT = 2;
    public static final int READ_EXTERNAL_STORAGE = 0;

    public static final int RequestPermissionCode = 1;
    ImageView user_image;
    public static Button b1;
    public static byte[] datas,datas2;
    public static String imgString;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_farmer_name1, container, false);

        db = new DatabaseHelper(getActivity().getApplicationContext());
        db.getReadableDatabase();

        firstname1           =      (EditText) view.findViewById(R.id.firstname);
        middlename1          =      (EditText) view.findViewById(R.id.middlename);
        lastname1            =      (EditText) view.findViewById(R.id.lastname);
        fisrtRelegionName    =      (EditText) view.findViewById(R.id.fistregionalname);
        MiddleReligionName   =      (EditText) view.findViewById(R.id.middleregionalname);
        LastReligionname     =      (EditText) view.findViewById(R.id.lastregionalname);
        user_image           =      (ImageView) view.findViewById(R.id.user_image);
        b1                   =      (Button) view.findViewById(R.id.add);

        progressDialog = new ProgressDialog(getActivity().getApplicationContext());
        ClearText();

        user_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Select From");
                builder.setCancelable(true);
                builder.setPositiveButton("Gallery", new DialogInterface
                        .OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        //                        Check for Runtime Permission
                        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Toast.makeText(getActivity().getApplicationContext(), "Call for Permission", Toast.LENGTH_SHORT).show();
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_EXTERNAL_STORAGE);
                            }
                        } else {
                            callgalary();
                        }
                    }


                });

                builder
                        .setNegativeButton(
                                "Camera",
                                new DialogInterface
                                        .OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.CAMERA)
                                                != PackageManager.PERMISSION_GRANTED) {
                                            Toast.makeText(getActivity().getApplicationContext(), "CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_SHORT).show();
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{Manifest.permission.CAMERA}, RequestPermissionCode);
                                            }
                                        } else {
                                            callCamera();

                                        }
                                    }
                                });


                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        return view;
    }

    public void ClearText() {
        firstname1.setText("");
        middlename1.setText("");
        lastname1.setText("");
        fisrtRelegionName.setText("");
        MiddleReligionName.setText("");
        LastReligionname.setText("");
    }


    private void callgalary() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, GALLERY_INTENT);
    }

    private void callCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 7);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {

            case READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    callgalary();
                return;
        }
    }

    private void setProgresBar() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMax(100);
        progressDialog.show();
        progressBarStatus = 0;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (progressBarStatus < 100) {
                    progressBarStatus += 30;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressBarHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.setProgress(progressBarStatus);

                        }
                    });
                }
                if (progressBarStatus >= 100) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }
            }
        }).start();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == GALLERY_INTENT && resultCode == RESULT_OK) {
            final Uri mImageUri = data.getData();
            setProgresBar();
            user_image.setImageURI(mImageUri);
            adds();
            adds2();

        }


        if (requestCode == 7 && resultCode == RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            setProgresBar();
            user_image.setImageBitmap(photo);
            adds();
            adds2();
        }
    }


    public void adds() {
        user_image.setDrawingCacheEnabled(true);
        user_image.buildDrawingCache();
        Bitmap bitmap = user_image.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        datas = baos.toByteArray();
    }

    public void adds2(){
        user_image.setDrawingCacheEnabled(true);
        user_image.buildDrawingCache();
        Bitmap bitmap = user_image.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        datas2 = baos.toByteArray();
        imgString = Base64.encodeToString(datas2,Base64.DEFAULT);

    }

    public static byte[] getProfilePic() {
        return datas;
    }
    public static String getProfilePic2(){
        return imgString;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}




