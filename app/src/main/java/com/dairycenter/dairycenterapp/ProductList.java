package com.dairycenter.dairycenterapp;

public class ProductList {

    String productname,prodct_number,prodct_unit,quantity;


    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProdct_number() {
        return prodct_number;
    }

    public void setProdct_number(String prodct_number) {
        this.prodct_number = prodct_number;
    }

    public String getProdct_unit() {
        return prodct_unit;
    }

    public void setProdct_unit(String prodct_unit) {
        this.prodct_unit = prodct_unit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
