package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import com.dairycenter.dairycenterapp.Adapter.CollectionReportListAdapter;
import com.dairycenter.dairycenterapp.Adapter.NoOfDaysMilkReportAdapter;
import com.dairycenter.dairycenterapp.Adapter.RateChartReportListAdapter;

import java.util.ArrayList;
import java.util.List;

public class RateChartReportListActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    RateChartReportListAdapter adapter;
    DatabaseHelper db;
    List<RateChartReport> list = new ArrayList<>();

    String centerid;
    String weighting_scale,analyzer_scale,id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_chart_report_list);

        getSupportActionBar().setTitle("Rate Chart Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        db = new DatabaseHelper(this);
        SQLiteDatabase dataBase = db.getWritableDatabase();
        Cursor mCursor = dataBase.rawQuery("SELECT * FROM "
                + DatabaseHelper.downloadsettingdata, null);
        if (mCursor.moveToFirst()) {
            do {
                String id1 = mCursor.getString(mCursor.getColumnIndex("cowmorning"));
                String id2 = mCursor.getString(mCursor.getColumnIndex("buffelomorning"));
                String id3 = mCursor.getString(mCursor.getColumnIndex("cowevening"));
                String id4 = mCursor.getString(mCursor.getColumnIndex("buffeloevening"));
                String weighting_scale = mCursor.getString(mCursor.getColumnIndex("weighting_scale"));
                String analyzer_scale = mCursor.getString(mCursor.getColumnIndex("analyzer_scale"));

                RateChartReport rateChartReport = new RateChartReport();

                rateChartReport.setWeightingscale(weighting_scale);
                rateChartReport.setAnalyaerscale(analyzer_scale);

                Cursor mCursor1 = dataBase.rawQuery("SELECT * FROM "
                        + DatabaseHelper.listRatechart, null);
                if (mCursor1.moveToFirst()) {
                    do {
                        if (id1.equals(mCursor1.getString(mCursor1.getColumnIndex("id_R")))) {
                            String id_R = mCursor1.getString(mCursor1.getColumnIndex("id_R"));
                            String chartname = mCursor1.getString(mCursor1.getColumnIndex("chartName"));
                            String ratechart = mCursor1.getString(mCursor1.getColumnIndex("ratechart"));
                            String chrttype = mCursor1.getString(mCursor1.getColumnIndex("charttype"));
                            String cratedat = mCursor1.getString(mCursor1.getColumnIndex("created_at"));
                            String fatrangefrm = mCursor1.getString(mCursor1.getColumnIndex("fatrange_from"));
                            String fatrangeto = mCursor1.getString(mCursor1.getColumnIndex("fatrange_to"));
                            String fatsnfrangefrm = mCursor1.getString(mCursor1.getColumnIndex("fatsnfrange_from"));
                            String fatsnfrangeto = mCursor1.getString(mCursor1.getColumnIndex("fatsnfrange_to"));

                            rateChartReport.setId(id_R);
                            rateChartReport.setChartname(chartname);
                            rateChartReport.setRatechart(ratechart);
                            rateChartReport.setChrttype(chrttype);
                            rateChartReport.setCratedat(cratedat);
                            rateChartReport.setFatrangefrm(fatrangefrm);
                            rateChartReport.setFatrangeto(fatrangeto);
                            rateChartReport.setFatsnfrangefrm(fatsnfrangefrm);
                            rateChartReport.setFatsnfrangeto(fatsnfrangeto);
                        }

                    } while (mCursor1.moveToNext());
                }

                RateChartReport rateChartReport2 = new RateChartReport();
                Cursor mCursor2 = dataBase.rawQuery("SELECT * FROM "
                        + DatabaseHelper.listRatechart, null);
                if (mCursor2.moveToFirst()) {
                    do {
                        if (id2.equals(mCursor2.getString(mCursor2.getColumnIndex("id_R")))) {
                            String id_R         = mCursor2.getString(mCursor2.getColumnIndex("id_R"));
                            String chartname    = mCursor2.getString(mCursor2.getColumnIndex("chartName"));
                            String ratechart    = mCursor2.getString(mCursor2.getColumnIndex("ratechart"));
                            String chrttype     = mCursor2.getString(mCursor2.getColumnIndex("charttype"));
                            String cratedat     = mCursor2.getString(mCursor2.getColumnIndex("created_at"));
                            String fatrangefrm  = mCursor2.getString(mCursor2.getColumnIndex("fatrange_from"));
                            String fatrangeto    = mCursor2.getString(mCursor2.getColumnIndex("fatrange_to"));
                            String fatsnfrangefrm = mCursor2.getString(mCursor2.getColumnIndex("fatsnfrange_from"));
                            String fatsnfrangeto = mCursor2.getString(mCursor2.getColumnIndex("fatsnfrange_to"));

                            rateChartReport2.setId(id_R);
                            rateChartReport2.setChartname(chartname);
                            rateChartReport2.setRatechart(ratechart);
                            rateChartReport2.setChrttype(chrttype);
                            rateChartReport2.setCratedat(cratedat);
                            rateChartReport2.setFatrangefrm(fatrangefrm);
                            rateChartReport2.setFatrangeto(fatrangeto);
                            rateChartReport2.setFatsnfrangefrm(fatsnfrangefrm);
                            rateChartReport2.setFatsnfrangeto(fatsnfrangeto);
                        }

                    } while (mCursor2.moveToNext());
                }

                RateChartReport rateChartReport3 = new RateChartReport();

                Cursor mCursor3 = dataBase.rawQuery("SELECT * FROM "
                        + DatabaseHelper.listRatechart, null);
                if (mCursor3.moveToFirst()) {
                    do {
                        if (id3.equals(mCursor3.getString(mCursor3.getColumnIndex("id_R")))) {
                            String id_R         = mCursor3.getString(mCursor3.getColumnIndex("id_R"));
                            String chartname    = mCursor3.getString(mCursor3.getColumnIndex("chartName"));
                            String ratechart    = mCursor3.getString(mCursor3.getColumnIndex("ratechart"));
                            String chrttype     = mCursor3.getString(mCursor3.getColumnIndex("charttype"));
                            String cratedat     = mCursor3.getString(mCursor3.getColumnIndex("created_at"));
                            String fatrangefrm  = mCursor3.getString(mCursor3.getColumnIndex("fatrange_from"));
                            String fatrangeto   = mCursor3.getString(mCursor3.getColumnIndex("fatrange_to"));
                            String fatsnfrangefrm = mCursor3.getString(mCursor3.getColumnIndex("fatsnfrange_from"));
                            String fatsnfrangeto = mCursor3.getString(mCursor3.getColumnIndex("fatsnfrange_to"));

                            rateChartReport3.setId(id_R);
                            rateChartReport3.setChartname(chartname);
                            rateChartReport3.setRatechart(ratechart);
                            rateChartReport3.setChrttype(chrttype);
                            rateChartReport3.setCratedat(cratedat);
                            rateChartReport3.setFatrangefrm(fatrangefrm);
                            rateChartReport3.setFatrangeto(fatrangeto);
                            rateChartReport3.setFatsnfrangefrm(fatsnfrangefrm);
                            rateChartReport3.setFatsnfrangeto(fatsnfrangeto);
                        }

                    } while (mCursor3.moveToNext());
                }

                RateChartReport rateChartReport4 = new RateChartReport();

                Cursor mCursor4 = dataBase.rawQuery("SELECT * FROM "
                        + DatabaseHelper.listRatechart, null);
                if (mCursor4.moveToFirst()) {
                    do {
                        if (id4.equals(mCursor4.getString(mCursor4.getColumnIndex("id_R")))) {
                            String id_R         = mCursor4.getString(mCursor4.getColumnIndex("id_R"));
                            String chartname    = mCursor4.getString(mCursor4.getColumnIndex("chartName"));
                            String ratechart    = mCursor4.getString(mCursor4.getColumnIndex("ratechart"));
                            String chrttype     = mCursor4.getString(mCursor4.getColumnIndex("charttype"));
                            String cratedat     = mCursor4.getString(mCursor4.getColumnIndex("created_at"));
                            String fatrangefrm  = mCursor4.getString(mCursor4.getColumnIndex("fatrange_from"));
                            String fatrangeto   = mCursor4.getString(mCursor4.getColumnIndex("fatrange_to"));
                            String fatsnfrangefrm = mCursor4.getString(mCursor4.getColumnIndex("fatsnfrange_from"));
                            String fatsnfrangeto = mCursor4.getString(mCursor4.getColumnIndex("fatsnfrange_to"));


                            rateChartReport4.setId(id_R);
                            rateChartReport4.setChartname(chartname);
                            rateChartReport4.setRatechart(ratechart);
                            rateChartReport4.setChrttype(chrttype);
                            rateChartReport4.setCratedat(cratedat);
                            rateChartReport4.setFatrangefrm(fatrangefrm);
                            rateChartReport4.setFatrangeto(fatrangeto);
                            rateChartReport4.setFatsnfrangefrm(fatsnfrangefrm);
                            rateChartReport4.setFatsnfrangeto(fatsnfrangeto);
                        }

                    } while (mCursor4.moveToNext());
                }

                list.add(rateChartReport);
                list.add(rateChartReport2);
                list.add(rateChartReport3);
                list.add(rateChartReport4);
                adapter = new RateChartReportListAdapter(RateChartReportListActivity.this, list);
                recyclerView.setAdapter(adapter);

            } while (mCursor.moveToNext());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}