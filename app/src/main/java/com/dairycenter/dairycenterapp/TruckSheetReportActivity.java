package com.dairycenter.dairycenterapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dairycenter.dairycenterapp.Adapter.RateChartReportListAdapter;
import com.dairycenter.dairycenterapp.Adapter.TruckSheetReportAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.dairycenter.dairycenterapp.DatabaseHelper.TRUCKSHEET;

public class TruckSheetReportActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    TruckSheetReportAdapter adapter;
    DatabaseHelper db;
    List<TruckSheetReport> list = new ArrayList<>();
    private DatePickerDialog.OnDateSetListener mDateSetListener1;
    private DatePickerDialog.OnDateSetListener mDateSetListener2;
    TextView start_date,end_date;
    ImageView Simg, Eimg;
    String days,Months;
    Button Viewreport;

    Date mdate;
    String datess1,datess2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_truck_sheet_report);

        getSupportActionBar().setTitle("Truck Sheet Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView           = (RecyclerView)findViewById(R.id.recycler_view);
        start_date             = (TextView) findViewById(R.id.sdte);
        end_date               = (TextView) findViewById(R.id.enddte);
        Simg                   = (ImageView) findViewById(R.id.startimg);
        Eimg                   = (ImageView) findViewById(R.id.endimg);
        Viewreport             = (Button) findViewById(R.id.viewreport);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        Simg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        TruckSheetReportActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener1,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                if (month < 10) {
                    Months = "0"+month;
                } else {
                    Months = String.valueOf(month);
                }

                if (day < 10) {
                    days = "0"+day;
                } else {
                    days = String.valueOf(day);
                }
                String dates = days + "-" + Months + "-" + year;
                start_date.setText(dates);

                String str = start_date.getText().toString();
                SimpleDateFormat formats = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    mdate = formats.parse(str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                datess1 = simpleDateFormat.format(mdate);
            }
        };

        Eimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        TruckSheetReportActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener2,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;

                if (month < 10) {
                    Months = "0"+month;
                } else {
                    Months = String.valueOf(month);
                }

                if (day < 10) {
                    days = "0"+day;
                } else {
                    days = String.valueOf(day);
                }

                String dates = days + "-" + Months + "-" + year;
                end_date.setText(dates);

                String str = end_date.getText().toString();
                SimpleDateFormat formats = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    mdate = formats.parse(str);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                datess2 = simpleDateFormat.format(mdate);
            }
        };

        Viewreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (start_date.getText().toString().isEmpty()) {
                    start_date.setError("Please select start date");
                    start_date.requestFocus();
                } else if (end_date.getText().toString().isEmpty()) {
                    end_date.setError("Please select end date");
                    end_date.requestFocus();
                }else {
                    db = new DatabaseHelper(TruckSheetReportActivity.this);
                    SQLiteDatabase dataBase = db.getWritableDatabase();
                    list.clear();
                    Cursor mCursor = dataBase.rawQuery("SELECT * FROM trucksheet " +
                            "where shiftclose_dates between? AND?", new String[]{datess1, datess2}, null);
                    if (mCursor.moveToFirst()) {
                            do {
                                String vehiNo = mCursor.getString(mCursor.getColumnIndex("vehicle_nos"));
                                String Drivername = mCursor.getString(mCursor.getColumnIndex("driver_names"));
                                String drivercon_no = mCursor.getString(mCursor.getColumnIndex("contact_nos"));
                                String date = mCursor.getString(mCursor.getColumnIndex("shiftclose_dates"));
                                String time = mCursor.getString(mCursor.getColumnIndex("shiftclose_times"));
                                String shift = mCursor.getString(mCursor.getColumnIndex("shiftclose"));
                                String totalwt_cow = mCursor.getString(mCursor.getColumnIndex("shift_wt_cows"));
                                String totalwt_buffalo = mCursor.getString(mCursor.getColumnIndex("shift_wt_buffalos"));
                                String avgfat_cow = mCursor.getString(mCursor.getColumnIndex("shift_fat_cows"));
                                String avgfat_buffalo = mCursor.getString(mCursor.getColumnIndex("shift_fat_buffalos"));
                                String avgsnf_cow = mCursor.getString(mCursor.getColumnIndex("shift_snf_cows"));
                                String avgsnf_buffalo = mCursor.getString(mCursor.getColumnIndex("shift_snf_buffalos"));
                                String totalwt = mCursor.getString(mCursor.getColumnIndex("shift_sum_cow_buffalos"));

                                String strt = date.toString();;
                                SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    mdate = formats.parse(strt);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat simpleDate = new SimpleDateFormat("dd/MM/yyyy");
                                date = simpleDate.format(mdate);


                                TruckSheetReport truckSheetReport = new TruckSheetReport();
                                truckSheetReport.setVehiNo(vehiNo);
                                truckSheetReport.setDrivername(Drivername);
                                truckSheetReport.setDrivercon_no(drivercon_no);
                                truckSheetReport.setDate(date);
                                truckSheetReport.setTime(time);
                                truckSheetReport.setShift(shift);
                                truckSheetReport.setTotalwt_cow(totalwt_cow);
                                truckSheetReport.setTotalwt_buffalo(totalwt_buffalo);
                                truckSheetReport.setAvgfat_cow(avgfat_cow);
                                truckSheetReport.setAvgfat_buffalo(avgfat_buffalo);
                                truckSheetReport.setAvgsnf_cow(avgsnf_cow);
                                truckSheetReport.setAvgsnf_buffalo(avgsnf_buffalo);
                                truckSheetReport.setTotalwt(totalwt);

                                list.add(truckSheetReport);
                                adapter = new TruckSheetReportAdapter(TruckSheetReportActivity.this, list);
                                recyclerView.setAdapter(adapter);

                                start_date.setError(null);
                                end_date.setError(null);

                            } while (mCursor.moveToNext());
                        }
                    else if (mCursor.getCount() == 0) {
                        list.clear();
//                        adapter.notifyDataSetChanged();
                        Toast.makeText(TruckSheetReportActivity.this, "Record not available", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return true;
    }
}


